<?php
  /**
 * Plugin Name: GPUSA Ajax Post Filter
 * Plugin URI: http://www.echo.co
 * Description: Allows end users to filter posts
 * Version: 1.0.0
 * Author: Yasin Ali
 */

// Enqueue script

add_action('wp_enqueue_scripts', 'ajax_filter_posts_scripts', 100);

function ajax_filter_posts_scripts() {

  // Enqueue script
  
  wp_register_script('afp_script', get_template_directory_uri() . '/js/ajax-filter-posts.js', false, null, false);
  wp_enqueue_script('afp_script');

  wp_localize_script( 'afp_script', 'afp_vars', array(
        'afp_nonce' => wp_create_nonce( 'afp_nonce' ), // Create nonce which we later will use to verify AJAX request
        'afp_ajax_url' => admin_url( 'admin-ajax.php' ),
      )
  );
}

add_action('wp_ajax_filter_posts', 'filter_posts', 10);
add_action('wp_ajax_nopriv_filter_posts', 'filter_posts', 10);

function filter_posts() {
    global $paged;
  
    $user = wp_get_current_user();
	$uid = (int) $user->ID;
	$gofs = get_option( 'gmt_offset' ); // get WordPress offset in hours
	$tz = date_default_timezone_get(); // get current PHP timezone
	date_default_timezone_set('Etc/GMT'.(($gofs < 0)?'+':'').-$gofs); // set the PHP timezone to match WordPress

    remove_all_filters('posts_orderby');
  
    // Verify nonce
    if( !isset( $_POST['afp_nonce'] ) || !wp_verify_nonce( $_POST['afp_nonce'], 'afp_nonce' ))
      die('Permission denied<br/><br/>User ID: '.$uid.'<br/>Token: '.$_POST['afp_nonce'].'<br/>Request Time: '.date('Y-m-d h:i:s'));
    
    // Post type is always passed
    $post_types = $_POST['posttypes'];
    $taxon  = ($post_types[0] == 'bios') ? 'bio_tag' : 'category';
    $topics = $_POST['topics'] ? $_POST['topics'] : null;
    $order  = $_POST['order'] ? $_POST['order'] : 'DESC';
    $paged  = $_POST['paged'] ? $_POST['paged'] : 1;
    
    // Build the arguments array to pass into WP_Query and query_posts
    $args = array(
      'tax_query'=> array(
        array(
          'taxonomy'    => $taxon,
          'field'       => 'slug',
          'terms'       => $topics,
        )
      ),
      'post_type'       => $post_types,
      'posts_per_page'  => 10,
      'orderby'         => 'date',
      'order'           => $order,
      'paged'           => $paged
    );
    
    // Make a call to query_posts to inform your pagination, otherwise it won't work
    query_posts($args);
    
    // Create new context object, get posts, and pagination
    $context = Timber::get_context();
    $context['posts'] = Timber::get_posts($args);
    $context['pagination'] = Timber::get_pagination();
    
    // Compile results to a template to build HTML
    $templates = array( 'layouts/teaser-list-' . $post_types[0] . '.twig', 'layouts/teaser-list.twig' );
    $result = Timber::compile($templates, $context );
    
    // Echo the HTML
    if ( !empty($context['posts']) ) {
      echo $result;
    } else {
      echo '<h2>No posts match the selected filters</h2>';
    }
    // Die
    wp_die();
}

/*

// temporarily disabled due to duplicate posts issue

add_action('wp_ajax_filter_posts', 'filter_posts', 10);
add_action('wp_ajax_nopriv_filter_posts', 'filter_posts', 10);

function filter_posts() {
	global $paged;
	$page = md5(serialize($_POST['topics']).serialize($_POST['posttypes']).serialize($_POST['order']).serialize($_POST['paged']));

	// supply from cache
	
	$page_from_cache = get_option( "ajax_cache-{$page}", 0 );
	if($page_from_cache){
		die($page_from_cache);
	}
	// end cache

  remove_all_filters('posts_orderby');
  
  // Verify nonce
  if( !isset( $_POST['afp_nonce'] ) || !wp_verify_nonce( $_POST['afp_nonce'], 'afp_nonce' ) )
    die('Permission denied');
  
  // Post type is always passed
  $post_types = $_POST['posttypes'];
  $taxon  = ($post_types[0] == 'bios') ? 'bio_tag' : 'category';
  $topics = $_POST['topics'] ? $_POST['topics'] : null;
  $order  = $_POST['order'] ? $_POST['order'] : 'DESC';
  $paged  = $_POST['paged'] ? $_POST['paged'] : 1;
  
  // Build the arguments array to pass into WP_Query and query_posts
  $args = array(
    'tax_query'=> array(
      array(
        'taxonomy'    => $taxon,
        'field'       => 'slug',
        'terms'       => $topics,
      )
    ),
    'post_type'       => $post_types,
    'posts_per_page'  => 10,
    'orderby'         => 'date',
    'order'           => $order,
    'paged'           => $paged
  );

	// Make a call to query_posts to inform your pagination, otherwise it won't work
	
	query_posts($args);

	// Begin cache build

	ob_start();

	// Create new context object, get posts, and pagination
	$context = Timber::get_context();
	$context['posts'] = Timber::get_posts($args);
	$context['pagination'] = Timber::get_pagination();
	// Compile results to a template to build HTML
	$templates = array( 'layouts/teaser-list-' . $post_types[0] . '.twig', 'layouts/teaser-list.twig' );
	$result = Timber::compile($templates, $context );

	// Echo the HTML
	if ( !empty($context['posts']) ) {
	echo $result;
	} else {
	echo '<h2>No posts match the selected filters</h2>';
	}

	$new_cache = ob_get_flush();

	// Die

	// save the cache
	
	update_option( "ajax_cache-{$page}",$new_cache);

	// end cache

	die($new_cache);  
}

add_action("save_post","invalidate_cache");

function invalidate_cache(){
	global $wpdb;
	$wpdb->query( "DELETE FROM {$wpdb->options} WHERE option_name LIKE 'ajax_cache%'" );
}

*/