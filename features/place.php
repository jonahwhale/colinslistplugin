<?php
/**
 * Register a custom post type called "place".
 *
 * @see get_post_type_labels() for label keys.
 */
function wpdocs_codex_place_init() {
    $labels = array(
        'name'                  => _x( 'Places', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Place', 'Post type singular name', 'textdomain' ),
        'menu_name'             => _x( 'Places', 'Admin Menu text', 'textdomain' ),
        'name_admin_bar'        => _x( 'place', 'Add New on Toolbar', 'textdomain' ),
        'add_new'               => __( 'Add New', 'textdomain' ),
        'add_new_item'          => __( 'Add New place', 'textdomain' ),
        'new_item'              => __( 'New place', 'textdomain' ),
        'edit_item'             => __( 'Edit place', 'textdomain' ),
        'view_item'             => __( 'View place', 'textdomain' ),
        'all_items'             => __( 'All places', 'textdomain' ),
        'search_items'          => __( 'Search places', 'textdomain' ),
        'parent_item_colon'     => __( 'Parent places:', 'textdomain' ),
        'not_found'             => __( 'No places found.', 'textdomain' ),
        'not_found_in_trash'    => __( 'No places found in Trash.', 'textdomain' ),
        'featured_image'        => _x( 'Place Cover Image', 'Overrides the “Featured Image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'set_featured_image'    => _x( 'Set cover image', 'Overrides the “Set featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'remove_featured_image' => _x( 'Remove cover image', 'Overrides the “Remove featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'use_featured_image'    => _x( 'Use as cover image', 'Overrides the “Use as featured image” phrase for this post type. Added in 4.3', 'textdomain' ),
        'archives'              => _x( 'Place archives', 'The post type archive label used in nav menus. Default “Post Archives”. Added in 4.4', 'textdomain' ),
        'insert_into_item'      => _x( 'Insert into place', 'Overrides the “Insert into post”/”Insert into page” phrase (used when inserting media into a post). Added in 4.4', 'textdomain' ),
        'uploaded_to_this_item' => _x( 'Uploaded to this place', 'Overrides the “Uploaded to this post”/”Uploaded to this page” phrase (used when viewing media attached to a post). Added in 4.4', 'textdomain' ),
        'filter_items_list'     => _x( 'Filter places list', 'Screen reader text for the filter links heading on the post type listing screen. Default “Filter posts list”/”Filter pages list”. Added in 4.4', 'textdomain' ),
        'items_list_navigation' => _x( 'places list navigation', 'Screen reader text for the pagination heading on the post type listing screen. Default “Posts list navigation”/”Pages list navigation”. Added in 4.4', 'textdomain' ),
        'items_list'            => _x( 'places list', 'Screen reader text for the items list heading on the post type listing screen. Default “Posts list”/”Pages list”. Added in 4.4', 'textdomain' ),
    );

    $args = array(
        'labels'             => $labels,
        'public'             => true,
        'publicly_queryable' => true,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'place' ),
        'capability_type'    => 'page',
        'has_archive'        => true,
        'hierarchical'       => false,
        'menu_position'      => null,
        'supports'           => array( 'title', 'author', 'thumbnail', 'excerpt', ),
    );

    register_post_type( 'place', $args );
}

add_action( 'init', 'wpdocs_codex_place_init' );
