<?php
// require_once(dirname(__FILE__).'/../dbos/DBOS.class.php');
require_once(dirname(__FILE__).'/../model/Place.class.php');

add_action( 'admin_menu', 'colins_list_admin_menu' );


function colins_list_admin_menu() {
    add_menu_page(
        'Colin\'s List',
        $menu_title="Colins's List",
        $capability='manage_options',
        $menu_slug='colins_list_admin',
        $function='colins_list_admin',
        $icon_url = false,
        $position = 90);
}

function da($var) {
    ?><pre><?php
    print_r($var);
    ?></pre><?php
}


function show_place($place) {
    require(dirname(__FILE__)."/../../../themes/blocksy/template-parts/place.php");
}

function getAllPlaces() {
 
    $params = ['limit'=>2000];
    // Run the find
    $mypod = pods( 'place', $params );
    $places = $mypod->data();
    return $places;
}

function colins_list_admin() {
    if(isset($_REQUEST['action'])) { $action = $_REQUEST['action']; } else { $action = false; }
    $pt = new PlaceTable();
    // $cgt = new CountryGroupTable();
    $cache = $pt->fetchCache();
    dump($cache);
    switch($action) {
        case"show_dropdown":
            colins_list();
            break;
        case"list_groups":
            $cgt->adminPage();
            break;
        case"list_places":
            $pt->adminPage();
            break;
        case"load_data":
            $ct->loadData();
            break;
        default:
    }



    // Here's how to use find()

    $places = getAllPlaces();
    $count = count($places);
    // Loop through the records returned
    ?>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
    <h1>Colin's List Admin</h1>
    <a class='button' href='/wp-admin/post-new.php?post_type=place'><i class='fa fa-plus'></i> New Place</a>
    <br /><br />
    <div> Showing <span class="badge badge-info"><?= $count ?></span> places </div>
    <table class='wp-list-table widefat  striped '>
        <thead>
        <tr>
            <th>Action</th>
            <th>Place</th>
            <td>Neighborhood</td>
            <th>Drinks</th>
            <th>Confirmed</th>
            <th>Picture</th>
            <th>Colin's List</th>
            <th><nobr />Why Not / Caveat</nobr></th>
            <th>Address</th>
            <th>Website</th>
            <th>Facebook</th>
            <th>Instagram</th>
            <th>Twitter</th>
            <th>Changing Table</th>
            <th>High Chairs</th>

            <th>Description</th>
            <th>Tip</th>
            <th>Staff Notes</th>
        </tr>
        </thead>
        <tbody>
    <?php
   foreach($places AS $pl) {

       $place = new Place($pl);

        ?><tr>
            <td>
                <a class="button" href="/wp-admin/post.php?post=<?= $place->getId(); ?>&action=edit">Edit</a>
            </td>
            <td nowrap><?= $place->showCell('Place',$place); ?> </td>
            <td><?= $place->getNeighborhood(); ?></td>
            <td nowrap><?= $place->showDrinks(); ?> </td>
            <td nowrap><?= $place->showCell('Confirmed',$place); ?></td>
            <td></td>
            <td><?= $place->showCell('Colin\'s List',$place) ?></td>
            <td><?= $place->showCell('Why Not / Caveat',$place); ?></td>
            <td nowrap><?= $place->showCell('Address',$place); ?></td>
            <td><?= $place->showCell('Website',$place); ?></td>
            <td><?= $place->showCell('Facebook',$place); ?></td>
            <td><?= $place->showCell('Instagram',$place); ?></td>
            <td><?= $place->showCell('Twitter',$place); ?></td>
            <td><?= $place->showCell('Changing Table',$place); ?></td>
            <td><?= $place->showCell('High Chairs',$place); ?></td>

            <td><?= $place->showCell('Description',$place); ?></td>
            <td><?= $place->showCell('Tip',$place); ?></td>
            <td><?= $place->showCell('Staff Notes',$place); ?></td>
        </tr><?php
    }
    ?>
        </tbody>
    </table>
    <?php

}
