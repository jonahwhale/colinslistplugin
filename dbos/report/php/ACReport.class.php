<?php
/**
 * This is a prototype for the hotseat grassroots weekly checkin report.
 * I wanted it abstract and flexible, a means of generating sql for Columns
 * and making reports easier to generate for the DBOS/ActiveCore suite of tools. JB  8/13/08 3:24 PM
 *
 * This tool was developed for Greenpeace, Ben Smith in 2009, but never went into production
 * until 2010 for David Pomerantz.
 *
 * @liscense GPL
 * @version $Id: ACReport.class.php 8494 2015-09-03 03:57:42Z jbaker $
 * @package ActiveCore
 *
 */

/**
* Required files
*/
include_once(dirname(__FILE__)."/ReportColumn.class.php");
require_once($_SERVER['DOCUMENT_ROOT']."/survey/includes/classes/class.survey_question.php");
require_once($_SERVER['DOCUMENT_ROOT']."/survey/includes/classes/v2/Question/SurveyQuestionInput.class.php");

/**
* Build a table of trends reports based off of defined report columns.
* @package ActiveCore
* @version 1.0 JB 8/13/08
*/
class ACReportTable extends DBOS {
    /**
    * The db table to find the reports in.
    */
    var $_table = "report";

    /**
    * The db to find reporting structure.
    */
    var $_db = PERSON_DB;

    /**
    * The date field to group and sort by for trends.
    */
    var $_dateField = "created";

    /**
    * The file that handles admin and ajax calls.
    */
    var $_controller = "/admin/report/admin_report.php";

    /**
    * The auto-increment field.
    */
    var $_idField = "report_id";

    /**
    * The field to use as the title of the record.
    */
    var $_titleField = "title";

    /**
    * Default view for date ranges
    */
    var $_default_view = "weekly";
    
    var $_sortField = "sequence";
    /**
    * Array of column titles to hide
    */
    var $_hideFields = array();

    /**
    * Just load the table info, no output.
    */
    function __construct() {
        $this->loadTable();
    }

    /**
    * The actions you can take on a record
    */
    function adminActionCell($row) {
        $chapter_id = $_REQUEST['chapter_id'];
        if(is_numeric($chapter_id)) {
            $url .= "&chapter_id=".$chapter_id;
        }
        $this->startNavItem();
        ?>
        <a id="viewReport" href="<?= $_SERVER[PHP_SELF] ?>?action=show_report&report_id=<?= $row['report_id'] ?>&table=report<?= $url ?>">
            <i class="fa fa-bar-chart-o fa-lg"></i> View
        </a>
        <?php $this->endNavItem(); ?>
        
        <?
        parent::adminActionCell($row);
        ?> | <a class="addMe" id="addColumn" href="<?= $_SERVER[PHP_SELF] ?>?action=edit&report_id=<?= $row['report_id'] ?>&table=report_to_column">Add Column</a> <?php
    }
    
    function startNavItem($in=false) {
        ?><?php
    }
    
    function endNavItem($in=false) {
        ?><span class="navDivider">|</span><?php
    }
    /**
    * The main page switch to output the page body in admin.
    */
    function adminPage() {
        global $R;
        $action = $_REQUEST['action'];
        switch($action) {
            case"show_report":
                // $this->adminPageTop();
                $r = new ACTrendReport($R['report_id']);
                $r->showTable();
                break;
            default:
                parent::adminPage();
        }

    }

    /**
    * @version 1.0 Jonah B  1/30/15 9:22 AM
    */
    function handleAjax() {
    
    }

    /**
    * Admin header content above table.
    */
    function adminPageTop() {
        parent::adminPageTop();
        $this->endNavItem();
        ?>  <a id="listColumns" href="<?= $_SERVER['PHP_SELF'] ?>?action=list&table=report_column"><i class="fa fa-list"></i> List Columns</a>
        | <a class="graphMe" id="buildReport" href="<?= $_SERVER['PHP_SELF'] ?>?action=show_report&report_id=<?= $_REQUEST['report_id'] ?>&table=report">Build</a>
        | <a id="add_column" class="addMe" href="/admin/report/admin_report.php?action=edit&table=report_to_column&report_id=<?= $_REQUEST['report_id'] ?>">Add a new column</a>
        <?
        if($report_id=$_REQUEST['report_id']) {
            ACRelation::addForm(array("to_id"=>$report_id,"to_type"=>"report"));
        }
    }
    
    /**
    * 
    */
    function adminPageBottom() {
        parent::adminPageBottom();
        echo acScraper("https://secureusa.greenpeace.org/admin/wiki/index.php/Reporting_Engine","reportBuilderDocs");
    }

}

/*
* Main report class.
*
* @version 1.0 JB  9/2/08 7:33 AM
* @package ActiveCore
* @subpackage ReportingEngine
*/

class ACReport extends ACReportTable {
    /**
    * Create the object.
    */
    var $useGoals = false;

    var $showFooterTotalRow = true;
    
    function __construct($id) {
        $this->loadRecord($id);
        $this->_title = $this->title;
        if(!$this->survey_id && $this->parent_id) {
            $parent = new ACReport($this->parent_id);
            if($parent->survey_id) $this->inherit_survey_id = $parent->survey_id;
        }
    }

    /**
    * Customize the actions you can take on these records.
    */
    function adminActionCell($row) {
        parent::adminActionCell($row);
        ?>
        <?
    }

    /**
    * HTML output of this object, essentially an admin summary.
    *
    * @version 1.0 Jonah  12/22/08 1:20 PM
    */
    function toHTML() {
        parent::toHTML();
        ?><br /><br /><?
        $this->listColumns();
    }

    /*
    * @version 1.0  Jonah  9/2/08 3:17 AM
    */
    function listColumns() {
        $cols = new ReportToColumnTable();
        $opts = array(
            "where"=>" AND report_to_column.report_id = '".(int)$this->id."' ",
            "ajax"=>true,
            "order_by"=>"sequence ASC"
        );
        $cols->showTable($opts);
    }

    /**
    * How to show this object in widget view, for embedding.
    * @author Jonah  12/22/08 1:21 PM
    */
    function widgetRow() {
        ob_start();
        include($_SERVER['DOCUMENT_ROOT']."/admin/report/html/report_widget_row.html");
        return ob_get_clean();
    }

    /*
    * @version 1.0 Jonah  11/20/08 5:44 PM
    * @version 1.1 SJP 01/20/2010 01:00 PM Updated default support
    */
    function chooseView($default=false) {
        if(!$this->view) $this->view = $this->_default_view;

        ob_start();

        // Use the native function, otherwise carry a backup.  JB  11/20/08 5:42 PM
        if(function_exists('select_views')) {
            select_views($this->view);
        } else {
              ?>
              <label for "view">Date Range: </label>
              <select name="view" id="view" class="chooseDateRange"><?
                $views = array(
                  'daily'=>'Daily',
                  'weekly'=>'Weekly',
                  'monthly'=>'Monthly',
                  'quarterly' => 'Quarterly',
                  'ytd'=>'Yearly'
                  );
                echo select_options($views,$this->view, 3600);
              ?></select> <?
        }
        return ob_get_clean();
    }

    /**
    * @version 1.0 JB
    */
    public function save($in) {
        if(!$this->created) $this->created = "now";
        parent::save($in);
    }
    
    /**
    * @version 1.0 Jonah B  1/30/15 11:04 AM - Moved this into its own function.
    */
    function showFilters($opts=false) {
        global $chapter_id, $rows, $chapter_archived;
        
        if($opts['chapter_id']) {
            $this->chapter_id = $opts['chapter_id'];
            $chapter_id = $this->chapter_id;
        } elseif($_REQUEST['chapter_id']) {
            $this->chapter_id = $_REQUEST['chapter_id'];
        }
        $ct = new ChapterTable();
        if(!$this->chapter_id && $this->hideChapterSelect!=true) {
            $this->chapter_id = $ct->getChaptersSession();
        }
        
        if($debug) da($this->chapter_id);
        if(is_array($this->chapter_id) && $this->chapter_id[1]!='') {
            
            foreach ($this->chapter_id as $this->chapter_count => $cid) {
                // echo "Begin chapter $cid";
                if($cid >= 0) {
                    foreach($this->columns as $count => $column) {
                        if(is_array($column)) {
                            $column['chapter_id'] = $cid;
                        }
                        $this->report_cols[$this->chapter_count][$count] = ReportColumn::factory($column);
                        $this->report_cols[$this->chapter_count][$count]->chapter_id = $cid;
                        $this->report_cols[$this->chapter_count][$count]->_default_view = $this->_default_view;
                    }
                }
            }
        } else {
            foreach($this->columns as $count=>$column) {
              $this->report_cols[1][$count] = ReportColumn::factory($column);
              $this->report_cols[1][$count]->_default_view = $this->_default_view;
            }
        }
        // da($this->report_cols);
        // The number of rows to be displayed.
        if($_REQUEST['rows']) $rows = $_REQUEST['rows'];
        if(!$rows) {
            $this->rows = 6;
        } else {
            $this->rows = $rows;
        }
        
        require(dirname(__FILE__)."/../html/form_customize_report.phtml"); 
    }    
    
    /**
    * Show headers for column groups
    *
    * @version 1.0 Jonah B  1/30/15 11:52 AM
    */
    function showTableHeader() {
        ?><tr id="acReportHeaderDisplay"><?
        foreach($this->header_cols as $hc => $hcols) {
            // da($hcols);
            if($hcols['title']==$this->lastHeaderGroupTitle) unset($hcols['title']);
            ?><td class="acReportHeaderDisplayColGroupHeader" <? if($hcols['cols']) { ?>colspan="<?= $hcols['cols'] ?>"<? } ?> align="center" <? if($hcols['title']) { ?>style="background-color:#EEE"<? } else { ?>style="border-bottom: none"<? } ?>>
                <?= camelcap($hcols['title']); ?>
            </td><?
            $this->lastHeaderGroupTitle = $hcols['title'];
        }
        ?></tr><?
    }
}



/**
* Reports that show data over time in a table format.
*
* @version 1.0 Jonah  12/22/08 1:22 PM
* @package ActiveCore
* @subpackage ReportingEngine
*/
class ACTrendReport extends ACReport {
    /**
    * Array of columns in this report, loads from database.
    */
    var $columns = array();
    var $column_headers = array();
    /**
    * Columns for a given report, sometimes a sub-report
    */
    var $report_cols = array();
    
    function __construct($report_id=1) {
        global $session_user;
        // Feel free to kill the =1 after hotseat 2008. JB  9/2/08 9:58 AM

        /*
        * Reports are built on the idea of columns.  You simply specify a list of
        * survey questions, a survey, or a DBOS class/field
        * There are 70 fields that hotseat needs to track. JB  9/2/08 2:43 AM
        * @link http://spreadsheets.google.com/a/greenpeace.org/ccc?key=pdXxDQ6mXpAAGIF4kMOEwSw&inv=jbaker@greenpeace.org&t=1957436349072589678&guest
        */

        $this->loadRecord($report_id);

        $this->view = $this->_default_view;
        if($_REQUEST['view']) $this->view = $_REQUEST['view'];

        $this->use_goals = true;
        
        // Get the columns for this report
        $sql = "
            SELECT
                *
            FROM report_to_column rtc
                JOIN report_column AS c USING (column_id)
            WHERE report_id = $this->id
            ORDER BY rtc.sequence
        ";
        $result=acquery($sql);
        if($num=mysql_num_rows($result)) {
            while($row=mysql_fetch_assoc($result)) {
                $this->columns[] = $row;
            }
        } else {
            // This report has no columns
            // echo $sql;
        }
    }


    /**
    * Show the report
    *
    * @returns true if columns or children are found, false if neither are found.
    *
    * @version 1.1 SJP  01/07/2010 02:06 AM Added select multiple chapters
    * @version 1.0 Jonah  9/2/08 3:02 AM Feel free to make this smarter in the future.
    * 
    */
    function showTable($opts=false) { 
        global $zdb_report, $session_user, $rows;
        $debug = false;
        if($debug) echo "showTable has begun";
        $fields = array('chapter_id','chapter_archived');
        if($fields) {
            foreach($fields AS $field) { 
                $$field = $_REQUEST[$field];
            }
        }
        $this->chapter_id = $_REQUEST['chapter_id'];
        
        // Temporarily assume that a parent can have no columns.  JB  9/2/08 2:58 AM
        // @todo - stop assuming that a parent can have no columns.  JB  11/20/08 3:43 PM

        // Check for children reports/sections.
        // @todo - make this truly hierarchical. JB  11/20/08 3:46 PM

        $sql = " SELECT * FROM report WHERE parent_id = $this->id ORDER BY sequence";

        $result=acquery($sql);

        // Show the children.
        if($num=mysql_num_rows($result)) {
            ?><h1 id="child_report_title"><?= $this->title ?>
            <? if($chapter_id=$_REQUEST['chapter_id']) {
                $chapter_name = Chapter::get_name($chapter_id);
                ?> for chapter <?= $chapter_name ?><?
            }
            ?></h1><?
            while($row=mysql_fetch_object($result)) {
                $r = new ACTrendReport($row->report_id);
                $r->showTable();
                ?><p></p><?
            }
            return $true;
        }

        if(!count($this->columns)) {
            show_errors("No columns found for $this->title.");
            return false;
        } else {
            $this->col_count = count($this->columns);
        }

        $this->showFilters(array("chapter_id"=>$chapter_id));

        
        if($this->report_cols && is_array($this->report_cols)) {
            ?><table id="showTableWrapperTable"><tr><?
            foreach($this->report_cols as $table_id => $cols) {
                $total = "";
                $no_total = array();
                ?><td>
                <table class="list" id="table_<?= $table_id ?>" border="0">
                    <tr class="warning">
                        <td align="center" valign="center" class="reportTopRowHeader" style="margin-top:4px;background-color:#FFCC66;" colspan="<?= $this->col_count ?>">
                            <h2 style="margin:0"><?= $this->title ?>
                                <? if(is_numeric($chapter_id[$table_id])) { ?><br />
                                <a id="reportChapterTitle" href="/admin/chapter/admin_chapter.php?action=chapter_summary&chapter_id=<?= $chapter_id[$table_id] ?>"><?= Chapter::get_name($chapter_id[$table_id]) ?></a><? } ?>
                            </h2>
                        </td>
                    </tr><?
                $this->header_cols = array();
                $header_display = false;
                foreach($cols as $count=>$c) {
                    $count_headers++;
                    if(!in_array($c->title,$this->_hideFields)) {
                        if($c->header) {
                            $header_display = true;
                            $check_header = $count_headers - 1;
                            if($c->header ==  $this->header_cols[$check_header]['title']) {
                                $this->header_cols[$check_header]['cols']++;
                                $count_headers--;
                            } else {
                                $this->header_cols[$count_headers]['title'] = $c->header;
                                $this->header_cols[$count_headers]['cols'] = 1;
                            }
                        } else {
                            $this->header_cols[$count_headers]['title'] = "";
                            $this->header_cols[$count_headers]['cols'] = 1;
                        }
                    }
                }
                    
                if($header_display) {
                    $this->showTableHeader();
                }
                ?><tr id="acReportShowTableLine456"><?
                foreach($cols as $count=>$c) {
                    if(!in_array($c->title,$this->_hideFields)) {
                        $column_key = "column_".$this->report_id."_".$count;

                        $view_link = "";
                        if($c->survey_id) {
                            $view_link = "/admin/people/survey/admin_survey.php?action=response_list&survey_id=".$c->survey_id;
                            $viewTitle = "View Survey ";
                        } elseif($c->admin_url) {
                            $view_link = $c->admin_url;
                        }
                        if($viewTitle) $viewTitle = "View";
                        ?><td><?= camelcap($c->title); ?>
                            <span class="caption"><?= acToggle($column_key."_".$table_id,""); ?>
                                <div title="<?= $viewTitle ?>" id="<?= $column_key ?>_<?= $table_id ?>" style="display:none;">
                                    <?= $c->ajaxField("description"); ?>
                                    <br /><? if($view_link) { ?><a href="<?= $view_link ?>"><?= $viewTitle ?>&nbsp;&gt;</a><? } ?> <?= $c->summary_link(array("title"=>"Edit Column")); ?>
                                </div>
                            </span>
                        </td><?
                    }
                }
                ?>
                </tr><!-- end ac report line 480 -->
                
                <?php
                $this->showTableBody($cols);   
                ?>
                </table>
            <div class="caption">Showing <b><?= $this->col_count ?></b> columns in <?= $this->title ?>.
            <a href="/admin/report/admin_report.php?action=summary&table=report&report_id=<?= $this->id ?>">&gt;</a>
            </div><br />
        </td>
    </tr>
                <?
            }
            ?>
</table><?
        } else {
            ?><span class="alert">No data for $this->report_cols</span><?php
        }
    }
    
    /**
    * @version 1.0 Jonah B  3/23/15 2:48 PM
    */
    public function showTableBody($cols=false) {
        global $data;
        if(!$cols) {
            // $cols = $this->report_cols;
        }
        $row = 0;
        while($this->rows >= $row) {
            ?><tr id="showTableBody"><?
                foreach($cols as $cid => $c) {
                    if(!in_array($c->title,$this->_hideFields)) {
                        if($c->title == "Date") {
                            $no_total[] = $cid;
                        }
                        // da($c);
                        $value = $c->showCell($row,array("table_id"=>$table_id,'chapter_id'=>$chapter_id));
                        $total[$cid] = $total[$cid] + $value;
                        $this->data[$row][$cid] = $value;
                        $data = $this->data; // test this method
                    }
                }
            ?></tr><?
            if($this->use_goals) {
                ?><tr class="odd goalRow"><?
                    foreach($cols as $c) {
                        if(!in_array($c->title,$this->_hideFields)) {
                            if($this->useGoals) $c->showGoalCell($row);
                        }
                    }
                ?></tr><?
            }
            $row++;
        }
        if($this->showFooterTotalRow) {
            if($total && is_array($total) && is_array($no_total)) {
                ?><tr style="background:#B4B4B4" class="tableRowFooterTotals"><?
                foreach ($total as $nid => $num) {
                    ?><td align="right"><?
                        if(!in_array($nid,$no_total)) {
                            ?><?= $num ?><?
                        }
                    ?></td><?
                }
                ?></tr><?
            }    
        }
    }
} 


// end ACReport class.

/**
* @package ActiveCore
*/

class ReportColumnTypeTable extends DBOS {
    var $_db = REPORT_DB;
    var $_table = "report_column_type";
    var $_idField = "report_column_type";

    function __construct() {
        $this->loadTable();
    }
}

/**
* @package ActiveCore
*/

class ReportColumnType extends ReportColumnTypeTable {

    function __construct($id) {
        $this->loadRecord($id);
    }
}
/**
* @package ActiveCore
*
*/
class ChapterReport extends ACTrendReport {
    function __construct($report_id) {
        parent::__construct($report_id);
        global $session_user;
        if($session_user->chapter_id>0) {
            $this->chapter_id = $session_user->chapter_id;
        } elseif($_REQUEST['chapter_id']) {
            $this->chapter_id = $_REQUEST['chapter_id'];
        }
    }
}

/*
* @version 1.0 jonah  8/25/08 4:12 PM -  Make this smarter later.
* @todo - Move this to the chapter class.
*/

function getChapterId() {
    global $session_user;
    if($session_user->chapter_id) return $session_user->chapter_id;
  
    return $_REQUEST['chapter_id'];
}