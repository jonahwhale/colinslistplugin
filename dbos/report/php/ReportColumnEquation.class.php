<?php
/**
* @license GPL
* @version $Id: ReportColumnEquation.class.php 8329 2015-03-31 22:12:02Z jbaker $
* @package ActiveCore
* @abstract
*/


/*
* @class ReportColumnEquation
* @creator Jonah Baker  9/30/08 9:07 AM
* Supports math: +
* Needs in addition: - / * ()
*/
class ReportColumnEquation extends ReportColumn {
    var $type = "equation";
    /**
    * Example: 1+2+3 with the numbers as the column ids
    */
    var $type_value;
    /*
    * HTML output for this Row column type. 
    * Currently supports + and / only, with only two columns.
    */
    function showCell($num) {
        global $data;
        
        $debug = false;
        $total = 0;
        ob_start();
        $this->cell_key = $this->column_id."_".$num;
        
        if($type_value=$this->type_value) {
            // We need to replace a string like (5+28)/93 with the values of those column ids.  JB  10/1/08 1:40 PM
            $symbols = array("+","/","-","(",")");
            $prep = str_replace($symbols,":",$type_value);
            $parts = explode(":",$prep);
            // $parts = preg_match('/[^0-9.,]/',$type_value);
            rsort($parts);
            // OK, we have the column ids, and now we need the values.
            foreach($parts as $key=>$col_id) {
                $col_id = str_replace("C","",$col_id);
                if(is_numeric($col_id)) {
                  $colObj = ReportColumn::factory($col_id);
                  $value = $colObj->getCellValue($num);
                  $values[$key] = $value;
                  $columns[$key] = "C".$col_id;
                }
            }
            $equation = str_replace($columns,$values,$type_value);
            @eval('$total = '.$equation.";");
            if(!$total) $total = "N/A";
        } 
        elseif($this->columns) 
        { 
            // do nothing for now
            $cols = explode("+",$this->columns);
            foreach($cols AS $col) {
                if($debug) da($data[$num][$col]);
                $total = $data[$num][$col] + $total;
            }
            $this->cell_key = $num."_".$this->columns;
            
        } 
        else 
        {
            $warning = "You must set the Type Value field for equation column types.";
        }
        
        $this->startCell();
        $total = substr($total,0,4);
        ?>
        <?= $total ?> 
        
        <?php if($this->type_value) { ?>
            <?= acToggle("equation_info_".$this->cell_key,""); ?>
            <span style="display:none;" id="equation_info_<?= $this->cell_key ?>" class="caption">
            Equation: Column <?= $this->type_value ?><br />
            <? da($equation); ?>
            </span><?
        } 
        if($warning) {
          ?><span class="warning"><?= $warning ?></span><?
        }
        $this->endCell();
        
        echo ob_get_clean(); 
    }
    
}
