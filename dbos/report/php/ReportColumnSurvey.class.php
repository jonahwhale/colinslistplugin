<?php
/**
* @license GPL
* @version $Id: ReportColumnSurvey.class.php 8337 2015-04-09 23:30:21Z jbaker $
* @package ActivistCore
*/

/**
 * Survey response rates or survey questions.
 *
 * Maybe this should only be survey questions.  Technically response rates would be
 * a class, not a survey.  JB  9/3/08 12:54 PM
 *
 * @class ReportColumnSurvey
 * @package ActivistCore
 * @subpackage ReportingEngine
 * @author Jonah  9/3/08 12:54 PM
 */

class ReportColumnSurvey extends ReportColumn {
    /**
    * Because of factory construction this is the array of the db row.  JB  12/22/08 6:00 PM
    */
    function __construct($opts) {
        $debug = false;
        $this->_dbValues=$opts;
        parent::__construct($opts);
        if($this->question_title && !$this->title) {
            $this->title = $this->question_title;
        }
        if($opts['question_title']) $this->question_title = $opts['question_title'];
        // If there is no question title, then assume it is the same as the column title.
        if($this->goal_survey_id && !$this->question_title) {
            $this->question_title = $this->title;
        }

        // Determine the survey id

        if($this->survey_stub) {
            $this->surveyObj = Survey::get_by_tag($opts['survey_stub']);
        } elseif($this->survey_id) {
            $this->surveyObj = new Survey($this->survey_id);
        }

        // Determine the question id
        if($this->surveyObj) {
            if($this->question_id = $this->surveyObj->getQuestionByTitle($this->question_title)) {
                if($this->question_id) {
                    $this->questionObj = new SurveyQuestion($this->question_id);
                    if($debug) echo "<br />questionObj for $this->question_title created via question_id:".$this->question_id;
                } else {
                    
                }
                
            } elseif($this->question_id = $this->surveyObj->getQuestionByType('Hours')) {
                if($debug) echo "<br />Tried to create via type: ".$this->question_id;
                $this->questionObj = new SurveyQuestion($this->question_id);
                
            } else {
                $this->_errors[] = "Question could not be found for this manual survey question.";
            }
        }

        // Determine the response?
        if(is_object($this->surveyObj)) {
            $response = new SurveyResponseQuestionInputTable($this->question_id);
            $response->question_id = $this->question_id;
            
            // da($response->question_id);
            $this->object = $response;
            // da($this->object);
        }
        
        $this->field = 'response_input_value';
        // da($this);
        
    }

    /**
    * @version 1.0 Jonah B  3/30/15 3:02 PM
    */
    function makeHeaderCell() {
        if($this->hidden) $style = "style='display:none;'";
        $divName = 'showHeaderDetail'.$this->survey_stub.''.$this->question_id;
        ob_start();
        // da($this);
        ?><td class="makeHeaderCell">
            <div class="makeHeaderCellContent" <?= $style ?>>
            <?= $this->title ?> 
            <?= acToggle($divName,''); ?>
            <div style="display:none;" id="<?= $divName ?>">
            <?php if(is_object($this->surveyObj)) { ?>
                <?= $this->surveyObj->summary_link(); ?>
                <?php if(is_object($this->questionObj)) { ?>
                <br /><?= $this->questionObj->summary_link(); ?>
                <? } ?>
            <? } ?>
            </div>
        </div>
        </td><?php
        return ob_get_clean();
    }
    
    /**
    * author Jonah  12/22/08 7:50 PM
    */
    function getCellValue($row) {
    
        $sql = "SELECT response_input_value FROM response_input_value WHERE ";
       return $this->showGoalCell($row,array("question_title"=>$this->question_title,"get_value"=>true));
    }

    /**
    * Show the value from the survey question.
    *
    * @returns Nothing
    */
    function showCell($row) {
        $this->view = $this->getView();
        if(is_object($this->object)) {
            $value = $this->object->getTrendCell($row,'response_input_value');
        }
        // da($this->object);
        // A little bit backwards, name functions better later.  JB  9/3/08 1:05 PM
        // echo $this->showGoalCell($row,array("question_title"=>$this->question_title));
        return parent::showCell($row);
        
    }

    /*
    * @creator Jonah  9/30/08 8:33 AM
    * The url variables to hand over in the link associated with the number shown in this column.
    */
    function urlVars($var=false) {
        $vars = "?survey_id=".$this->object->survey_id."&action=survey_summary&row=".$this->row;

        return $vars;
    }

    function toHTML($opts=false) {
        global $session_user;
        $debug = false;
        // $this->adminActionCell($this->_dbValues);
        parent::toHTML();
        if($debug) echo $this->view;
        // $this->adminActionCell($this->_dbValues);
        ?>Survey Info: <?
        if($this->surveyObj) {
            echo $this->surveyObj->summary_link();
        }
        if($this->questionObj) {
            echo "<br />Question info:";
            echo $this->questionObj->summary_link();
        } else {
            ob_start();
            echo "The question '".$this->question_title."' could not be found in survey '".$this->surveyObj->title."'! ";
            ?><a href="<?= $this->surveyObj->_controller
                ?>?action=survey_form&question_id=new&question_label=<?= urlencode($this->question_title);
                ?>&question_type=numeric&survey_id=<?= $this->surveyObj->id ?>">Please create it</a><?
            $error = ob_get_clean();
            show_errors($error);
        }

        // Show all reports
        // $rep = new ACReportTable();
        // $rep->showTable();
    }

}