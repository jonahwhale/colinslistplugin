<?php
/**
 * The 'date' cell that shows what week/day/month/etc we are looking at in that row.
 *
 * @version 1.0 Jonah  11/21/08 9:42 AM
 */

class ReportColumnDate extends ReportColumn {
    function __construct($opts=false) {
        parent::__construct($opts);
        $this->title = "Date";
    }
    
    /**
    * @version 1.0 Jonah B  3/31/15 3:44 PM
    */
    function showCell($num) {
        $this->startCell(array("nobreak"=>true));
        
        $this->view = $this->getView();
        
        $range = $this->rowToDateRange($num,$this->view);
        switch($this->view) {
            default:
            case"daily":
                $int_type = "day";
                break;
            case"monthly":
                $int_type = "month";
                break;
            case"quarterly":
                $int_type = "quarter";
                break;
            case"ytd":
            case"yearly":
                $int_type = "year";
                break;
            default:
            case"weekly":
                $int_type = "week";
                break;
        }
        ?>Start: <?= $range['date_starting']; ?><br/><?
        if($num == 0) {
            if($int_type == "day") { ?>to<? } else { ?>current <? } ?><?= $int_type ?><?
        } else {
            ?><?= $num ?> <? if($num == 1) { ?><?= $int_type ?><? } else { ?><?= $int_type ?>s<? } ?> ago<?
        }
        $this->endCell();
    }
}