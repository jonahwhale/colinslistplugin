<?php

class testGrassrootsReport extends test {

    class enterHours() {
        $this->getSurvey("fieldHours");
        
    }
    
    class checkAttributes() {
        $this->checkAtt("FT1","1-Very Close, Constantly Involved","Volunteer Power");
        $this->checkAtt("FT2","2-Regularly involved, has been active in past six months");
        $this->checkAtt("FT3","3-In our network but has not volunteered in past 6 months");
        $this->checkAtt("GWV","Greenwire Volunteer");
    }
}