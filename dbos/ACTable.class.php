<?php
/**
 * Auto-generated tables for DBOS classes.
 * Currently table related function are sprinked around, mostly in DBOS, should be collected here.
 *
 * @license GPL
 * @version $Id: $
 * @package ActiveCore
 * @subpackage DBOS
 *
 */

/**
* Required.
*/
require_once(dirname(__FILE__)."/Browse.class.php");

/**
 * Auto-generated tables for DBOS classes.
 * Currently table related function are sprinked around, mostly in DBOS, should be collected here.
 *
 * To activate a general set of tables and editing forms use:
 * $table = new ACTableTable();
 * $table->adminPage();
 *
 * @author JB  9/4/08 2:47 PM
 * @version 1.1 - Changed to grid edit for ajax table editing by default.  JB  8/5/11 1:26 PM
 * @version 0.1 JB  10/7/08 11:23 AM
 * - Now supports ajax grid editing for most field types.
 */

class ACTable {
    /**
    * For mapping features.  Set defaults.
    */
    var $_useBrowse = true;
    var $_mapWidth = 920;
    var $_mapHeight = 555;
    /**
    * This will make tables ajax editable
    */
    var $_gridEdit = true;
    var $_allowDelete = true;
    /**
     * Show or hide the edit/delete actions with the summary page output (also hides the 'create new ...' link at top) - JA 8/5/13
     */
    var $_readOnlySummary = false;
    /**
     * Title to use instead of table name
     */
    var $_customTableTitle = '';
    
    public $_useDataTables = false;
    public $_dataTablePath = false;
    /**
    * Hide these fields
    */
    var $_hiddenFields = array();
    
    /**
    * Helps with bootstrap compatibility
    */
    var $_active = 'active';
    var $_useUUID = false;

    /*
    * @alias to _loadTableInfo();
    * @author JB  8/15/08 10:03 PM
    */

    public function loadTable($table=false) {
        if(!$this->_titleField) {
            $this->_titleField = $this->_idField;
        }
        return $this->_loadTableInfo();
    }

 

    /**
    * @todo - Consolidate this into a single call for a db table, so you don't do so many calls. JB  12/18/08 9:02 PM
    * @returns Array describing foreign table/key relationship of this field, or false.
    */
    public function getRelation($field) {
        global $zdb_schema;
        if($this->_useCache) {
            $cache = new ACCache();
            $cache->key = $this->_db."_".$this->_table."_".$field;
        }
        // echo $cache->key."<br />";
        $debug=false;
        if(!is_object($zdb_schema)) return false;
        // now see if we have some phpmyadmin relational data

        $sql = " SELECT * FROM pma_relation
        WHERE master_db='{$this->_db}' AND master_table = '{$this->_table}' AND master_field = '{$field}' ";

        if($use_cache) {
            $result = $cache->get();
            if($result!==false) {
                 // hit
            } else {
                 $result = $zdb_schema->fetchAll($sql);
                 $cache->addValue($result,60*10);
            }
        } else {
            $result = $zdb_schema->fetchAll($sql);
        }
        if($num=count($result)) {
            $row=$result[0];
            return $row;
        } else {
            return false;
        }

    }

    /**
    * @version 1.1 JB  7/1/13 9:12 AM - Use dbo, start with preview
    * @version 1.0 JB  2/22/12 1:17 PM
    */
    function forceUniqueIndex($opts=false) {
        global $db;
        if($opts && is_array($opts)) extract($opts);
        $indexName = "unique_".$this->_table;
        if($_REQUEST['preview']=="false") {
            $preview = false;
        } else {
            $preview = true;
        }
        if(is_array($this->_uniqueFields)) {
            $fields = $this->_uniqueFields;
        } else {
            show_errors("Must define _uniqueFields to use this function");
            return false;
        }
        $sql = " 
            SELECT max(".$this->_idField.") AS last_id, count(*) AS count, 
        ";
        $sql .= implode(",",$fields);
        $sql .= "
            FROM $this->_table ";
         
        $sql .= "
            GROUP BY ".implode(",",$fields)."";
        $sql .= " HAVING count > 1 ";
        $sql .= " ORDER BY count DESC";
            
        $keepers = $this->_dbo->fetchAll($sql);
        
        echo "<br />".$sql;
        $kCount = count($keepers);
        if($c=count($keepers)) {
            ?><br />Found <?= $kCount ?> dups <?php
            da($keepers);
            foreach($keepers AS $keeper) {
                $sql = "DELETE FROM $this->_table WHERE 1=1 ";
                foreach($fields AS $field) {
                    $sql .= " AND $field = '".mysql_escape_string($keeper[$field])."' ";
                }
                $sql .= " AND $this->_idField NOT IN (".$keeper['last_id'].") ";
                echo "<br />".$sql;
                if(!$preview) $this->_dbo->query($sql);
            }
        }
        
        $sql = "ALTER TABLE  `".$this->_table."` IF EXISTS DROP INDEX  $indexName ";
        echo $sql;
        if(!$preview) $this->_dbo->query($sql);
        
        $sql = "ALTER TABLE  `".$this->_table."` ADD UNIQUE  $indexName (  ".implode(",",$fields)." )";
        echo $sql;
        if(!$preview) $this->_dbo->query($sql);
    }    

    /**
    * Add and where clauses to table searches
    *
    * @version 1.2 accept strings
    * @version 1.1 modify likes and = based on field type being searched. JB  2/25/11 11:27 AM
    * @version 1.0 Jonah  2/5/09 11:18 AM
    */
    public function addFilter($pairs,$opts=false) {
        if(is_string($pairs) && is_string($opts)) {
            $pairs = array($pairs=>$opts);
        }
        if(!is_array($pairs)) {
            show_error("addFilter expects an associative array of filters");
            return false;
        }
        if($opts && is_array($opts)) extract($opts);

        foreach($pairs AS $field=>$value) {
            $value = mysql_escape_string($value); // Jonah B  10/24/12 4:44 PM
            switch($this->tbl[$field]['type']) {
                case"text":
                case"varchar":
                    $value = str_replace("*","",$value); // already wildcareded
                    $this->_where .= " AND $field LIKE '%".$value."%' ";
                    break;
                default:
                    if($this->contains("*",$value)) {
                        $value = str_replace("*","%",$value);
                        $this->_where .= " AND $field LIKE '".$value."' ";
                    } else {
                        if($value) {  // Jonah B changed blank value handling  12/12/13 12:33 PM
                            if($value=="blank") unset($value);
                            $this->_where .= " AND $field = '$value' ";
                        }
                    }
            }
        }
        if($debug) da($this->_where);
    }

    /*
    * Show table of records and allow admin user to view and edit them
    *
    * - opts
    *   - where - raw sql appended to select statement, expects " AND field = '$value' "
    *   - join - raw sql to be appended to the join section. EX.
        $opts = array(
            "where"=>" AND rtc.report_id = $this->id ",
            "join"=>" JOIN report_to_column AS rtc USING(column_id) "
            "filterFields" => array of field names
        );
    *
    * @version 1.3 - Limit table rows paging display with $this->_tableListSize
    * @version 1.2 - Added sort compatibility -  JB 8/11/11 10:49 AM
    * @version 1.1 JB - Integrated column where_sql and output filter data.
    * @version 0.1 Alpha JB  8/4/08 6:22 PM
    */

    public function showTable($opts=false) {
        global $wpdb;
        if(!is_object($this->_dbo)) {
            global $zdb_action;
            $this->_dbo =& $zdb_action;
        }
        if(is_array($opts)) extract($opts);
        // Limits date, weekly monthly etc, works with row.  Ex weekly row 2 means two weeks ago
        if(!$view) $view = $_REQUEST['view'];
        if(!$order_by) $order_by = $_REQUEST['sort'];
        if(!$limit) $limit = 100;
        if($this->_gridEdit==true) $ajax = true;

        if($date_field) {
            $this->_dateField = $date_field;
        } elseif($_REQUEST['date_field']) {
            $this->_dateField = $_REQUEST['date_field'];
        }

        // If this table is being viewed as a drilldown from the reporting engine columns,
        // get the where from the column.
        if($_REQUEST['column_id']) {
            $column = new ReportColumn($_REQUEST['column_id']);
            $filters['column'] = $column->title;
        }

        // The date range
        if(isset($_REQUEST['row'])) {
            $row = $_REQUEST['row'];
            $date_range = $this->rowToDateRange($row,$view);
            $filters['dates'] = " from ".$date_range['date_starting']." to ".$date_range['date_ending'];
        }

        $limit_chapter = $this->limitChapterSql();

        $sql = " SELECT `{$wpdb->prefix}{$this->_table}`.* ";
        $sql .= $limit_chapter['select'];
        $sql .= " FROM `{$wpdb->prefix}$this->_table` ";
        $sql .= $limit_chapter['join'];
        $sql .= $join;
        $sql .= " WHERE 1";
        if($this->_dateField && $_REQUEST['year']) {
            $sql .= " AND YEAR($this->_dateField) = '".mysql_escape_string($_REQUEST['year'])."' ";
        }        
        $sql .= $limit_chapter['where'];
        $sql .= $date_range['where'];
        
        if($_REQUEST['dateStart'] && $this->_dateField) {
            $sql .= " AND $this->_dateField >= '".date("Y-m-d",strtotime($_REQUEST['dateStart']))."'";
        }
        if($_REQUEST['dateEnd'] && $this->_dateField) {
            $sql .= " AND $this->_dateField <= '".date("Y-m-d",strtotime($_REQUEST['dateEnd']))."'";
        }
        
        if(is_array($filterFields)) {
            foreach($filterFields AS $filterField) {
                if($_REQUEST[$filterField]) {
                    if($_REQUEST[$filterField]=="blank") unset($_REQUEST[$filterField]);  // JB changed blank value handling.   12/12/13 12:30 PM
                    $sql .= " AND `$filterField` = '".mysql_escape_string($_REQUEST[$filterField])."' ";
                    $filters[$filterField] = " $filterField ID: ".$_REQUEST[$filterField];
                }
            }
        }
        $sql .= $this->_where;

        if($column->where_sql) $sql .= $column->where_sql;

        if($where) $sql .= $where;
        
        if($order_by) {
            $sql .= " ORDER BY {$order_by} ".$this->escape($_REQUEST['sortby'])." ";
        } elseif($this->_dateField) {
            $sql .= " ORDER BY $this->_dateField DESC ";
        }

        $this->_tableSql = $sql;
        if($sql_only) return $sql;
        
        if($this->_useBrowse) {
            $browse = new Browse($sql,$this->_dbo,array("db"=>$this->_db,"rows"=>$this->_tableListSize));
    
            foreach($_GET AS $f=>$v) {
                $browse->add_var("&{$f}={$v}");
            }
            $browse->add_var("&table=".$this->_table);
            $browse->sort_variable .= "&table=".$this->_table;
            
            $sql = $browse->sql;
        }
        

        if(is_object($this->_dbo)) {
            $rows=$this->_dbo->fetchAll($sql);
        } else {
            $zdb_handle = "zdb_".$this->_db;
            global $$zdb_handle;
            if(is_object($$zdb_handle)) {
                // For some reason this does not work.  JB  3/18/11 10:09 AM
                $$zdb_handle->fetchAll($sql);
                echo"<b>You must define _dbo for this class</b>";
            } else {
                
                $rows=$wpdb->get_results($sql,ARRAY_A);
            }
        }

        // Backwards compatibility for GreenpeaceUSA.  JB  10/7/08 4:30 PM
        if(is_array($this->hidefields)) {
            $hidefields=$this->hidefields;
        } elseif(is_array($this->_hiddenFields)) {
            $hidefields = $this->_hiddenFields;
        } else {
            $hidefields = array();
        }
        // Activate some graphing variables.
        $graphf = array(); $data = array();
        if($this->_graphField) {
            $graphf['label'] = $this->_dateField;
            $graphf['point'] = $this->_graphField;
        }
        $i=0;
        if(!$this->_tableTitle) {
            $this->_tableTitle = $this->camelcaps($this->_table);
        }

        if(is_array($limit_chapter)) {
            $chapter_id = $limit_chapter[chapter_id];
            $chapter_name =  Chapter::get_name($chapter_id);
            $filters[chapter] = " in chapter $chapter_name ";
        }
        $this->_rowData = $rows;
        if($this->_useDataTables) {
            // @todo make this better
            if($this->_dataTablePath) {
                require($this->_dataTablePath);
            } else {
                require_once(dirname(__FILE__)."/html/dataTable.phtml");
            }
        } else {
            if($totalRows=count($rows)) {
                if($this->_useBrowse) { ?>
                
                <span class="acShowingRecords">
                    Showing <b><span id="records_shown"><?= $totalRows ?></span></b> 
                    <?= $this->_tableTitle ?>s
                <? if(is_array($limit_chapter)) {
                    ?>for chapter <b><?= $chapter_name ?></b>
                <? } ?>
                <?php if(is_array($filters)) {
                    foreach($filters AS $field=>$desc) {
                        echo "[ ".$field.": ".$desc." ] ";
                    }
                } ?>
                </span>
                <? } ?>
                <?php if($this->_useBrowse) { ?>
                <?= $browse->show_nav_lite(); ?>
                <? } ?>
        <form action="<?= $PHP_SELF ?>" method="get" id="dbosFilterActionForm">
            <input type="hidden" name="table" value="<?= $this->_table ?>">
            <input type="hidden" name="action" value="bulk_edit">
            
            <?php if($this->_allowBulkEdit!==false) { ?>
            <select name="name">
                <option value="bulkDelete">Delete</option>
                <option value="bulkEdit">Edit</option>
            </select>
            <input type="submit" value="Bulk Edit">
            <? } ?>
    
            <?php
                foreach($rows AS $currentRow => $row) {
                    $i++; $i % 2 ? $row_class = "odd" : $row_class = "even";
                    extract($row);
                    // $debug = false;
                    if($debug) $this->dump($row);
                    if($i==1) { // do header row 
                        $this->showTableHeader($row,$browse);
                    }
                    
                    if($ajax) {
                        if(is_array($this->_idField)) {
                            $ajax = false;
                        } else {
                            if(is_object($row)) {
                                $this->loadRecord($row->{$this->_idField});
                            } else {
                                $this->loadRecord($row[$this->_idField]);
                            }
                        }
                    }
                    $this->showRow($row);
                }
                $this->showTableFooter();
                ?>
    
                <center><? $browse->show_nav(); ?></center>
                <?php
                if(count($data)) {
                  $g = new ACGraph($data,$this->_table."_ACTable_");
                  $g->width = "800";
                  $g->height = "300";
                  $g->render();
                }
            } else {
                ?><p id="noRecordsFound"><?php
                if($this->_noRecordsMessage) {
                    echo $this->_noRecordsMessage;
                } else {
                    ?>No records found in <?= $this->_tableTitle ?> table <?php
                }
                ?></p><?php
            }
            if(is_array($filters)) {
               ?><br /> for filters:<br /><?
               echo implode(",<br />",$filters);
            }
            if(!$_REQUEST['showSQL']) $sqlStyle="display:none;";
            ?><div class="table_debug_info" style="<?= $sqlStyle ?>"> SQL: <? $this->dump($sql); ?></div><?
            // da($this);
        }
    }
    
    /**
     * @version 1.0 Jonah B  3/30/15 11:27 AM
     */
    public function showTableHeader($row=false,$browse=false) {
	if(is_string($row)) {
		$row = explode(",",$row);
	}
        if(!$browse) $browse = new browse();
        
        require_once(dirname(__FILE__)."/html/tableHeader.phtml");
    }
    
    public function showTableFooter() {
        ?>
            </tbody>
            </table>
        <?php
    }
    
    /**
    * @version 1.0 Jonah B  7/6/15 12:41 PM
    */
    public function showTableRow($row=false) {
        $this->showRow($row);
    }
    
    /**
    * 
    */
    public function showRow($row) {
        if(!$row) {
            $row = $this->_dbValues;
        }
        
        ?>
        <tr class="<?= $row_class ?> ACTable.showTable.tr">
                <?php if(!$this->_hideActionCell) { ?>
                      <td 
                        nowrap 
                        class='acActionCell details-control'
                      >
                      <?php $this->adminActionCell($row); ?>
                      </td>
                  <?php } ?>
                  <? foreach($row AS $f=>$v) {
                        
                        if(!in_array($f,$this->_hiddenFields)) {
                            // Deal with fields that have relationships defined to other tables via phpmyadmin.
                            
                            if($f && $related=$this->tbl[$f]['rel']) {
                                $obj = DBOS::factory($this->tbl[$f][rel][foreign_table],$v);
                                if(is_object($obj) && method_exists($obj,"summary_link")) {
                                    $v = $obj->summary_link();
                                } else {
                                    // echo "Could not find object for "; da($related);
                                }
                            } elseif($f=="person_id") {
                                $p = new person($v);
                                $v = $p->summary_link();
                            } else {
                                // last ditch effort to relate
                                // $rel = $this->getRelation($f);
                                // da($rel);
                            }
                            if($f==$graphf['label']) $data[$i-1][label] = $v;
                            if($f==$graphf['point']) $data[$i-1][point] = $v;
                            ?><td class="acDataCell"><span id="<?= $f ?>_<?= $count ?>"><?php
                                if(!$this->_gridEdit || $related || $this->tbl[$f]['type']=="timestamp") { ?>
                                <?= $this->_adminShowCellValue($f,$v,$i,$row); ?>
                              <? } else {
                                    
                                    $ajaxable = $this->ajaxField($f,array("raw"=>true,"gridedit"=>true));
                                    if($ajaxable===false) { echo $v; } else { echo $ajaxable; }
                               } ?></span>
                              </td>
                       <? } ?>
                  <? } ?>
                </tr>
            <?php
    }

    /**
    * @version 1.0 Jonah B  2/18/14 12:12 PM
    */
    public function _jsTableHeader() {
        include(dirname(__FILE__)."/html/jsTableHeader.html");
    }
    

    /**
     * A way to override a cell in children
     *
     * @version 1.2 JA Oct 17 12:30 EDT 2012  - obscure any field named ccnumber (not all CCs are 16 digits)
     * @version 1.1 JB  11/18/11 10:24 AM - Added cc obscure
     * @version 1.0 JB  4/21/11 11:56 AM
     */

    public function _adminShowCellValue($field,$v,$count=false,$row=false) {
        // If it so much as looks like a cc number, obscure it.  JB  11/18/11 9:13 AM
        if(is_numeric($v) && strlen($v)==16) {
            $v = substr($v,12,4);
            $v = "XXXXXXXXXXXX".$v;
        }
        $debug = false;
        // echo("show_{$field}");
        if($debug){
            echo "_adminShowCellValue:".htmlentities($field).":$v:$count:$row";
            
        }
        
        
        $this->_fieldRelations = array_change_key_case($this->_fieldRelations);
        if(key_exists(strtolower($field),$this->_fieldRelations)) {
            
            $lookUp = $this->_fieldRelations[$field];
            $rel = $this->_parseRelation($lookUp);
            
            $objName = $rel['table'];
            if(class_exists($objName)) {
                $obj = new $objName($v);
               
                if($obj->_titleField) {
                    // $v = $obj->{$obj->_titleField};
                    $v = $obj->summary_link();
                }
            } else {
                // da($rel);
                $v = $this->_getRelatedRecord($rel,$v);
            }
        } elseif(method_exists($this,"show_{$field}")) {
            $functionName = "show_".$field;
            $v = $this->{$functionName}($v);
        } elseif(method_exists($this,str_replace("_","","show{$field}"))) {
            $functionName = "show".$field;
            $v = $this->{$functionName}($v);
        }
        // da($v);
        return $v;
    }
    
    /**
    * @version 1.0 Jonah B  10/31/12 1:13 PM - ActiveCore Greenpeace
    */
    public function _getRelatedRecord($rel,$value) {
        if(!$value) return false;
        /*
        $q = Doctrine_Core::getTable('User')
    ->createQuery('u')
    ->select('COUNT(p.id)')
    ->leftJoin('u.Phonenumber p')
    ->where('u.username = ?', 'jwage');
        */
        $sql = " SELECT `".$rel['select_field']."` FROM `".$rel['table']."` WHERE `".$rel['field']."` = ".mysql_escape_string($value);
        $value = $this->_getOne($sql);
       
        return $value;
    }
    
    /**
    * @version 1.0 Jonah B  5/2/16 12:47 PM
    */
    function _getOne($sql) {
        if(method_exists($this->_dbo,'getOne')) {
            $value = $this->_dbo->getOne($sql);
        } else {
            $out = $this->_dbo->fetchArray($sql);
            // $statement->execute();
            // $out = $statement->fetchArray();
            return $out[0];
        }
        
        return $value;
    }

    public function da($in) {
        ?><pre class="ac da"><?php
        print_r($in);
        ?></pre><?php
    }
    
   /**
    * Export all records to excel spreadsheet.
    *
    * @version 1.0 Jonah B  10/31/12 11:30 AM - Greenpeace ActiveCore
    */
    function exportRecords($opts=false) {
        global $session_user;
        $debug = false;
        if(is_array($opts)) extract($opts);
        require_once 'Spreadsheet/Excel/Writer.php';
        if($sql) {
            // Custom sql has been handed, make sure they aren't malicious.  Jonah B  10/31/12 11:41 AM
            if($this->contains("update",strtolower($sql)) || $this->contains("delete",strtolower($sql))) {
                return false;
            }
            // da($sql);
        }

        // Config fields
        // da($this);
        $tableFields = array_keys($this->tbl);
        $headers = array();
        foreach($tableFields as $field) {
            $label = str_replace("_"," ",$field);
            if(is_array($this->_fieldLabels) && in_array($field,array_keys($this->_fieldLabels))) {
                $label = $this->_fieldLabels[$field];
            }
            $headers[trim(strtolower($field))] = $label;
        }
        /*
        if($project_headers) {
            $headers = array(
                "bug_id" => "ID",
                "title" => "Title",
                "submitted_by" => "From",
                "assigned_to" => "To",
                "status" => "Status",
                "priority" => "Priority",
                "hours_est" => "Estimated Hours",
                "release_name" => "Release",
                "created" => "Created",
                "date_testing" => "Testing",
                "date_production" => "Needed by",
                "date_resolved" => "Completed"
            );
        } else {
            $headers = array(
                "bug_id" => "ID",
                "title" => "Title",
                "project_name" => "Project Name",
                "priority" => "Priority",
                "status" => "Status",
                "release_name" => "Release",
                "submitted_by" => "From",
                "assigned_to" => "To",
                "hours_est" => "Estimated Hours",
                "created" => "Created",
                "date_testing" => "Testing",
                "date_production" => "Needed by",
                "date_resolved" => "Completed"
            );
        }
        */
        if(!$sql) $sql = "SELECT * FROM $this->_table LIMIT 10000";
        if(!is_string($sql)) {
          show_errors("No sql string handed to sql_to_excel");
          return false;
        } else {
          $sql = stripslashes($sql);
        }
    
        $result=mysql_db_query($this->_db,$sql) or mysql_die($sql); if($debug) { da($sql); }
        if(!$num_rows=mysql_num_rows($result)) {
          show_errors("Empty query result, nothing to show for $sql");
          return false;
        }
        
        if(!$debug) {
            // Creating a workbook
            $workbook = new Spreadsheet_Excel_Writer();
            // sending HTTP headers
            $workbook->send( date("Y") . date("m") . date("d")."-".$num_rows."-".$this->_table."-export.xls");
            // Creating a worksheet
            $worksheet =& $workbook->addWorksheet('Export');
    
            $summary = "$num_rows records exported on " . date("m/d/Y") . " for ".$session_user->user_name;
            $worksheet->write(0, 0, $summary);
        }
    
        while($row=mysql_fetch_assoc($result)) {
            extract($row);
            
            $a++;
            if($a==1) {
                $b=0;
                if(!is_array($export_fields)) {
                    foreach($row AS $field=>$value) {
                        $export_fields[] = $field;
                    }
                }
                foreach($export_fields AS $field) {
                    if($headers[$field]) {
                        if($debug) {
                            da($headers[$field]);
                        } else {
                            $worksheet->write(1, $b, $this->camelcaps($headers[$field]));
                        }
                        $b++;
                    }
                }
                $a=2;
            }
            $b=0;
            if($debug) {
                da("<hr>");
            }
            $i=0;
            foreach($row AS $field=>$value) {
                $i++;
                $value = $this->_adminShowCellValue($field,$value,$i,$row);
                if(in_array($field,array_keys($headers))) {
                    if(in_array($field,array("created","date_testing","date_production","date_resolved"))) {
                        if($value && $value != "0000-00-00 00:00:00" && $value != "0000-00-00") { 
                            $value = hdate($value);
                        } else {
                            $value = "";
                        }
                    }
                    if($debug) {
                        da("<em>".$field." =</em> " . $value);
                    } else {
                        $worksheet->write($a, $b, $value);
                    }
                    $b++;
                }
            }
        }
    
        if(!$debug) {
            // Let's send the file
            $workbook->close();
        }
    }

    /**
    * @version 1.0 Jonah B  4/6/14 3:56 PM
    */
    public function _parseRelation($in) {
        da($in);
        $pts = explode('.',$in);
        if(count($pts)==2) {
                $out['table'] = $pts[0];
                $out['field'] = $pts[1];
        }
        
        if($this->contains(':',$field)) {
            list($field,$select_field) = explode(':',$pts[1]);
        } else {
            $field = $pts[1];
        }        
        return $out;
    }

    /*
    * Returns the name of the record, the title, or the person's name, or whatever.
    * This won't work for all objects, since there isn't always a good name to be returned.
    * 
    * @version 1.0 Jonah  8/30/08 7:13 PM
    */

    public function getRecordName($id) {
       if(!$this->_titleField) return $id;

       $sql = " SELECT $this->_titleField FROM $this->_table WHERE $this->_idField = '".mes($id)."' ";
       return $this->_dbo->fetchOne($sql);
    }

    static function arrayToUL($array) {
        ?><ul class="items"><?
        foreach($result AS $row) {
          extract($row); ?>
          <li class="item">
              <div class="name"><?= $id ?></div>
              <span class="informal"><?= $title ?></span>

          </li>
        <? } ?>
        </ul><?

    }

    /**
    * Show table of records for admin to edit
    */
    function showAdminTable($opts=false) {
        // $this->adminPageTop();
        if($this->_showFilter!==false) {
            // $this->showSearchFilter();
        }
        $this->showTable($opts);
    }
    
    /**
    * @version 1.0 JB  10/1/12 2:37 PM
    */
    /*
    function choose($default=false,$opts=false) {
        if($opts && is_array($opts)) extract($opts);
        $sql = "SELECT $this->_idField, $this->_titleField FROM $this->_table ";
        ?><select id="<?= $this->_idField ?>" name="<?= $this->_idField ?>">
            <option value="">&lt;none&gt;</option>
            <?php
            echo select_options($sql,$default);
        ?></select><?php
    }
    */
    
    public function showSuccess($in) {
        ?><div class='alert alert-success'><?= $in ?></div><?php
    }
    /**
    * @version 1.0 JB  2/25/11 11:31 AM
    */
    function showSearchFilter() {
        require(dirname(__FILE__)."/html/searchFilter.phtml");
        
        if(isset($_REQUEST['filterValue'])) {
            $this->addFilter(array($_REQUEST['filterField']=>$_REQUEST['filterValue']));
        } elseif($_REQUEST['filterBy']) {
            $this->addFilter($_REQUEST['filterBy']);
        }
        
    }

    function getRes($sql) {
        return acquery($sql,$this->_db);
    }


    /**
     * This is a table related function.  This is for tables that 99% of the time will be
     * filtered to a particular field.  Filters survey_response to a survey id,
     * to person_to_attribute to a particular attribute.
     *
     * @version 1.0  Jonah  8/19/08 12:45 PM
     */

    public function defaultFilter($id) {
        return false;
    }

    public function trendGraph($opts=false) {
        return $this->graph($opts);
    }

    public function showTrendGraph($opts=false) {
        return $this->graph($opts);
    }
    /**
    * Tinker with the sql to find the records belonging to a particular chapter.
    *
    * @returns array with chapter id and where sql
    *
    * @version SJP 01/07/2010 04:07 AM Allowed input of chapter_id
    */
    function limitChapterSql($chapter_id=false) {
        if(!$chapter_id && is_numeric($_REQUEST['chapter_id'])) {
            $chapter_id = $_REQUEST['chapter_id'];
        }
        
        // Some alpha chapter limiting stuff
        if(is_numeric($chapter_id)) {
            $out['chapter_id'] = $chapter_id;
            // we want to avoid limiting to chapter id twice.  I'm not sure
            // what the best way to do that is.  JB  10/7/08 4:28 PM
            if(!$this->contains("chapter_id =",$sql)) {
                if($this->tbl['chapter_id']) {
                    $where .= " AND ".$this->_table.".chapter_id = $chapter_id ";
                } else {
                    $where .= " AND chapter_id = $chapter_id ";
                }
            }
        }
        if($where) $out['where'] = $where;
        return $out;
    }

    /**
    * $row is an integer of time past.  Row 2 of a weekly chart will give the
    * date range from two weeks ago.
    * @returns array with sql and other date info
    *
    * @version 1.2 use precise date starting and ending instead of group - JB
    * @version 1.1 escape field names for tricky names like 'date' - JB  8/26/10 4:25 PM
    * @version 1.0 production - SQL range was 8 days because of extra =  JB  6/15/10 3:23 PM
    * @version 0.2 beta - supports month - JB  11/21/08 9:31 AM
    * @version 0.1 alpha experimental JB  8/25/08 1:13 PM
    */
    public function rowToDateRange($row,$view=false,$opts=false) {
        if($opts && is_array($opts)) extract($opts);
        if(!$view) $view = $this->_default_view;
        if(!$view) $view = "weekly";
        if(!$row) $row = 0;
        if($tableAlias) {
            $this->_tableAlias = $tableAlias;
        } else {
            $this->_tableAlias = $this->_table;
        }
        if($dateField) {
            $this->_dateField = $dateField;
        }
        // @todo - hourly.  JB  12/3/08 1:04 PM
        switch($view) {
            case"day":
            case"daily":
                if($date_starting) {
                    $date_starting = date("Y-m-d",strtotime($date_starting));
                } else {
                    $date_starting = date("Y-m-d",strtotime(date("Y").date("d")."-".$row." day"));
                }
                $date_ending = $date_starting;
                $date_formated = date("M d, Y",strtotime($date_starting));
                break;
            case"week":
            case"weekly":
                if($date_starting) {
                    $week_now = date('W',strtotime($date_starting));
                    $current_week = $week_now;
                    $new_year = date("Y",strtotime($date_starting));
                } else {
                    $week_now = date('W');
                    $current_week = $week_now - $row;
                    $new_year = date("Y");
                }
                if($current_week < 1) {
                    $new_year -= 1;
                    $test_day = 31;
                    do{
                        $totalWeeksInYear = date("W", mktime(0,0,0,12,$test_day,$new_year));
                        $test_day--;
                    }  while($totalWeeksInYear == 1);
                    $current_week = $totalWeeksInYear + $current_week;
                }
                $first_day = strtotime($new_year."-01-01");
                $is_monday = date("w", $first_day) == 1;
                $is_weekone = strftime("%V", $first_day) == 1;
                if($is_weekone) {
                    $week_one_start = $is_monday ? strtotime("last wednesday",$first_day) : $first_day;
                } else {
                    $week_one_start = strtotime("next wednesday",$first_day);
                }
                $start_weekday = $week_one_start+(3600*24*7*($current_week-1));
                $date_starting = date("Y-m-d",$start_weekday);
                $date_ending = date("Y-m-d", mktime(0,0,0,date("m",$start_weekday),date("d",$start_weekday)+6,date("Y",$start_weekday)));
                $date_formated = date("M d, Y",strtotime($date_starting));
                break;
            case"quarter":
            case"quarterly":
                $quarter_info = getQuaterlyRange($date_startin);
                if($row > 0) {
                    $current_month =  date("m",strtotime($quarter_info['start_date'])) - ($row * 3);
                    $current_quarter =  date("Y-m-d",mktime(0, 0, 0, $current_month, 01, date("Y")));
                    $quarter_info = getQuaterlyRange($current_quarter);
                }
                $date_starting = $quarter_info['start_date'];
                $date_ending = $quarter_info['end_date'];
                $date_formated = date("M",strtotime($quarter_info['start_date']))."-".date("M",strtotime($quarter_info['end_date']))." ".date("Y",strtotime($quarter_info['start_date']));
                break;
            case"ytd":
            case"year":
            case"yearly":
                if($date_starting) {
                    $year_now = date('Y',strtotime($date_starting));
                    $current_year = $year_now;
                } else {
                    $year_now = date('Y');
                    $current_year = $year_now - $row;
                }
                $date_starting = date($current_year.'-01-01');
                $date_ending = date($current_year.'-12-31');
                $date_formated = date("Y",strtotime($date_starting));
                break;
            default:
            case"month":
            case"monthly":
                if($date_starting) {
                    $month_now = date("m",strtotime($date_starting));
                    $current_month = $month_now;
                } else {
                    $month_now = date("m");
                    $current_month = $month_now - $row;
                }
                $date_starting =  date("Y-m-d",mktime(0, 0, 0, $current_month, 01, date("Y")));
                $date_ending = date("Y-m-t",strtotime($date_starting));
                $date_formated = date("M Y",strtotime($date_starting));
                break;
       }

        $out['formated'] = $date_formated;

        $out['date_starting'] = date('M-d-Y',strtotime($date_starting));
        $out['date_ending'] = date('M-d-Y',strtotime($date_ending));

        $out['sql_starting'] = date("Y-m-d",strtotime($date_starting));
        $out['sql_ending'] = date("Y-m-d",strtotime($date_ending));

        if(!$dates_only) {
            $out['where'] = " AND LEFT({$this->_tableAlias}.`{$this->_dateField}`,10) >= '{$out['sql_starting']}' AND LEFT({$this->_tableAlias}.`{$this->_dateField}`,10) <= '{$out['sql_ending']}' ";
        }

        return $out;
    }

    /**
    * For ajax autocomplete
    *
    * @version 0.1 experimental JB  11/21/08 9:32 AM
    */
    function searchUL($search,$opts=false) {
        if(!$search) echo("No search given to searchUL");
        $sql = $this->showTable(array("sql_only"=>true,"where"=>" AND $this->_titleField LIKE '%".mes($search)."%' "));
        $sql .= " LIMIT 100 ";
        $result=acquery($sql,$this->_db);
        ?><ul><?
        while($row=mysql_fetch_assoc($result)) {
            extract($row);
            ?><li><?= $row[$this->_idField] ?><span class="informal"> - <?= $row["title"] ?></span></li><?
        }
        ?></ul><?
    }

     /*
    * Find single 'cell' with a date, will either count the records in a table
    * in that date range or sum a field.
    * - opts array
    *   - view (weekly, monthly, etc.)
    *
    * @returns string with sql
    *
    * @version 1.2 JB  1/28/11 11:39 AM - Added 20 record limit, might want to limit to records from the last two years instead.
    * @version 1.1 SJP 01/07/2010 04:10 AM Updated chapter support
    * @version 1.0 Jonah  8/18/08 7:40 PM
    *
    */
    function getTrendSql($opts=false) {
        if($opts && is_array($opts)) extract($opts);

        if(!$chapter_id && is_numeric($_REQUEST['chapter_id'])) {
            $chapter_id = $_REQUEST['chapter_id'];
        }
        $this->_chapter_id = $chapter_id;

        if(!$date_field) $date_field = $_REQUEST['date_field'];
        if(!$date_field) $date_field = $this->_dateField;
        if($_REQUEST['view']) $view = $_REQUEST['view'];
        $g = $this->group_time($view,$date_field,array("table"=>$this->_table));

        // Make sure that we haven't manually limited to chapter_id already
        if(!$this->contains("chapter_id",$where)) $chapter = $this->limitChapterSql($chapter_id);

        if($chapter['where']) {
            /*
            * Really bad hack -- SJP 01/07/2010 01:16 PM
            */
            $chapter['where'] = str_replace("Array",$chapter_id,$chapter['where']);
            $where .= $chapter['where'];
        }
        if($chapter['join']) $join .= $chapter['join'];

        // Begin building the sql string.

        $sql = " SELECT ";
        if($field) {
          $sql .= " SUM($field) ";
        } elseif($count_field) {
          $sql .= " COUNT({$count_field}) ";
        } else {
          $sql .= " count(*) ";
        }
        $sql .= " as count, {$this->_table}.$date_field FROM $this->_table ";
        if($join) $sql .= $join;

        $sql .= " WHERE 1 ";
        $sql .= " AND (TO_DAYS({$this->_table}.$date_field) > TO_DAYS(NOW()) - 10000) ";

        if($this->_where) $sql .= $this->_where;
        if($where) $sql .= $where;

        $sql .= $g['sql'];

        // $sql .= " LIMIT 20 ";
        // Having needs to be inserted between ORDER BY and other stuff.
        // if($having) $sql .= " ".$having;

        return $sql;
    }

    /*
    * If a field is given it's assumed that you want it to be summed. Otherwise it simply
    * be the rate at which records are added to the table. JB  8/15/08 10:19 PM
    *
    * <code>
    * $ex = new ExampleTable();
    * $range = 4;
    * $current = 0;
    * $view = 'weekly';
    *
    * while($current <= $range) {
    *     $value = $ex->getTrendCell($current,'quantity',$view);
    *     $current++;
    *     $date_ending=$rsvp->_currentDateRange[sql_ending];
    * }
    * </code>
    * @param date int Interval from now.  0 is this week, month ect, 1 is last week/month.
    * @param field varchar A field to be SUMmed, leave blank to get a simple record count from that table.
    * @param view weekly or monthly currently supported
    * @param opts ("where" - SQL string with AND field=value type format)
    *
    * @returns A numeric value of a count or sum of things that happened within a time interval.
    * @version 1.0 Jonah  8/19/08 12:55 PM
    * @version 1.1 SJP 01/07/2010 04:11 AM Updated chapter support
    * @version 1.2 SJP 05/15/2010 11:05PM Added custom date_field
    */
    public function getTrendCell($date,$field=false,$view=false,$opts=false) {
        $debug = false;
        if($opts && is_array($opts)) extract($opts);

        if(!$view) $view = $_REQUEST['view'];
        if(!$view) $view = "weekly";

        if(is_numeric($date)) {
            /*
            * Currently the system will not work across years.  We should be converting the
            * 'weeks since today' format, into the week of the year, then into a date range
            * with a start date and an end date. JB  9/5/08 2:31 PM
            * Date range is now in use. -- SJP 08/13/2009 06:44 PM
            */
            $date_range = $this->rowToDateRange($date,$view);
            $this->_currentDateRange = $date_range;
            $sql_where = $date_range['where'];
            if($debug) { echo "<br> Date range is: "; $this->da($date_range); }
            if($debug) echo "<br> The view is: $view <br>";
            /* Get where from date range above -- SJP 08/08/2009 11:56 PM
            switch($view) {
                case"weekly":
                case"week":
                    $where .= " AND WEEK({$this->_table}.{$this->_dateField}) = (WEEK(NOW()) - $date) AND YEAR({$this->_table}.{$this->_dateField}) = (YEAR(NOW()) - 0) ";
                    break;
                default:
                case"monthly":
                    $where .= " AND {$this->_table}.{$this->_dateField} > '".mes($date_range['sql_starting'])."' AND {$this->_table}.{$this->_dateField} <= '{$date_range[sql_ending]}' ";
                    break;
            }
            */
        } else {
            // I'm not sure why this would be, if it's not numeric, we might as well error and die.  JB  11/20/08 6:17 PM
            // $where .= " AND WEEK({$this->_table}.{$this->_dateField}) = WEEK('".mes($date)."') ";
        }

        $sql_opts = array(
            "where"=>$this->_where.$sql_where.$where,
            "field"=>$field,
            "join"=>$join,
            "view"=>$view,
            "having"=>$having,
            "date_field"=>$date_field
        );
        
        if($chapter_id) $sql_opts['chapter_id'] = $chapter_id;
        if($count_field) $sql_opts['count_field'] = $count_field;

        $this->sql = $this->getTrendSql($sql_opts);

        if($_REQUEST['debug_dbos']) da($this->sql);
        $res = $this->getRes($this->sql);
        if($num=mysql_num_rows($res)) {
            $row=mysql_fetch_assoc($res);
            return $row['count'];
        } else {
            return 0;
        }

    }
    
    function browse($sql) {
        return $sql;
    }
    
    /**
    * 
    * Show a trend graph.  
    *
    * @vars opts
    *   - title
    *   - date_field
    *   - group_by 
    *   - sum
    *   - view ('daily','weekly','monthly','yearly')
    *   - title_exact
    *
    * @version 1.4 JS 2014-11-19: added title_exact option
    * @version 1.3 JB added group_by and sum, and support yearly  10/31/11 9:56 PM opinvoice
    * @version 1.2 JB added timer  1/28/11 11:34 AM
    * @version 1.1 SJP 05/15/2010 11:05PM Added custom date_field
    * @version 1.0 Jonah  8/18/08 7:41 PM
    */
    public function graph($opts=false) {
        $debug = false;
        if(is_array($opts)) extract($opts);
        
        if($_REQUEST['view']) $view = urlencode($_REQUEST['view']);
        if($_REQUEST['limit']) $limit = intval($_REQUEST['limit']);
        
        if($group_by=="YEAR") $view="yearly";
        if(!$render_type) $render_type = "googleapi";
        if(!$view) $view="weekly";
        
        if(!$this->tbl)   {
          $this->tbl = $this->_loadTableInfo();
          // $this->tbl = self::$_tableInfo[$this->_table];
        }

        if(!$title) {
            $title = $this->_table;
        }

        if($debug) $this->da($this->tbl);

        if(!$date_field) $date_field = $this->_dateField;

        if(!$date_field) {
            $errors[] = "We don't know the date field for $this->_table.";
        }

        if($errors) show_errors($errors);
        
        $g = $this->group_time($view,$date_field,array("table"=>$this->_table));
        
        if(!$group_by) {
            $sql = $this->getTrendSql($opts); if($debug) da($sql);
        } else {
            if($group_by=="YEAR") {
                $group_by = " YEAR(".$this->_dateField.") ";
            }
            $sql = " SELECT SUM(".$sum.") AS count, $group_by  AS $this->_dateField FROM $this->_table GROUP BY $group_by ORDER BY $group_by DESC ";
        }
        if($_REQUEST['showSQL']) {
            $this->da($sql);
        }
        if($this->_dbo) {
            $trend = $this->_dbo->fetchAll($sql);
        } else {
            $trendRes = acquery($sql,$this->_db);
            while($row = mysql_fetch_assoc($trendRes)) {
                $trend[] = $row;
            }
        }

        if($debug) $this->da($trend);
        $hidefields = array();
        $graphf = array($date_field,'count'); $data = array();
        $i=0;
        if($num=count($trend)) {
            if($render_type != 'googleapi') {
                acToggle('tableWrapper_'.dirify($title));
            }
                ?>
                <!-- begin ACTable::graph @time <?= date("Y-m-d H:i:s") ?>  -->
                <div id="tableWrapper_<?= $this->dirify($title) ?>" style="display:none">
                <div>Showing <b><?= $num ?></b> records</div>
                <table class="list table" id="acTableListData">
                <?
                foreach($trend AS $count=>$row) {
                    $i++; $i % 2 ? $row_class = "odd" : $row_class = "even";
                    extract($row);
                    if($i==1) { // do header row ?>
                          <tr class="reverse">
                            <td class="HeaderRow">Action</td>
                            <?
                            foreach($row as $f=>$v) {
                              if(!in_array($f,$hidefields)) { ?>
                                <td ><?= $this->camelcaps($f); ?></td>
                           <? }
                            }
                            ?>
                          </tr>
                    <? }

                    if($this->_idField && is_array($this->_idField)) {
                        $id_field_label = $this->_idField[0];
                        $id_field_value = $this->$id_field_label;
                    } else {
                        $id_field_label = $this->_idField;
                        $id_field_value = $this->$id_field_label;
                    }

                    if($render_type != 'googleapi') {
                        ?>
                        <tr class="<?= $row_class ?>">
                          <td> <a href="<?= $this->_controller ?>?action=edit_<?= $this->_table ?>&<?= $id_field_label ?>=<?= $id_field_name ?>">Edit</a>
                          </td>
                          <?
                    }
                    foreach($row AS $f=>$v) {
                         if(!in_array($f,$hidefields)) {
                            if($f==$graphf[0]) {

                                switch($view) {
                                    case"quarter":
                                    case"quarterly":
                                        $date_format = $this->rowToDateRange($i,$view,array("date_starting"=>$v));
                                        $v = $date_format['formated'];
                                        break;
                                    default:
                                        $v = date("M d, Y",strtotime($v));
                                        break;
                                }

                                $data[$i-1][label] = $v;

                            }
                            if($f==$graphf[1]) $data[$i-1][point] = $v;
                            if($render_type != 'googleapi') {
                                ?><td class="acDataCell"> <span id="<?= $f ?>_<?= $count ?>"><?= $v ?></span> </td><?
                            }
                       }
                    }
                    if($render_type != 'googleapi') {
                        ?></tr><?
                    }
                }
                ?></table>
                </div>
            <?
            if(count($data)) {
              $g_title = '';
              // if title_exact, do not alter provided title
              if (isset($title_exact) && $title_exact == TRUE)
                  $g_title = $title;
              else
                  $g_title = $this->camelcaps(str_replace("_", " ",$title." ".$view));
              $g = new ACGraph($data,$g_title);
              $g->view = $view;
              if($width) { $g->width = $width; }
              if($height) { $g->height = $height; }
              $g->render($render_type);
            }
        } else {
            ?><p style="padding-right: 20px;">No <?= $title ?> records found.</p><?
        }

        ?><!-- End ACTable::graph @time <?= date("Y-m-d H:i:s") ?>  --><?php
    } // end function graph

    /**
     * The central switch statement for the admin pages associated with DBOS tables/classes.
     *
     * @version 1.2 - Added bulk import capabilities - JB  7/22/11 10:09 AM
     * @version 1.1 SJP 05/15/2010 06:49PM Moved summary under edit for better usablity
     * @version 1.0 JB  8/26/08 5:20 PM
     */
    public function adminPage($action=false) {

        if(!$action && isset($_REQUEST['action'])) {
            $action = strip_tags($_REQUEST['action']);
        }
        
        if(isset($_REQUEST['table'])) {
            $table = strip_tags($_REQUEST['table']);
        }
        // Only supports simple index fields.
        if(!is_array($this->_idField)) {
            if(!$id) $id = $_REQUEST[$this->_idField];
        } else {
            foreach($this->_idField AS $field) {
              $id[$field] = $_REQUEST[$field];
            }
        }

        if($action!="export") $this->adminPageTop();
        
        switch($action) {
            case"bulk_select":
            case"bulk_edit":
            case"bulkSelect":
                $this->bulkSelectConfirm();
                break;        
            case"forceUniqueIndex":
                $this->forceUniqueIndex();
                $this->showTable();
                break;
            case"bulk_import_form":
                $this->bulkImportForm();
                break;
            case"bulk_import":
                $this->bulkImport();
                break;
            case"delete":
                $obj = DBOS::factory($this->_table,$id);
                $obj->delete();
                $obj->showSuccess("The record was deleted from ".$this->_referToAs);
                $this->showTable();
                break;
            case"edit_".$this->_table:
            case"edit":
                $obj = DBOS::factory($this->_table,$id);
                if($this->select_field) {
                    $opts['select_field'] = $this->select_field;
                }
                
                $obj->showForm($opts);
                break;
            case"db_edit_".$this->_table:
                $obj = DBOS::factory($this->_table,$id);
                
                $id = $obj->saveForm();
                if(!is_numeric($id)) {
                    $this->showTable();
                    break;
                }
            case"summary":
            case $this->_table."_summary":
                $obj = DBOS::factory($this->_table,$id);
                $this->showRecord($obj);
                break;
            case"list":
            case"listTableRecords":
            case"":
                $this->showAdminTable();
                break;
            case"export":
                $this->exportRecords();
                break;
            case"table_export":
                $this->tableExport();
                break;
            case"report":
                $this->report();
                break;
            default:
              ?>The action <?= $action ?> is not defined for this the <?= $this->_table ?> class.  Use Object->adminPage to set.<?
        }
        
        if(!$action=="export") $this->adminPageBottom();
    }
    
    public function showRecord($obj=false) {
        if(!$obj) $obj = $this;
        // Should be toAdminHTML.
        if(is_numeric($obj->id)) {
            if ($this->_customTableTitle)
            {
                $filtered_title = $this->_customTableTitle;
            } else {
                $filtered_title = $this->camelcaps(str_replace("_"," ",$obj->_table));
            }
            /*
            if (!$this->_readOnlySummary)
            { 
                ?><a class="editMe" id="edit" href="<?= $_SERVER['PHP_SELF'] ?>?action=edit&table=<?= $obj->_table ?>&<?= $this->_idField ?>=<?= $obj->id ?>">Edit this <?= $filtered_title ?></a><?
                ?><a class="deleteMe" id="delete" href="<?= $_SERVER['PHP_SELF'] ?>?action=delete&table=<?= $obj->_table ?>&<?= $this->_idField ?>=<?= $obj->id ?>">Delete this <?= $filtered_title ?> record</a><?
            }
            */
        }
        if(is_numeric($obj->id)) {
            $filtered_title = $this->camelcaps(str_replace("_"," ",$obj->_table));
            
            ?>
            <div class='btn-group' role='group'>
            
                <a class="editMe btn btn-default" id="edit" 
                    href="<?= $_SERVER['PHP_SELF'] ?>?action=edit&table=<?= $obj->_table ?>&<?= $this->_idField ?>=<?= $obj->id ?>&page=mvc_engagements">
                    <i class='fa fa-edit'></i> Edit this <?= $this->_referToAs ?>
                </a>
            <?
            ?>
            <a class="deleteMe btn btn-default" id="deleteOne" href="<?= $_SERVER['PHP_SELF'] ?>?action=delete&table=<?= $obj->_table ?>&<?= $this->_idField ?>=<?= $obj->id ?>&page=mvc_engagements">
                <i class='fa fa-trash-o'></i> Delete this <?= $this->_referToAs ?>
            </a>
            </div>
            <?

            $obj->recordSummaryNavigation();
            ?><?php
        }
        if(!is_object($obj)) {
            if(class_exists($this->_table)) {
                $obj = new $this->_table($id);
            }
        }
        ?><div id='objectToHTML'><?php
        $obj->toHTML();
        ?></div><?php    
    }
    
    /**
    * @version 1.0 Jonah B  11/11/16 2:54 PM
    */
    public function tableExport() {
        // $this->da($this);
        $all = $this->_dbo->fetchAll("SELECT * FROM ".$this->_table." WHERE is_anonymous=1");
        
        ?><textarea cols="500" rows="400"><?php
        foreach($all AS $r) {
            $sql = "\nINSERT INTO $this->_table SET ";
            
            foreach($r AS $f=>$v) {
                if($f==$this->_idField) {
                    // continue;
                    $sql2 = $this->_exportCascadeRelations($v);
                    
                }
                $ps[] = "  $f = '".addslashes($v)."' ";
            }
            $sql .= implode(",",$ps);
            $sql .= ";";
            
            echo($sql);
            echo($sql2);
            unset($ps);
            
        }
        ?></textarea><?php
    }
    
    function _exportCascadeRelations($id=false) {
        return false;
    }
    
    /**
    * @version 1.0 Jonah B  4/18/13 10:14 AM
    */
    function adminPageBottom() {
    
    }
    
    public function bulkSelectConfirm() {
        da($_REQUEST);
    }
    
    /**
    * Jonah  10/31/11 4:58 PM
    */
    public function recordSummaryNavigation() {
        ?><span id="recordSummaryNavigation"></span><?php
    }
    
    /**
     * List top navigation
     *
     * @version 1.1 SJP 05/15/2010 06:23PM Added filtered title names
     * @version 1.0 jonah 2008
     */
    public function adminPageTop() {
        $filtered_title = $this->camelcaps(str_replace("_"," ",$this->_table));
        if($this->_showTableNavigation!==false) {
            require(dirname(__FILE__)."/html/tableTopNavigation.phtml");
        }
    }
    
    /**
     * @version 1.0 Jonah B GP Luigi 2/6/12 3:28 PM
     */
    function reportField($info,$opts=false) {
        $debug = false;
        if(is_string($info)) {
            $field = $info;
            unset($info);
            $info = $this->tbl[$field];
        }
        if($info['field']==$this->_idField) return false;
        
        if($debug) da($info);
        ?><!-- <h2><?= $info['field'] ?></h2> --><?php
        if($info['rel']['foreign_table'] || $info['type']=='enum' || 1==1) {
            $title = str_replace('_id','',$info['field']);
            
            $sql = 'SELECT COUNT(*) AS count, '.$info['field'].' 
                FROM '.$this->_table.' 
                WHERE 1
                GROUP BY '.$info['field'].'
                ORDER BY count DESC LIMIT 10';
            $all=$this->_dbo->fetchAll($sql);
            
            if($num=count($all)) {
                ?>
            <table class="reportFieldWrapper" width="100%">
                <tr>
                    <td valign="top" style='display:none;'>
                
                        <table class="list table" >
                            <tr class="headerRow">
                                <td>Entered</td>
                                <td><?= $info['rel']['foreign_table'] ?></td>
                            </tr>
                        <?php
                        foreach($all AS $row) {
                            $i++;
                            if(class_exists($info['rel']['foreign_table'])) {
                                $obj = new $info['rel']['foreign_table']($row->{$info['field']});
                                if($obj->_recordName) $recordName = $obj->_recordName;
                            }
                            if(!$recordName) $recordName = $row->{$info['field']};
                            
                            $data[$i-1][label] = $recordName;
                            $data[$i-1][point] = $row->count;   
                            // $obj = new user($row->{$info['field']});
                            ?><tr>
                                <td><?= $row->count ?></td>
                                <td valign="top" align="right"><?= $recordName ?></td>
        
                                <?php if($debug) da($info); ?>
                            </tr><?php
                            unset($recordName);
                        }
                        ?></table>
            
                </td>
                <td valign="top">
                <?php 
                switch($info['type']) {
                    case"date":
                    case"datetime":
                        $this->graph(array("date_field"=>$info['field']));
                        break;
                    default:
                        $g = new ACGraph($data,$info['field']);
                        $g->type="pie";
                        $g->render(); 
                }
                
                ?>
                <span class='caption'>Top 10 by <?= $info['field'] ?> <?= $info['rel']['foreign_table'] ?></span>
                </td></tr>
                </table><?php
             
            }
        } else {
            ?><div class="warning">Unknown field type for general reporting</div><?php
            da($info);
        }
    }    
    
    /**
    * Here to be overridden
    */
    public function adminPageTopExtend() { }

    /**
    * @version 1.0 Jonah B GP Luigi 2/6/12 3:28 PM
    */
    function report() {
        if(file_exists($_SERVER['DOCUMENT_ROOT'] . '/core/includes/classes/Graph/Graph.class.php')) {
            require_once( $_SERVER['DOCUMENT_ROOT'] . '/core/includes/classes/Graph/Graph.class.php');
        } else {
            require_once(dirname(__FILE__)."/Graph/Graph.class.php");
        }
        
        ?><h1>General reporting for <?= $this->_table ?></h1>
        <?php $this->showSearchFilter(); ?>
        <form action="<?= $PHP_SELF ?>" method="GET" name="form">   
            <b><?= $this->_dateField ?></b>:
            <input type="hidden" name="dateField" value="<?= $this->_dateField ?>" />
            Start <input type="date" name="startDate" value="<?= $_REQUEST['startDate'] ?>" />
            End <input type="date" name="endDate" value="<?= $_REQUEST['endDate'] ?>" />
            <input type="hidden" name="action" value="report" />
            <input type="hidden" name="table" value="<?= $this->_table ?>" />
            <input type="submit" value="Limit" />
        </form>
        <div id="reportGraphHeader">
            <?php $this->graph(array("width"=>"800","height"=>"100"));  ?>
            <h3>Add this graph with the following:</h3>
            <pre>
              <?php echo '$this->graph(array(\'width\'=>\'800\',\'height\'=>\'100\');'; ?>
            </pre>
        </div><?php
        
        foreach($this->tbl AS $field=>$finfo) {
            ?>$this->reportField('<?= $field ?>'); <?php
            $this->reportField($finfo);
        }
        ?><h3>Field Info</h3><?php
        da($this->tbl);
    }
    
    /**
     * For tables with 'parent_id' fields.
     *
     * @returns HTML with nested ul.
     */
    public function showTree($opts=false) {
        ob_start();
        /**
        * Find all of the children and parents.
        */
        $sql = " SELECT $this->_idField, $this->_parentField FROM $this->_table WHERE $this->_parentField > 0";
        $result=acquery($sql,$this->_db);
        while($row = mysql_fetch_assoc($result)) {
            $parents[$row[$this->_parentField]] = $row[$this->parentField];
            $children[$row[$this->_idField]] = $row[$this->_idField];
        }
        /**
        * Find the root nodes, the parent's that have no parents.
        */
        $sql = " 
            SELECT $this->_idField FROM $this->_table 
            WHERE $this->_parentField = 0 and $this->_idField 
                IN (".explode(",",$parents).")";

        /**
        * Find all of the orphans
        */
        return ob_get_clean();
    }
    
    function showFields() {
        $fieldCount = count($this->tbl);
        
        ?><?= $fieldCount ?> fields in this table
        <ul><?php
        foreach($this->tbl AS $field=>$info) {
            ?><li><?= $field ?></li><?php
        }
        ?>
        </ul><?php
    }
    
    /**
    * And easy way to bulk import data from a spreadsheet when you are setting up a table.
    *
    * @version 1.0 JB  7/22/11 7:42 AM
    */
    public function bulkImportForm() {
        // $this->showFields();
        ?>
        <h1><i class="fa fa-import"></i> Import Records</h1>
        <div class="alert alert-info">
        Expects tab seperated data.  In most cases you can copy/paste from excel.  
        Field order must match except for id field.
        </div>
        
        <form action="<?= $_SERVER['PHP_SELF'] ?>" method="POST">
            <textarea name="input" cols="100" rows="100"></textarea>
            <input type="hidden" name="table" value="<?= $this->_table ?>">
            <input type="hidden" name="action" value="bulk_import"><br />
            <center style="width:400px;">
                <input type="checkbox" value="1" title="Use the data copy/pasted into form to suggest the structure of a new database" name="suggestTableStructure"> Suggest table structure<br />
                <input type="checkbox" value="1" name="updateDups"> Update duplicates <br />
                <input type="checkbox" value="1" name="use_idField"> Import data contains record id <br />
                <input type="checkbox" value="1" name="includesHeaderRow">Data includes header row<br />
                Title Row: <input type="text" name="titleRow" size="2" />
                <br />
                <input type="submit" class="btn btn-primary" value="Import Now"> 
            </center>
        </form>
        <?php

    }

    /**
    * An easy way to bulk import data from a spreadsheet when you are setting up a table.
    * Expects tab seperated.  Just copy/paste.  Field order must match except for id field.
    *
    * @version 1.0 JB  7/22/11 7:43 AM
    */
    public function bulkImport() {
        $verbose = false;
        mysql_select_db($this->_db);
        $in = $_REQUEST['input'];
        $in = trim($in);
        $lines = explode("\n",$in);
        $count = count($lines);
        
        ?>Found <?= $count ?> lines for import <?php
        
        // $this->showFields();
        
        if(substr($in,0,6)=="INSERT" || substr($in,0,7)=="REPLACE") {
            mysql_query($in) or mysql_die($in);
            return true;
        } 
        
        foreach($this->tbl AS $field=>$info) {
            
            if($field != $this->_idField) {
                $fields[] = $field;
            } elseif($_REQUEST['use_idField']) {
                $fields[] = $field;
            }
        }
        
        if($debug) da($fields);
        foreach($lines AS $line) {
            $i++;
            if(substr($line,0,6)=="INSERT") {
                
                $sql = $line;
            } else {
            
                if($i==1 && $_REQUEST['suggestTableStructure']) {
                    $this->importSuggestTable($line);
                }
                
                if($i==1 && $_REQUEST['includesHeaderRow']) {
                    $this->importHeaders = explode("\t",$line); 
                    echo("\n* Headers: ");
                    da($this->importHeaders);
                    $this->showFields();
                    continue;
                }
                
                if(is_numeric($_REQUEST['titleRow'])) {
                    $sql = $this->processLineToSQL($line);
                } else {
                
                    $line = str_replace("\r","",$line);
                    $sql = "INSERT INTO $this->_table SET ";
                    if($verbose) echo "<br>Processing line ".$line."<br><br>";
                    $parts = explode("\t",$line);
                    foreach($parts AS $count=>$part) {
                        // echo("| Value: ".$part);
                        if($count!=0) $updates.= ",";
                        $updates .= " ".$fields[$count]." = '".$this->escape(trim(stripslashes($part)))."'";
                    }
                    if($debug) echo "<br /><br />UPDATES:".$updates;
                    $sql .= $updates;
                    if($_REQUEST['updateDups']) {
                        $sql .= " ON DUPLICATE KEY UPDATE ";
                        $sql .= $updates;
                    }
                    $sql .= ";";
                }
            }
            echo "<br><br>".$sql;
            
            if($_REQUEST['do_mysql'] || 1==1) {
                mysql_query($sql) or mysql_die($sql);
            }
            unset($updates);
        }

    }
    
    /**
    * @version 1.0 Jonah B  5/19/14 11:38 AM
    */
    public function processLineToSQL($line) {
        
    }

    /**
    * Create a default table from field headings.
    *
    * version 1.0 JB  8/5/11 12:56 PM
    */

    public function importSuggestTable($line) {
        $fields = explode("\t",$line);
        $sql = "
CREATE TABLE IF NOT EXISTS `".$this->_table."` (
  `".$this->_idField."` int(10) NOT NULL AUTO_INCREMENT, ";

        foreach($fields AS $field) {

            $field = str_replace(")","",$field);
            $field = str_replace("(","",$field);
            $field = trim($field);
            if(!$field) continue;
            $field = str_replace(".","",$field);
            $field = dirify($field);

            if(($field == "lat" || $field == "lon" || $field == "latitude") && !$isLatLon) {
                $isLatLon = true;
                $sql .= "
                   `latitude` decimal(12,8) NOT NULL,
                    `longitude` decimal(12,8) NOT NULL,
                ";
            } elseif($field=="notes") {
                $sql .= "  `notes` text NOT NULL,";
                $notesSet = true;
            } else {
                $sql .= "\n `".$field."` varchar(300) NOT NULL, ";
            }
        }

        $notes .= "  `notes` text NOT NULL, ";

        $sql .= "
            `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (`".$this->_idField."`),
        ";
        if(1==2) {
            $sql .= " UNIQUE KEY `UniqueName` (`facility_name`,`city`,`state`) ";
        }
        
        $sql .= ") ENGINE=MyISAM ";
        da($sql);
        return $sql;
    }

    /**
    * Publish the table as xml.
    * @opts - where: expects ' AND field = value ';
    *
    * @version 1.0 JB  7/21/11 5:39 PM
    */
    public function publishXML($opts=false) {
        if($opts && is_array($opts)) extract($opts);
        $doc = new domdocument("1.0");
        $node = $doc->createElement($this->_table."s");
        $parnode = $doc->appendChild($node);


        // Set the active MySQL database
        $db_selected = mysql_select_db($this->_db);
        if (!$db_selected) {
          die ('Can\'t use db : ' . mysql_error());
        }

        // Select all the rows in the markers table
        $query = "SELECT * FROM $this->_table WHERE 1 ";
        if($where) $query .= $where;

        $result = mysql_query($query);
        if (!$result) {
          die('Invalid query: ' . mysql_error());
        }

        header("Content-type: text/xml");

        // Iterate through the rows, adding XML nodes for each
        while ($row = mysql_fetch_assoc($result)) {

            // ADD TO XML DOCUMENT NODE
            $node = $doc->createElement($this->_table);
            $newnode = $parnode->appendChild($node);
            foreach($row AS $field=>$value) {
                $newnode->setAttribute($field, $value);
            }


        }

        $xmlfile = $doc->saveXML();
        echo $xmlfile;

    }

    /**
    * http://jbaker.dev.greenpeaceusa.org/chemicals/coalash/?action=xml
    * http://ac-jonah-gp.activistinfo.org/chemicals/coal/?mapWidth=920&mapHeight=555
    *
    * @returns link to xml feed
    * @version 1.0 JB 7/28/11 9:40 AM
    */
    public function showMapXML($opts=false) { // Start XML file, create parent node
        if($opts && is_array($opts)) extract($opts);

        if(!is_array($this->_mapPublishFields)) {
            $this->_mapPublishFields = array($this->_titleField=>$this->{$this->_titleField});
        }
        $doc = new domdocument("1.0");
        $node = $doc->createElement("markers");
        $parnode = $doc->appendChild($node);


        // Set the active MySQL database
        $db_selected = mysql_select_db($this->_db);
        if (!$db_selected) {
          die ('Can\'t use db : ' . mysql_error());
        }

        // Select all the rows in the markers table
        $query = "SELECT * FROM $this->_table WHERE longitude < 0";
        if($where) $query .= " ".$where;
        $result = mysql_query($query);
        if (!$result) {
          die('Invalid query: ' . mysql_error());
        }

        header("Content-type: text/xml");

        // Iterate through the rows, adding XML nodes for each
        while ($row = @mysql_fetch_assoc($result)){
            // $pcompany = new CoalAshCompany($row['parent_company_id']);
            //$pond = new CoalAshPond($row['pond_id']);
            // ADD TO XML DOCUMENT NODE
            $node = $doc->createElement("marker");
            $newnode = $parnode->appendChild($node);
            $newnode->setAttribute("lat", $row['latitude']);
            $newnode->setAttribute("lng", $row['longitude']);
            foreach($this->_mapPublishFields AS $field=>$label) {
                if($this->contains(",",$field)) {}
                $newnode->setAttribute($field, $row[$field]);
            }

            // $newnode->setAttribute("color", $pcompany->color);
        }

        $xmlfile = $doc->saveXML();
        echo $xmlfile;
        return $this->_mapXMLFeedUrl;
    }

    /**
    * Nice marker generator:
    * http://www.powerhut.co.uk/googlemaps/custom_markers_v2_API.php
    *
    * http://jbaker.dev.greenpeaceusa.org/chemicals/coalash/
    * http://ac-jonah-gp.activistinfo.org/chemicals/coal/?mapWidth=920&mapHeight=555
    *
    * @version 1.0 JB  11/10/10 9:37 AM
    */
    public function showMap($opts=false) { // Start XML file, create parent node
        global $google_map_key,$session_user;
        if($opts && is_array($opts)) extract($opts);
        if(!$this->_mapTitle) $this->_mapTitle = $this->_table." map ";
        if($_REQUEST['mapWidth']) $this->_mapWidth = $_REQUEST['mapWidth'];
        if($_REQUEST['mapHeight']) $this->_mapHeight = $_REQUEST['mapHeight'];
        include($_SERVER['DOCUMENT_ROOT']."/core/includes/classes/DBOS/html/dbosMap.html");
    }

    /**
    * Nice marker generator:
    * http://www.powerhut.co.uk/googlemaps/custom_markers_v2_API.php
    *
    * @version 1.0 JB  7/28/11 8:55 PM
    */
    public function showMapCustomMarker($base=false,$name=false) {
        if(!$base) $base = "/core/templates/quitcoal/img/pointFight";
        if(!$name) $name = "myIcon";
        list($imageWidth, $imageHeight, $type, $attr) = getimagesize($_SERVER['DOCUMENT_ROOT'].$base."/image.png");
        list($shadowWidth, $shadowHeight, $type, $attr)= getimagesize($_SERVER['DOCUMENT_ROOT'].$base."/shadow.png");

        ?>
        // ACTable::showMapCustomMarker
        // Google Map Custom Marker Maker 2011
        // Please include the following credit in your code

        // Sample custom marker code created with Google Map Custom Marker Maker
        // http://www.powerhut.co.uk/googlemaps/custom_markers.php

        var <?= $name ?> = new GIcon();
        <?= $name ?>.image = '<?= $base ?>/image.png';
        <?= $name ?>.shadow = '<?= $base ?>/shadow.png';
        <?= $name ?>.iconSize = new GSize(<?= $imageWidth ?>,<?= $imageHeight ?>);
        <?= $name ?>.shadowSize = new GSize(<?= $shadowWidth ?>,<?= $shadowHeight ?>);
        <?= $name ?>.iconAnchor = new GPoint(23,<?= $shadowHeight ?>);
        <?= $name ?>.infoWindowAnchor = new GPoint(23,0);
        <?= $name ?>.printImage = '<?= $base ?>/printImage.gif';
        <?= $name ?>.mozPrintImage = '<?= $base ?>/mozPrintImage.gif';
        <?= $name ?>.printShadow = '<?= $base ?>/printShadow.gif';
        <?= $name ?>.transparent = '<?= $base ?>/transparent.png';
        <?= $name ?>.imageMap = [35,0,36,1,37,2,38,3,41,4,43,5,43,6,44,7,44,8,44,9,44,10,44,11,44,12,44,13,43,14,42,15,40,16,36,17,35,18,18,19,16,20,15,21,5,22,5,23,5,24,5,25,3,26,6,27,6,28,6,29,6,30,6,31,6,32,6,33,6,34,6,35,37,36,37,37,37,38,37,39,37,40,37,41,39,42,39,43,39,44,39,45,39,46,39,47,39,48,39,49,39,50,39,51,39,52,39,53,39,54,0,54,0,53,0,52,0,51,0,50,0,49,0,48,1,47,1,46,1,45,1,44,1,43,1,42,1,41,1,40,1,39,1,38,1,37,1,36,1,35,1,34,1,33,1,32,1,31,2,30,2,29,2,28,2,27,2,26,2,25,1,24,1,23,1,22,0,21,0,20,0,19,0,18,0,17,0,16,0,15,0,14,1,13,1,12,2,11,3,10,5,9,5,8,6,7,7,6,18,5,19,4,24,3,24,2,25,1,26,0];
        <?php
    }
    
    public function _getDataset() {
        foreach($this->_rowData as $row) {
            $r = (array)$row;
            foreach($r AS $f=>$v) {
                switch($v) {
                    case'0':
                        $v = 'Y';
                        break;
                    case'1':
                        $v = 'N';
                        break;
                    default:
                }
                $vs[] = '"'.$v.'"';
            }
            $out = implode(",",$vs);
            
            $rows[] = "[".$out."]";
            
        }
        echo implode(',',$rows);
    }
    
    public function _getJSON() {
        $out = array();
        foreach($this->_rowData as $count=>$row) {
            foreach($row AS $field=>$v) {
                
                if(method_exists($this,'show_'.$field)) {
                    
                    $functionName = "show_".$field;
                    $v = $this->{$functionName}($v);
                   
                }
                
                $out[$count][$field] = $v;
               
            }
            
            
        }    
        // $this->dump($out);
        $json = json_encode($out);
        return $json;
    }
    
    /**
     * Move this to DBObjectAC
     * @version 1.0 Jonah B  2/16/12 10:48 AM
     */
    public function _deleteAllRecordsFromTable() {
        $sql = "DELETE  FROM $this->_table ";
        mysql_query($sql) or mysql_die($in);
    }

    /**
    * Delete the records where this field is blank.
    *
    * @version 1.0 Jonah B  7/1/13 8:20 PM
    */
    function deleteBlankRecords($field) {
        if(!$field) return false;
        $sql = "DELETE FROM $this->_table WHERE $field = '' ";
        $this->_dbo->query($sql);
    }
    
    /**
    * @version 1.0 JB  7/28/11 5:31 PM
    */
    public function openInfoWindowHtml() {
        ?>
      <!-- begin marker html generation -->
      var html = "<b><?= $this->_referToAs ?>:</b> " + <?= $this->_titleField ?> + "<br/><br />";
      if(ceo_image && ceo_url){
      //../../core/mods/phpThumb/phpThumb.php?src=
        html += "<a style='display:inline-block;float:left;margin-right:2px;' href='"+ceo_url+"' target='_blank'><img border='0' src='"+ceo_image +"' alt='CEO image' width='60' height='90'/></a>";
      }
      html += "<br/><br /><b>Associated Power Plant:</b> "+associated_power_plant;
      html += "<br/><br /><b>Facility:</b> "+facility;

      <?php if(is_numeric($_COOKIE['session_user_id'])) { ?>
      html += "<p><a href='/admin/coalash/admin_coalash.php?action=edit_coalash_pond&pond_id="+pond_id+"'">Edit</a></p>";
      <? } ?>
      <!-- end marker html generation -->
        <?php
    }
    
    /**
    * @version 1.0 Jonah B  5/1/16 10:27 PM - p24
    */
    function camelcaps($in) {
        $in = str_replace("_"," ",$in);
        return $in;
    }
    
    /**
    * @version 1.0 Jonah B  5/1/16 10:27 PM - p24
    */
    function contains($needle, $haystack) {
        $pos = strpos($haystack, $needle);
        if($pos === false) {
          return false;
        } else {
          return true;
        }
    }    

    /**
    * @version 1.0 Jonah B  5/1/16 10:27 PM
    */
    function pretty($array,$opts=false) {
        $hidevars = array('tbl','result_content','hidden_fields','explain');
        if(is_array($array)) {
          $this->html_show_array($array);
        } elseif(is_object($array)) {
            $object = $array;
            if($object->tbl) unset($object->tbl);
            $vars = get_object_vars($object);
            ?><table class="table" id="pretty"><?
            foreach($vars AS $f=>$v) {
                $pre=substr($f,0,1);
                if($pre=="_") {
                  continue;
                }
                if(in_array($f,$hidevars)) continue;
                $value = stripslashes($object->$f);
                if($this->contains("http",$value)) {
                    $value = "<a href='".$value."'>$value</a>";
                }
                ?><tr>
                <td align="right" class="field_label"><?= $this->camelcaps($f) ?></td>
                <td><?= $value ?></td>
                </tr><?
            }
            ?></table><?
        }
    }
    
    function html_show_array($in) {
        ?><table class="table"><?php
        foreach($in AS $f=>$v) {
            if(is_array($v)) {
                ob_start();
                $this->html_show_array($v);
                $v = ob_get_clean();
                /*
                $vo = "<pre>";
                $vo .= print_r($v,true);
                $vo .= "</pre>";
                $v = $vo;
                */
            }
            ?>
            <tr>
                <td align="right" valign="top"><?= $f ?>:</td>
                <td><?= $v ?></td>
            </tr><?php
        }
        ?></table><?php
    }
    
    /**
     * @version 1.0 Jonah B  7/1/13 9:30 AM
     */
    public function setAllUUIDs() {
        $sql = "SELECT * FROM $this->_table";
        $rs = $this->_dbo->fetchAll($sql);
        foreach($rs AS $r) {
            $sql = "UPDATE $this->_table SET uuid = UUID() WHERE uuid='' ";
            echo("\n* ".$sql);
            $this->_dbo->query($sql);
        }
    }
    
    /**
     * @version 1.0 Jonah B  6/13/14 2:29 PM
     *
     * type (checkbox,text), label
     */
    function searchField($name,$opts=false) {
        if($_REQUEST[$name]) {
            $value = $_REQUEST[$name];
        }
        ob_start();
        if($opts && is_array($opts)) extract($opts);
        if(!$label) {
            $label = $this->camelcaps($name);
        }
        
        if(!$id) {
            $id = "search_".$name;
        }
        switch($type) {
            case"checkbox":
                if($value) $checked = "checked";
                ?><label for="<?= $id ?>"><?= $label ?>:</label><input type="checkbox" name="<?= $name ?>" id="search_<?= $name ?>" <?= $checked ?> /><?php
                break;
            default:
            ?><input type="text" name="<?= $name ?>" id="search_<?= $name ?>" size="25" value="<?= stripslashes($filter_title) ?>" placeholder="<?= $label ?>" /><?php
        }
        return ob_get_clean();
    }
    
    /**
     * @version 1.0 Jonah B  4/6/15 11:36 AM
     */
    
    function icon($type) {
        switch($type) {
            case"add":
                $type = "plus";
        }
        ?><i class="fa fa-<?= $type ?>"></i><?php
    }

    /**
    * HTML Pulldown function for the very lazy.  Useful for fast and easy admin forms, but
    * not recommended for high traffic public stuff.  Just hand it a table name and it
    * just might give you a pulldown of the records in it.
    *
    * @package ActiveCore
    * @author jonah 2007
    * @version 1.4 GP Jonah  10/27/08 5:16 PM
    *  - Fixed bug where it would not default to blank or 0.
    * @version 1.3 GP Jonah  10/20/08 9:18 AM
    *  - ability to change the value of the 'choose' field.
    * @version 1.2 GP  4/25/08 6:17 PM
    *  - added phpmyadmin display field support
    */
    
    function dropdown($table, $opts=false,$opt=false) {
        global $db_name, $errors;
        // Assume that the table has a table_id field, and use that as the "value"
        if(is_string($opts)) {
            $id_value = $opts;
            unset($opts);
        }
        // Dumb, but needed for backwards compatibility. JB 2007
        if(!$opts) $opts = array();
        if(is_array($opt)) $opts = array_merge($opt,$opts);
    
        if($opts[id_field]) {
            $id_field = $opts[id_field];
        } else {
            $id_field = $table."_id";
        }
    
        if($opts['choose'] === true || $opts['choose'] == 1) {
            $opts['choose'] = "Choose...";
        }
    
        if(isset($opts['default'])) {
          $id_value = $opts['default'];
        } elseif($_REQUEST[$id_field]) {
          $id_value = $_REQUEST[$id_field];
        } elseif(!$id_field) {
          global $$id_field;
          $id_value = $$id_field;
        }
    
        // determine the db
        if($opts[db]) {
          $db_name = $opts[db];
        } else {
          $db_name = $db_name;
        }
        // try to determine name field
        if(!$opts[name_field]) {
            $fields=get_table_fields($table, $db_name);
            if(in_array("name", $fields)) {
              $name_field = "name";
            } elseif(in_array("title", $fields)) {
              $name_field = "title";
            } elseif(in_array($table."_name", $fields)) {
              $name_field = $table."_name";
            } elseif(in_array($table."_title", $fields)) {
              $name_field = $table."_title";
            } elseif(in_array($table."_label", $fields)) {
              $name_field = $table."_label";
            } else {
              // last ditch effort to see if this has been defined in phpmyadmin
              /*
              $sql = " SELECT display_field FROM pma_table_info WHERE db_name = '$db_name' AND table_name = '$table' ";
              $result=mysql_db_query("phpmyadmin",$sql) or mysql_die($sql); if($debug) { echo "<hr><pre> $sql </pre><hr>"; }
              $row=mysql_fetch_assoc($result);
              $name_field = $row[display_field];
              */
            }
        } else {
          $name_field = $opts[name_field];
        }
    
        // Failed to find a good name field, use id instead.  JB  9/4/08 2:04 PM
        if(!$name_field) {
          $name_field = $id_field;
        }
        // now do the html output
    
    
          /*
          * Manually set defaut select field for pulldown -- SJP 09/02/2010 10:00PM
          */
          if($opts['select_field'] && is_array($opts['select_field'])) {
              foreach ($opts['select_field'] as $id => $value) {
                  if($id == $opts['id_field']) {
                      $select_field = $value;
                  }
              }
          }
    
    
        // Make the sql
        if(!$opts[read_only]) {
          $sql = "SELECT $id_field";
          if($select_field) {
              $sql .= ", {$select_field} as {$name_field} ";
          } elseif($name_field) {
              if(is_array($name_field)) $name_field = $name_field[0];
              $sql .= ", LEFT($name_field,40) as $name_field ";
          } else {
              // fairly useless, but still functional.
              $name_field = $id_field;
              $sql .= ", $id_field ";
          }
          $sql .= " FROM $table  WHERE 1=1 {$opts[where]} ";
          if($name_field) $sql .= " ORDER BY $name_field ";
          $sql .= " LIMIT 5000 ";
          
          if($fname=$opts['fname']) {
              // they have already created the array, use that
              if($this->contains("[",$fname)) {
                $name = $fname;
              } else {
                // they used fname as the name of the array, now add the field to it
                $name="{$opts[fname]}[{$id_field}]";
              }
          } else {
              $name = $id_field;
              if($opts['multiselect']) $name .= "[]";
          }
          $result = acquery($sql,$db_name);
          $num=mysql_num_rows($result);
          if($num>1000 && !$this->contains("[]",$name)) {  // use an autocomplete
              /*
              * This doesn't really work, the only reason I have it at all is because a pulldown of more than a thousand records
              * would break anyway.  JB  4/14/09 10:50 AM
              */
              ?>
              <input type="text" id="<?= $name ?>" name="<?= $name ?>" value="<?= $id_value ?>"/><div id="<?= $table ?>_choosen"></div>
              <div id="<?= $table ?>_menu" class="auto_complete"></div>
              <span class="caption">Start typing and select an option from the list that appears.</span>
              <script language="javascript">
                new Ajax.Autocompleter('<?= $name ?>','<?= $table ?>_menu','/admin/xml/?action=search&table=<?= $table ?>&field=<?= $name ?>',{ tokens: ['\n'],  minChars: 2 });
              </script>
    
              <?
              // if($id_value) echo get_record_name($table, $id_value, $name_field);
          } else {
              if($opts[multiselect]) $multiple = 'multiple="multiple"';
              ?><select name="<?= $name ?>" <?= $multiple ?> >
                <? if($opts[choose]!=false && !$opts[multiselect]) { ?><option value=""><?= $opts[choose] ?></option><? }
              echo $this->select_options($sql, $id_value, false, false, array("db"=>$db_name,"multiselect"=>$opts[multiselect]));
              ?></select><?
          }
        } else {
          if($id_value) { echo get_record_name($table, $id_value, $name_field); }
        }
    }
    
    /**
    * @function select_options
    * @author Sam 2005
    * @author Jonah  7/29/09 4:50 PM
    * @package ActiveCore
    // This function is intended to be a simple, flexible, and fast solution to
    // the common task of displaying an html drop-down menu.
    //
    // The function only returns the <option> tags.  This is so you can
    // put whatever you need into the <select> tag, and also allows you to
    // put any default <option> tag above the generated options
    //
    // $inp can be one of:
    //  - an array ( value => name )
    //  - a string, which is assumed to be a SQL query which returns two columns
    //    per row ( column1 => column2 ).  If there are more columns, they are ignored.
    //    Queries are made against the gp_action DB by default - if you need stuff
    //    from another DB, just specify the DB name explicitly
    //  - a mysql result resource, which returns ( column1 => column2 ) for each row.
    // $selected is the value of the option which should be marked SELECTED, if any.
    //  its value must pass a CASE-SENSITIVE string match to the option value.
    //  therefore, remember to take into account leading zeroes on numeric values,
    //  leading/trailing spaces, decimal precision on float values, etc.
    // $ttl is the cache lifetime, in seconds.
    //  - It has no effect unless $inp is a string, or $cache_key is set.
    //  - if it is set to zero, caching will not be used.
    // $cache_key is a cache id that you can use to override the default cache
    //   behavior - that is, if you want it to use a cached value even if the
    //   passed $inp value isn't a string.
    //
    // NB: the returned options are cached without any option being selected -
    //  the cached string is munged to select the correct value
    //
    // SIMPLE EXAMPLE:
    //   to display a dropdown of US states, put something like the following in your
    //   html file:
    // <select name="state" id="state">
    // <option> -- choose state -- </option>
    // <?= select_options("SELECT zone_code,zone_name FROM x_zone WHERE country_id='223'",$state,3600) ?>
    // </select>
    //
    //
    */

    function select_options($inp, $selected = '', $ttl = 0, $cache_key = '', $options=false) {
        // our output string
        $str = '';
        $cache_hit = 0;
        if($options[db]) {
          $db = $options[db];
        } else {
          $db = "gp_action";
        }
        // determine if a cache string could exist
        if ($cache_key) {
        //  $cache_string = $cache_key;
        } elseif (is_string($inp)) {
          $cache_string = $inp;
        } else {
          $cache_string = '';
        }
    
        // try to get the string from the cache, if we might be able to
        if ($cache_string and $ttl) {
          $c = new Cache_Lite(array('cacheDir'=>'/tmp/','lifeTime'=>$ttl));
          $str = $c->get($cache_string);
          if ($str) {
            $cache_hit = 1;
          }
        }
        if ($cache_hit) {
          // do nothing
        } elseif (is_string($inp)) {
          $res = mysql_db_query($db,$inp);
          if (!$res) {
            return '<option>DATABASE ERROR:'.htmlentities(mysql_error()).'</option>';
          }
        } elseif (is_resource($inp)) {
          $res = $inp;
        }
    
        // build id_tag -- SJP 03/17/2009 04:30 PM
        if($options['id_tag']) {
            $add_tag = $options['id_tag']."_";
        }
    
        // build up string if no cache data
        if (empty($str)) {
          if (is_resource($res)) {
            if (mysql_num_fields($res) == 1) {
              // if there is only one field returned, use it for both the value and the name
              while ($row = mysql_fetch_array($res)) {
                $value = stripslashes($row[0]);
                $str .= '<option value="'.$value.'"';
                if($add_tag) {
                    $str .= ' id="' . $add_tag . $value . '"';
                }
                $str .= '>'.$value."</option>\n";
              }
            } else {
              // more than one field, use first two for value, name
              while ($row = mysql_fetch_array($res)) {
                list($value,$name) = $row;
                $name = stripslashes(substr($name,0,100));
                $str .= '<option value="'.$value.'"';
                if($add_tag) {
                    $str .= ' id="' . $add_tag . $value . '"';
                }
                $str .= '>'.$name."&nbsp; </option>\n";
              }
            }
          } elseif (is_array($inp)) {
    
            foreach($inp as $value => $name) {
                if(is_array($name)) { // attempt to recover if you have an array.
                    $name = $name['label'];
                }
                $str .= '<option value="'.$value.'"';
                if($add_tag) {
                    $str .= ' id="' . $add_tag . $value . '"';
                }
                $str .= '>'.$name."</option>\n";
            }
          }
          if ($c and $str) {
            // update cache if we tried to get it from cache
            $ok = $c->save($str,$cache_string);
          }
        }
    
        // now munge the string
        if (!is_array($selected)) {
          $selected = array($selected);
        }
        foreach ($selected as $sel) {
            $search = '<option value="'.$sel.'"';
            if($add_tag) {
                $search .= ' id="' . $add_tag . $value . '"';
            }
            $search .= '>';
            $replace = '<option value="'.$sel.'"';
            if($add_tag) {
                $replace .= ' id="' . $add_tag . $value . '"';
            }
            $replace .= ' selected="selected">';
          $str = str_replace($search,$replace,$str);
        }
        if ($cache_hit) {
          $str = "<!-- Cache Hit [$cache_string]--> ".$str;
        } elseif ($cache_string) {
          $str = "<!-- Cache Miss [$cache_string]--> ".$str;
        }
        return $str;
    }
    
    /**
    * @version 1.0 Jonah B  8/24/16 9:42 AM
    */
    function showPageHeader($title) {
        require($_SERVER['DOCUMENT_ROOT']."/../dbos/html/header.phtml");
    }
    
    /**
    * @version 1.0 Jonah B  8/24/16 9:42 AM
    */
    function showPageFooter() {
        require($_SERVER['DOCUMENT_ROOT']."/../dbos/html/footer.phtml");
    }    

    /*
    *  Returns the grouping sql for various trend type reports based off of a
    *  "created_date" field.
    *  $v = date($group[dformat],strtotime($v));
    * $opts:
    *   - table - will append the table to the field to avoid ambiguity during joins
    *
    * @creator jonah
    *
    * @version 1.0 2/17/08 12:50 PM JB
    */
    
    function group_time($view="monthly",$dfield="date",$opts=false) {
        if(is_string($opts)) $opts = parse_css($opts);
        if(is_array($opts)) extract($opts);
    
        if($view=="explain") {
          show_errors("group_time(view,datefield,options).  Use in conjunction with select_views.");
          return false;
        }
    
        if($table) $dfield = $table.".".$dfield;
        if(!$sortby) $sortby = "DESC";
        $order_by = $dfield . " " . $sortby;
    
        switch($view) {
            default:
            case "monthly":
              $group_by = " CONCAT(YEAR($dfield), MONTH($dfield)) ";
              $date_col_label = "Month";
              $date_field = "monthly_date";
              $dformat = "m/Y";
              break;
            case "bimonthly":
              $group_by = ' bimonthly_date ';
              $date_col_label = "2 Month Period";
              $date_field = "bimonthly_date";
              $dformat = "m/Y";
              break;
            case "weekly":
              $group_by = " CONCAT(YEAR($dfield), WEEK($dfield)) ";
              $date_col_label = "Week of";
              $date_field = "weekly_date";
              $dformat = "W/Y";
              break;
            case "yearly":
            case "ytd":
              $group_by = " YEAR($dfield) ";
              $date_col_label = "Year";
              $date_field = "year";
              $dformat = "Y";
              break;
            case "quarterly":
              $group_by = " CONCAT(YEAR($dfield), QUARTER($dfield)) ";
              $date_col_label = "Quarter";
              $date_field = "monthly_date";
              $dformat = "m/Y";
              break;
            case "daily":
              $group_by= " LEFT($dfield, 10) ";
              $date_col_label = "Day";
              $date_field = "daily_date";
              $dformat = "m/d/Y";
        }
    
        $info['sql']= " GROUP BY {$group_by} ORDER BY {$order_by}";
        $info['group_by'] = $group_by;
        $info['order_by'] = $order_by;
        $info['date_field'] = $dfield;
        $info['dformat'] = $dformat;
        $info['label'] = $date_col_label;
    
        return $info;
    }    
    
    function dirify($in) {
        return $in;
    }
}  // end class ACTable


