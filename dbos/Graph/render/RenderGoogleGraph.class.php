<?php
/**
 * Generate flash graphs based on google api
 *
 * @license LGPL
 * @version $Id: RenderGoogleGraph.class.php 7079 2011-07-22 09:13:03Z jbaker $
 * @package ActivistCore
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */
require_once(dirname(__FILE__)."/GoogleGraph.class.php");

/**
* @version 1.0 SJP 2011
*/
class RenderGoogleGraph extends GoogleGraph {

    /*
    * Startup RenderGoogleGraph
    *
    *  @version 1.0 Sarah JP 08/20/2011 09:06PM
    */
    function __construct($graph) {
        if(!$this->range) $this->range = 20;
        $this->id = $this->dirify($graph->legend);
        $this->graph = $graph;
        switch($graph->type) {
            case"pie":
                $this->type = "PieChart";
                $this->load_pie();
                break;
            default:
                $this->load();
                break;
        }
    }
    
    
    function dirify($in) {
        $out = str_replace(" ","_",$in);
        return $out;
    }
    /*
    * Load line graph
    *
    * @version 1.1 Jonah B  3/19/14 12:25 PM - Allow more than 10 entried
    * @version 1.0 SJP 08/21/2011 09:11PM
    */
    function load() {
        if($_REQUEST['limit'])  {
            $this->limit_entries = (int) $_REQUEST['limit'];
        } elseif($this->limit) {
            $this->limit_entries = $this->limit;
        }
        
        $view_label = $this->graph->view;
        if(!$view_label) $view_label = "Label";
        if(!$this->limit_entries) $this->limit_entries = 10;
    
        $columns[$view_label] = "string";
        $columns[""] = "number";
        $this->columns = $columns;

        $data =  $this->graph->data;
        if($data) {
            $i = 0;
            foreach($data as $row) {
                $i++;
                if($i <= $this->limit_entries) {
                    extract($row);
                    $values[] = array($view_label=>$label,""=>$point);
                }
            }
        }
        $this->values = array_reverse($values);
    }

    /*
    * Load pie graph
    * @version 1.0 SJP 08/21/2011 09:11PM
    */
    function load_pie() {

        $columns['label'] = "string";
        $columns['point'] = "number";
        $this->columns = $columns;

        $data = $this->graph->data;
        if($data) {
            foreach($data as $row) {
                extract($row);
                $values[] = array("label"=>$label,"point"=>$point);
            }
        }

        $this->values = $values;

    }

    /*
    * Output the files to the browser necesarry to show the graph.
    *
    * @version 1.0 SJP 08/20/2011 09:07PM
    */
    function show() {
        if(!count($this->graph->data)) return;

        $this->title = $this->graph->legend;
        $this->toggle_table = true;

        if(!count($this->columns)) return;
        if(!count($this->values)) return;
        
        $this->addColumns($this->columns);
        $this->addValues($this->values);
        if($this->debug) da($this);
        $this->render();
    }

}