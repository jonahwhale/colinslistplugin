<?php
/**
 * ActivistCore pager class.
 *
 * @version $Id: class.browse.php 5416 2009-01-16 13:18:59Z jonah $
 * @liscense GPL
 * @creator Jonah Baker 2003
 * @package ActivistCore
 */
 
 /*
 *  Pager Class
 *
 *  This is a class that limits the size of pages created by
 *  SQL queries and lets you navigate between pages.
 *
 *  show_nav and show_nav_lite are the two primary functions.
 *
 * @version 1.2 - Fixed some issues with desc vs asc sort bys.  -  10/27/11 9:19 AM
 * @version 1.1 spacopac 2/14/08 5:48 PM
 *
 */

class Browse extends ACTable {
    var $max_rows = 25;  // default maximun number of rows to view on a page.
    var $db;
    var $sql;
    var $pass_variable = "&pass_variable=variable";  // needs to be set for each instance.
    var $sort_variable = "&pass_variable=variable";  // needs to be set for each instance.
    var $pass_hidden;
    var $found_rows; // calculated found rows
    var $refer_to_as = "records"; // people, events, etc.
    var $debug = false;
    var $_useDoctrine = true;
    var $_dbo = false;
    /**
    * Set it up.
    *
    * @version 1.1 Jonah B  4/28/16 6:51 PM
    */
    function __construct($sql = false, $dbo=false,$options=false) {
        $this->_dbo = $dbo;
        if(is_numeric($options)) {
            $this->set_rows = $options;
        } elseif(is_array($options)) {
            $opts = $options;
            unset($options);
            extract($opts);
            if($db) $database = $db;
            if($rows) $set_rows = $rows;
        }

        // Set the number of records per page.
        if($page_size) $max_rows = $page_size;
        if($max_rows) $set_rows = $max_rows;
        if(!$set_rows) $set_rows = "25";
        if($sort) $this->sort = $sort;
        if($sortby) $this->sortby = $sortby;

        $this->max_rows = $set_rows;

        $this->set_db($database);
        $this->start($sql);
    }

    /*
    * if there are any other globals we need, then I say, we're crazy
    *
    * @version 1.1 Now works with register globals off JB  7/21/10 1:15 PM
    */
    function collect_globals() {
        global $debug;
        global $sort, $sortby, $alpha;
        if (isset($_REQUEST['max_rows'])) $this->max_rows = $_REQUEST['max_rows'];
        if (isset($_REQUEST['position'])) $this->position = $_REQUEST['position'];

        if (isset($debug)) $this->debug = $debug;
        if (isset($_REQUEST['sort']) && !$this->sort) $this->sort = $_REQUEST['sort'];
        if (isset($_REQUEST['sort_by']) && !$this->sortby)  $this->sortby = $_REQUEST['sort_by'];
        if (isset($alpha)) $this->alpha = $alpha;
        $this->php_self = $_SERVER['PHP_SELF'];
    }

    /*
    * set the database.  if none given, use current value
    */
    function set_db($db = false) {
      if ($db === false) {
        $sql = "SELECT DATABASE()";
        $res = mysql_query($sql);
        $db = mysql_result($res,0,0);
      }
      $this->db = $db;
      return $db;
    }

    /*
    * @version 1.2 - If not asc then desc, causing sql errors.  JB  10/24/11 10:22 AM
    * @version 1.1 SJP 10/09/2009 10:00 PM
    */
    function start($sql,$opts=false) {
        global $R;
        if(is_array($opts)) extract($opts);
        if(!$sql) return false;

        unset($this->res);
        // pull in important global variables
        $this->collect_globals();

        if(is_string($this->sort)) $this->sort = array($this->sort);
        if(is_string($this->sortby)) $this->sortby = array($this->sortby);

        if($this->max_rows <= 0) $this->max_rows=$this->set_rows;

        /*
        * Sorting capablities by only one sort order -- SJP 10/09/2009 10:04 PM
        */
        if($this->sort && strpos($sql,"ORDER BY") === false) {
             //list($sql_string,$sql_order_by) = split("ORDER BY",$sql);

              $sql .= " ORDER BY ";
              if($this->sort && is_array($this->sort)) {
                $i = 0;
                foreach($this->sort as $sort_id => $sort) {
                    if($sort) {
                        $i++;
                        if($i > 1) $sql .= ", ";
                        // Default is desc, only name fields are asc.  Date and integer fields are desc.  JB  10/27/11 9:11 AM
                        if(strtoupper($this->sortby[$sort_id])!="DESC" && strtoupper($this->sortby[$sort_id])!="ASC") $this->sortby[$sort_id] = "DESC";
                        /*
                        if(!$this->sortby[$sort_id]) {
                            $this->sortby[$sort_id] = "DESC";  
                        } 
                        */
                        $sql .= $sort."   ";
                        if(is_array($this->sortby)) {
                            $sql .= $this->sortby[$sort_id];
                        } else {
                            $sql .= $this->sortby;
                        }
                    }
                }
              } else {
                  if(!$this->sortby) {
                      $this->sortby = "ASC";
                  } else {
                      $this->sortby = "DESC";
                  }
                  $sql .= $this->sort."  ".$this->sortby;
              }
        }

        /*
        * Here we need to make some sql calls and set a few variables
        * based off of the results.
        steps:
        1. munge the query to include sql_calc_found_rows and limit
        2. run query (ONCE ONLY!)
        3. set variables
        */

        if($this->debug) { echo "<hr>SQL from browse->start: $sql <hr>"; }
        if ($this->position < 1) $this->position = 1;
        $this->sql_in=$sql;

        //
        //  Here is where we could cache the result in a custom mysql table,
        //  and return the table result instead of the actual query itself.  This would be useful for caching,
        //  but also useful to ensure that the data being browsed isn't being changed as the person is doing data entry. JB  6/22/08 5:04 PM
        //
        if(!$use_table_cache) {
          $this->sql=$this->append_sql_limit($this->sql_in);
        } else {
          // do not limit, select all records so they can be inserted into the cache table.
        }
        $qstart = microtime(1);
        
        if($this->_useDoctrine) {
            $this->rows = $this->fetchAll($sql);
        } else {
            $this->res = acquery($this->sql,$this->db);
        }
        $this->query_time = sprintf("%.3f",microtime(1) - $qstart);
        
        // The following is experimental.  JB  6/22/08 5:13 PM
        if($use_cache_table) {
            $rowcount=0;
            while($row=mysql_fetch_assoc($this->res)) {
                if($rowcount==0) {
                    $create_table_sql .= "
                        CREATE TABLE $cache_table_name (
                    ";
                    foreach($row AS $f=>$v) {
                        $cache_table_name = "cache_".dirify($this->sql);
                        $create_table_sql .= " $f TEXT ,";
                    }
                    $create_table_sql .= " ) ";
                    mysql_db_query(CACHE_DB,$create_table_sql);
                    // Now do an insert for each row.
                }
                $rowcount++;
            }
        }
        
        if(!$this->_useDoctrine) {
            if (!$this->res) {
              mysql_die($this->sql);
            } else {
              $r = mysql_query("SELECT FOUND_ROWS()");
              if ($r) $this->all_rows = mysql_result($r,0,0);
              if (!$r) mysql_die("select found rows after query: $sql");
              $this->display_rows = mysql_num_rows($this->res);
            }
        }
        // extra url info
        $this->url = '&'.$this->pass_url_variable($this->sortby);

        // set up position variables - no need to do this more than once
        $this->num_rows = $this->display_rows;
        $this->pos_next = $this->position + $this->max_rows;
        $this->pos_prev = $this->position - $this->max_rows;
        if ($this->pos_prev < 1) $this->pos_prev = 1;
        $this->pos_end = $this->all_rows - $this->max_rows + 1;
        if ($this->pos_end < 1) $this->pos_end = 1;
        $this->pos_page_end = $this->pos_next-1;
        if($this->pos_page_end > $this->all_rows) $this->pos_page_end = $this->all_rows;
    }

    // return next result in result set
    function fetch() {
      return mysql_fetch_assoc($this->res);
    }

    // ?? What's this for?
    function max_rows($max_rows) {
      $this->max_rows=$max_rows;
      $this->start($this->sql_in);
    }

    // add the proper limits
    function append_sql_limit($query) {
      $sql_position = $this->position - 1;
      $query = $this->_mungequery($query);
      if(!$this->show_all) {
        $query .= " LIMIT {$sql_position}, {$this->max_rows} ";
      }
      return $query;
    }

    // little links
    function beginLink() {
      ?><a href="<?= $this->php_self ?>?position=1&max_rows=<?= $this->max_rows ?>&<?= $this->url ?>">&lt;&lt; Begin</a><?
    }

    function prevLink() {
      ?><a href="<?= $this->php_self ?>?position=<?= $this->pos_prev; ?>&max_rows=<?= $this->max_rows ?>&<?= $this->url ?>">&lt; Prev</a><?
    }

    /**
    * $_show_time specifies whether to display the
    * amount of time the query took to run.
    */

    function show_nav_lite($_show_time = 0) {
      $this->url = $this->pass_url_variable($this->pass_variable);
      if (func_num_args() == 1) {
        $this->show_time = $_show_time;
      } else {
        // this will default to showing the query run-time on
        // the second nav display -- usually at the bottom.
        $this->show_time = $this->_count_nav_called++;
      }

      ?><!--  beginning of table navigation bar -->
      <table border="0" align="center" bgcolor="#FFFFFF"><?
      if ($this->position!=1) {
        ?><tr><td><? $this->beginLink() ?></td><td><? $this->prevLink() ?></td><?
      }
      ?><td> Displaying <b><?= "$this->position - $this->pos_page_end"; ?></b> of <b id="records_found"><?= $this->all_rows; ?></b> <?= $this->refer_to_as ?>.
        <td><?
        if((($this->position + $this->max_rows) <= $this->all_rows) && $this->display_rows!=$this->all_rows) {
          $this->show_next();
        }
        ?></td>
        </tr><?
        if ($this->show_time) {
          ?><tr><td colspan="4" align=center><span class="caption">Query took <?= $this->query_time ?> seconds.</span></td></tr><?
        }
      ?></table>
      <!--  end of table navigation bar -->
      <?php
      if($this->debug) {
        echo "<br> The all_rows variable is: $this->all_rows <br>";
        echo "<br> The position variable is: $this->position <br>";
        echo "<br> The max_rows variable is: $this->max_rows <br>";
      }
    }  // end function show_table_nav_lite

    function show_next() {
      ?>&nbsp;<a href="<?= $this->php_self; ?>?position=<?= $this->pos_next; ?>&max_rows=<?= $this->max_rows ?>&<?= $this->pass_variable ?>">Next &gt;</a>&nbsp;&nbsp;<a href="<?= $this->php_self ?>?position=<?= $this->pos_end ?>&max_rows=<?= $this->max_rows ?>&<?= $this->pass_variable ?>">End &gt;&gt;</a><?
    }  // end show_next function

    // this function needs to be customized for various class extensions.

    function pass_url_variable($arg) {
      $pv = $this->parse_query_string($this->pass_variable);
      $this->pass_variable = $this->build_query_string($pv);
      return $this->pass_variable;
    }
    
    function parse_query_string($in) {
        return $in;
    }

    function build_query_string($in) {
    
    }
    
    function add_var($in) {
        // The things we don't want to pass.
        if($this->contains("WT_FPC",$in)) return false;
        if($this->contains("PHPSESSID",$in)) return false;
        if($this->contains("position",$in)) return false;
        if($this->contains("max_rows",$in)) return false;
        if($this->contains("alpha=",$in)) return false;

        if(is_string($in)) {
          if($this->contains('=',$in)) {
            $this->pass_variable .= $in;
          }
        }
    }

    function pass_hidden_variable() {
        $url_var = $this->pass_variable;
        $pairs = explode("&", $url_var);
        foreach($pairs as $pair) {
          list($k,$v) = explode("=", $pair);
          if($k and $k != "max_rows") {
            ?><input type="hidden" name="<?= $k; ?>" value="<?= $v; ?>"><?
          }
        }
    }

    function show_nav($_show_time = 0) {
      $this->url = $this->pass_url_variable($this->pass_variable);
      if (func_num_args() == 1) {
        $this->show_time = $_show_time;
      } else {
        // this will default to showing the query run-time on
        // the second nav display -- usually at the bottom.
        $this->show_time = $this->_count_nav_called++;
      }

      ?><!--  beginning of table navigation bar -->
      <table border="0" align="center" bgcolor="#FFFFFF"><?
      if ($this->position!=1) {
        ?><tr><td><? $this->beginLink() ?></td><td><? $this->prevLink() ?></td><?
      }
      ?><td><table><tr><td><?
      
      if($this->display_rows!=$this->all_rows || $this->display_rows>"51" || 1==1) {
        ?><form method="get" action="<?= $this->php_self ?>" >
        <input type="submit" value="Show">
        <input type="hidden" name="alpha" value="<?= $this->alpha ?>">
        <input type="text" name="max_rows" size="5" value="<?= $this->max_rows ?>">
        rows starting from
        <input name="position" type="text" size="5" value="<?= $this->pos_next ?>"><?
          $this->pass_hidden_variable();
        ?></form><?
      }
      ?></td></tr></table></td><td><?
      if((($this->position + $this->max_rows) <= $this->all_rows) && $this->display_rows!=$this->all_rows) {
          $this->show_next();
      }
      ?></table><?
      if($this->display_rows!=$this->all_rows || 1==1) {
        ?><center><a href="<?= $this->php_self ?>?position=1&max_rows=<?= $this->all_rows ?>&<?= $this->url ?>">Show all records </a></center><br /><?
      }
      if ($this->show_time == 1) {
        ?><center><span class="caption">Query took <?= $this->query_time ?> seconds.</span></center><?
      }
      ?><!--  end of table navigation bar --><?
    }  // end show_table_navigation function

    /*
    * @creator Sam Withrow 2007
    * @private
    * @abstract
    * adds the SQL_CALC_FOUND_ROWS directive to a select query, if it's not already there
    */

    function _mungequery($sql) {
      if (preg_match('/^\s*select((?:\s+sql_calc_found_rows\b)?\s+)(.*)$/is',$sql,$m)) {
        return "SELECT SQL_CALC_FOUND_ROWS {$m[2]}";
      }
      return $sql;
    }

    /**
     * Create string for sort and pass varilable
     * @version 1.0 SJP 11/24/2009 04:57 pm
     */
    function generatePassVars($pass_vars) {
        if($pass_vars && is_array($pass_vars)) {
            $pass_vars = build_query_string($pass_vars);
        }
        $this->sort_variable = $pass_vars;
        $sort_vars = $this->generateSortVars();
        $this->pass_variable = $pass_vars.$sort_vars;
    }

    /**
     * Generate sorting pass query
     * @version 1.0 SJP 11/24/2009 04:57 pm
     */
    function generateSortVars() {
        $sort_vars = "";
        if(count($this->sort)) {
            foreach($this->sort as $sort => $sort_name) {
                if($sort_name) {
                    $sort_vars .= "&sort[{$sort}]=".$sort_name;
                    $sort_vars .= "&sortby[{$sort}]=".$this->sortby[$sort];
                }
            }
        }
        return $sort_vars;
    }

    /**
     * Get link for sorting headers
     *
     * @version 1.1 JB - Fixed bug where array was expected -  9/2/11 4:04 PM
     * @version 1.0 SJP 08/02/2009 10:18 AM
     */
    function getSortVariable($var_name,$var_sort) {
        if(is_array($this->sort)) {
            if(in_array($var_sort,$this->sort))  {
                $sort = array_search($var_sort,$this->sort);
                 if($this->sortby[$sort] == "ASC")  {
                    $var_sort_by = "DESC";
                    $var_sort_by_name = "Descending";
                }
            }
        }
        if(!$var_sort_by) $var_sort_by = "ASC";
        if(!$var_sort_by_name) $var_sort_by_name = "Ascending";
        ?>
        <a 
            class="sortby" 
            href="<?= $this->php_self ?>?page=<?= $_REQUEST['page']; ?>&action=<?= $_REQUEST['action']; ?>&sort=<?= $var_sort ?>&sortby=<?= $var_sort_by ?>&<?= $this->sort_variable ?>" 
            title="Sort <?= $var_sort_by_name ?>">
            <?= $var_name ?>
        </a><?
    }

    /**
     * Gets browse variable for display options
     * @function getSortImg
     * @created SJP 08/02/2009 10:17 AM
     */
    function getSortImg($var_sort) {
        if(is_array($this->sort)) {
            if(in_array($var_sort,$this->sort))  {
                $sort = array_search($var_sort,$this->sort);
                if($this->sortby[$sort] == "ASC") {
                    ?> <img src="/admin/images_oa/sort_up_arrow.gif" width="7" height="6" border="0" /><?
                } else {
                    ?> <img src="/admin/images_oa/sort_down_arrow.gif" width="7" height="6" border="0" /><?
                }
            }
        } else {
            if($this->sort == $var_sort && $this->sortby == "ASC")  {
                ?><img src="/admin/images_oa/sort_up_arrow.gif" width="7" height="6" border="0" /><?
            } elseif($this->sort == $var_sort && $this->sortby == "DESC") {
                ?><img src="/admin/images_oa/sort_down_arrow.gif" width="7" height="6" border="0" /><?
            }
        }
    }

    /**
     * Get sorting headers
     * @version 1.0 SJP 07/29/2010 12:35AM
     */
    function getSortHeader($var_name,$var_sort) {
        $this->getSortVariable($var_name,$var_sort);
        $this->getSortImg($var_sort);
    }
    
    /**
     * @version 1.0 Jonah B  9/14/18 3:55 PM
     */
    function fetchAll($sql) {
        global $wpdb;
        return $wpdb->get_results($sql);
        /*
          // print_r($sql);
          $statement = $this->_dbo->prepare($sql);
          $statement->execute();
          $out = $statement->fetchAll();
          return $out;    
        */
    }
} // end class browse

