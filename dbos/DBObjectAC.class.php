<?php
/**
* DBObjectAC deals with objects and database, not UI display.
* Also see DBOS which extends DBObjectAC and gives more html editing/ajax functions.
*
* ACTable
*  - ACTree
*    - DBObjectAC
*      - DBOS
* @version $Id: DBObjectAC.class.php 5085 2008-10-09 15:59:06Z jonah $
* @package ActiveCore
* @liscense LGPL
* @author Sam Withrow
* @author Jonah Baker, Aaron Negeurbaur, GPUSA, CCIE, World Forum Foundation
*
* @version 1.1 - opinvoice, made camelcap compatible - Jonah B  1/17/12 12:53 PM
* @version 1.0 Sam Withrow 5/22/08 12:52 PM
*/

/**
* Include required parent classes.
*/

include_once(dirname(__FILE__)."/ACTable.class.php");

/**
* Object oriented database functions.  At the heart of the ActiveCore framework.
* Also see DBOS which extends DBObjectAC and gives more html editing/ajax functions.
*
* @package ActiveCore
* @author Sam Withrow 2007
* @liscense GPL
*
* @version 1.1 - opinvoice, made camelcap compatible - Jonah B  1/17/12 12:53 PM
* @version 1.0 Sam Withrow 5/22/08 12:52 PM
*/

abstract class DBObjectAC extends ACTable {
    // items pertaining to subclass
    protected $_db;
    protected $_table;
    /**
    * The unique record identifier field(s), array if multiple
    */
    protected $_idField;
    /**
    * We assume that there is a 'modified' field on all tables.
    */
    var $_timeStampField = 'modified';
    
    /**
    * Jonah B added  7/12/13 12:21 PM
    */
    var $_onDuplicateKey = true;
    
    /**
    * database connection, doctrine preferred.
    */
    protected $_dbo; 

    // individual items pertaining to this instance
    protected $_loaded = 0;
    protected $_dbValues = array();
    protected $_validationErrors = array();
    var $_logging = false;


    // holds table field info for all subclasses
    protected static $_tableInfo = array();
    protected static $_classes   = array();
    protected $_related = array();
    /**
    * parent relational table
    */
    var $_parentTable;
    var $_parentDB;
    var $_parentTitleField;
    var $_parentIdField;
    
    /**
    * For human readable output in place of the table name.  JB  7/13/10 2:18 PM
    */
    var $_referToAs;
    
    var $_lowercase = true;
    /**
    * Register by handing __contruct the record id only.
    */
    public function __construct($id = false) {
      $c = get_class($this);
      if (!isset(self::$_classes[$c])) {
        self::register($c,$this->_db,$this->_table,$this->_idField);
      }
      if ($idf = $this->_idField) {
        if ($this->isId($id)) {
          $this->$idf = $id;
          if ($this->exists($id)) $this->loadFields($id);
        }
      }
    }

    /**
    * register a class, to be used with DBObjectAC::factory()
    */
    public function register($className,$dbName,$tableName,$idField = '') {
      $ar = array(
        'db'    => $dbName,
        'table' => $tableName,
        'fullTable' => "$dbName.$tableName",
      );
      if ($idField != '') {
        $ar['idField'] = $idField;
      }
      self::$_classes[$className] = $ar;
      self::_loadTableInfo($className);
    }

    public function getById($id = false) {
      if (!isset($this)) {
        throw new Exception("Instatiating DBObjectAC When Class is Unknown!");
      }
      $c = get_class($this);
      return new $c($id);
    }

    /**
    * Find the record that has the same variables set as this object
    *
    * @version 1.1 Jonah B  7/6/15 11:17 AM - return array and accept
    * @version 1.0 Sam Withrow
    */
    public function find($where=false) {
        if($where) {
            $str = $where;
        } else {
            $c = 0;
            $str = '';
            foreach ($this->getTableFields as $tf) {
              $v = $this->getField($tf);
              if ($c++) $str .= ' AND ';
              $ev = mysql_real_escape_string($v);
              $str .= "`$k` = '$ev'";
            }
        }
        $sql = "SELECT * FROM `{$this->_table}` WHERE {$str}";
        
        $res = mysql_db_query($this->_db,$sql) or mysql_die($sql);
        while($row = mysql_fetch_assoc($res)) {
            $out[] = $row;
        }
        return $out;
    }
    
    /**
    * @version 1.0 Jonah B  7/1/16 9:42 AM
    */
    function uniqueField($id) {
        if(!is_numeric($id)) {
            $this->{$this->_titleField} = $id;
            unset($id);
            $getid = $this->fetchOne("SELECT id FROM ".$this->_table." WHERE ".$this->_titleField."='".mysql_escape_string($this->{$this->_titleField})."'");
            $id = $getid['id'];
        }
        return $id;
    }

    /**
    * Load the values into the object.
    */

    public function loadFields($id = false) {
        if ($id === false) return false;
        $this->clearFields();
        if ($this->isId($id)) {
          $this->_load($id);
        } elseif ($id == 'new') {
          $this->setField($this->_idField,'new');
        }
    }

    /**
    * To be used when the $_table vars are set for the class when the class has been
    * manually defined.
    *
    * @version 1.0 JB  8/12/08 12:16 PM
    */

    public function loadRecord($id) {
        $this->id = $id;
        
        $this->_load($id);
        
        if(is_array($this->_dbValues)) {
            foreach($this->_dbValues AS $f=>$v) {
                // $f = strtolower($f);
                $this->$f = $v;
            }
        }
    }

    /**
     * Loads the record, but puts db fields into their own array, see loadRecord
     * for complete load.
     *
     * @version 1.0  Sam Withrow 2007
     * @returns True/False
     */

    protected function _load($id) {
        global $wpdb;
        $debug = false;
        // select table fields...
        $retval = false;
        
        $this->id = $id;
        if(!$this->_titleField) {
            $this->_titleField = $this->_idField;
        }

        // If id is an array, set the variables.
        if(is_array($id)) {
            if(count($id)==2) {
                foreach($id AS $field=>$value) {
                  $this->$field = $value;
                }

            }
        }

        $sql = "SELECT * FROM `{$wpdb->prefix}{$this->_table}` WHERE ";
        if(is_string($this->_idField)) {
          $sql .= " {$this->_idField} = '{$id}' ";
        } elseif(is_array($this->_idField)) {
          foreach($this->_idField AS $count=>$field) {
            // In theory this will always be numeric, but i'll allow for whatever
            // unless someone objects.  JB  8/10/08 3:54 PM
            $sql_pair[] = " {$field} = '{$this->$field}' ";

          }
            $sql .= implode(" AND ",$sql_pair);
        }
        if($debug) echo $sql;
        
        $rows = $this->fetchAll($sql);
        if($debug) $this->da($rows);
        
        if (count($rows)) {
            foreach ($rows[0] as $k => $v) {
              $this->_dbValues[$k] = $v;
              // WHy not? JB  8/13/08 8:29 AM
              // $this->$k = $v;
              // See loadRecord JB  9/4/08 11:33 AM
            }
            $retval = true;
            $this->id = $id;
            $this->_loaded=true;
        } else {
            $retval = false;
            $this->_loaded=false;
        }
        
        return $retval;
    }

    public function clearFields() {
        $this->_loaded = 0;
        foreach ($this->getTableFields() as $tf) {
            if(isset($this->$tf)) {
                unset($this->$tf);
            }
        }
        $this->_dbValues = array();
        $this->_validationErrors = array();
    }

    public function validate() { return true; }

    public function getValidationErrors() {
      $out = array();
      foreach ($this->_validationErrors as $ve) {
        list($f,$str) = $ve;
        $out[$f] = $str;
      }
      return $out;
    }
    public function addValidationError($field,$str) {
      $this->_validationErrors[] = array($field,$str);
    }

    public function __set ( $name, $value ) {
      $this->setField($name,$value);
    }

    public function setField($field,$val) {
      $this->$field = $val;
    }

    public function setFields($fvals) {
      foreach ($fvals as $f => $v) {
        $this->$f = $v;
      }
    }

    public function getId () {
      return $this->getField($this->_idField);
    }
    public function __get ( $name ) {
      return $this->getField($name);
    }

    public function getField($name) {
        if(!$name) return false;
        if(is_array($name)) {
            foreach($name AS $field=>$value) {
                $out[] = $field."=".$value;
            }
            $str = implode("&",$out);
            return $str;
        }
        $methname = '_get_'.$name;
        if (method_exists ($this, $methname )) {
            return $this->$methname();
        } elseif(isset($this->$name)) {
            return $this->$name;
        } elseif(is_array($this->_dbValues)) {
            if(isset($this->_dbValues[$name])) {
              return $this->_dbValues[$name];
            }
        } else {
            return false;
        }
    }

    public function getPropertyInfo() {
    }

    public function getFields() {
      $fs = $this->getTableFields();
      $out = array();
      foreach ($fs as $fn) {
        $out[$fn] = $this->getField($fn);
      }
      return $out;
    }

    protected function _getFullTable($className = '') {
        if(is_object($this)) {
            if($this->_table) return $this->_table;
        }

        if ($className == '') {
          if (isset($this)) {
            $className = get_class($this);
          } else {
            throw new Exception("Tried to _getFullTable() without a class");
          }
        }
        if (isset(self::$_classes[$className]['fullTable'])) {
          return self::$_classes[$className]['fullTable'];
        }
    }

    public function loadTable($className=false) {
        return $this->_loadTableInfo($className=false);
    }

    /*
    * Load information about the table and fields from mysql.
    *
    * @version 1.1  4/17/08 6:44 PM by Jonah
    * @version 1.0 Sam Withrow 2007
    */

    public function _loadTableInfo($className=false) {
        if(!$className) $className = get_class($this);
        $tn = self::_getFullTable($className);
        list($db,$tbl) = explode('.',$tn);
        
        if(!$tbl) { // if table wasn't set by the method above, it failed.
            $tn = $this->_db.".".$this->_table;
            $db = $this->_db;
        }

        if (isset(self::$_tableInfo[$tn])) {
            return true;
        } else {
            
            $sql = "SHOW FULL COLUMNS FROM `$tn`";
            $columns = $this->fetchAll($sql);
            if(!$columns) return false;
            $ti = array();
            
            foreach ($columns AS $row) {
              // $fn = strtolower($row['Field']);  // This was causing problems JB  11/8/11 9:17 AM
              $ti[$fn] = array();
              foreach ($row as $k => $v) {
                $ti[$fn][$k] = $v;
              }
            }
            self::$_tableInfo[$tn] = $ti;
            mysql_free_result($res);
            return true;
        }
    }

    /*
    *
    * @returns Array of fields in the table.
    */
    public function getTableFields() {
        if($this->_columns) {
            return $this->_columns;
        } else {
            $o = array();
            $tn = $this->_db.'.'.$this->_table;
            if(!self::$_tableInfo[$tn]) {
                self::$_tableInfo[$tn] = $this->_loadTableInfo();
            }
            return array_keys(self::$_tableInfo[$tn]);
        }
    }

    /**
    * Get all object fields, with HTML Entities Substituted
    */
    public function getFieldsHtmlEscaped() {
        // get all fields and html escape them
        $fvals = $this->getFields();
        $out = array();
        foreach ($fvals as $k => $v) {
          $out[$k] = htmlentities($v);
        }
        return $out;
    }

    /**
     * remove record from database
     *
     * 
     * @returns true/false
     *
     * @version 1.1 JB added multi-index row support 2015
     * @version 1.0 Sam Withrow 2007
     */

    public function delete() {
        $idval = $this->getId();
        if (!$this->hasId()) return;
        $debug = false;
        $tn = $this->_getFullTable();

        $sql = "DELETE FROM {$this->_table} WHERE ";
        $sql .= $this->getWhere();
        $sql .= " LIMIT 1";
        if($debug) echo($sql);
        if(is_object($this->_dbo)) {
            $this->_dbo->query($sql);
        } else {
            $res = mysql_db_query($this->_db,$sql) or mysql_die($sql);
        }
        
        if (isset($res)) {

            /*
            * Log database event -- SJP 07/06/2010 11:54PM
            * @todo For survey responses this is saying that the record was added then changed when it is just changed. JB  7/13/10 11:45 AM
            */
            if($this->person_id) {
                $log = new PersonChangeLog($this->person_id,$this);
                $log->logDelete();
            }

          return true;
        } else {
          return false;
        }
    }

    /*
    * @author Jonah  8/10/08 4:13 PM
    *  Return a string with the where sql pair.
    */

    public function getWhere() {
        if(is_string($this->_idField)) {
          $idf = $this->_idField;
          $sql_where = " $this->_idField = '{$this->$idf}' ";

        } elseif(is_array($this->_idField)) {
            foreach($this->_idField AS $field) {
              $sql_set[] = " $field = '{$this->$field}' " ;
            }
            $sql_where = implode(" AND ", $sql_set);
        }
        return $sql_where;
    }
    
    public function getDebug() {
        $debug = false;
        if($_REQUEST && isset($_REQUEST['debug'])) {
            $debug = $_REQUEST['debug'];
        }
        return $debug;
    }
    
   /***
    *
    * Save the object record to the database
    *
    * @returns true/false (JB would rather that it returns the record id or false  8/12/08 5:58 PM)
    *
    * @version 1.9 JB - Added ability to copy record.
    * @version 1.8 JB - Special treatment for int type numbers
    * @version 1.7 JB  7/27/10 10:19 AM - Fixed setting of table info bug
    * @version 1.6 JB GP 11/23/08 8:51 AM - Changed to return id instead of true.
    * @version 1.5 JB  10/25/08 4:20 PM
    * - fixed bug that might have done weird things to date fields.
    *
    * @version 1.4 JB  8/12/08 5:59 PM
    *  - added support for multi-index fields
    *  - return false if no fields have changed.
    *
    * @version 1.3 jonah  7/30/08 1:27 PM
    *  - fixed 'changed fields' handling
    *
    * @version 1.2 jonah  7/21/08 2:19 PM
    *   - Added 'on duplicate key' to inserts.
    * @version 1.0 Sam Withrow 2007
    */

    public function save() {
        global $wpdb;
        $debug = $this->getDebug();
        $changed_fields = array();

        // Save the record as a new record with fields from an existing record.  JB  7/25/11 4:51 PM
        if(isset($_REQUEST['copyRecord']) && $_REQUEST['copyRecord']) {
                $this->{$this->_idField} = "new";
                unset($this->_dbValues);
        }

        $table_fields = $this->getTableFields();
        if($debug) $this->da($table_fields);
        
        foreach ($table_fields as $k=>$info) {
            if(is_numeric($k)) {
                $k=$info;
            }
            
            if ($k == $this->_idField) continue;  // Assuming we are not manually setting id fields
            
            if ($k == $this->_timeStampField || $k=="modified") continue;
            
            $v = $this->getField($k);
            
            // Be smart about dates.  JB  10/8/08 4:48 PM
            if($debug) { $this->da("DBObjectAC->save says hello table field $k value $v id: ".$this->{$this->_idField}.""); }
            
            if(!self::$_tableInfo[$this->_table][$k]) {
                self::$_tableInfo[$this->_table] = $this->loadTable(); // This was pointing to $k, instead of setting the whole array.  JB  7/27/10 10:19 AM
                // da(self::$_tableInfo[$this->_table]);
            }
            
            if(!$info) {
                $field_type = self::$_tableInfo[$this->_db.".".$this->_table][$k]['type'];
            } else {
                //$this->da($info);
                if(is_array($info)) {
                    $field_type = $info['Type'];
                }
            }
            // Do some date transformations so people can use various date formats and not worry about it
            if(($field_type=="datetime" || $field_type=="date") && $this->_dbValues[$k] != $v && is_string($v)) {
                $tmp_date = $v;
                if(trim($v)) {
                    // Fix for some time situations that confuse strtotime
                    $tparts = explode(" ",$v);
                    if(is_array($tparts)) {
                      if(isset($tparts[2]) && strtolower($tparts[2])=="pm") {
                          if(substr($tparts[1],0,2) > 12) $v = str_replace("pm","",$v);
                      }
                    }
                    $timestamp = strtotime($v);
                    $v = date("Y-m-d H:i:s", $timestamp);
                    $this->$k = $v;
                 }
                 if($debug) $this->da("This is a date: $tmp_date : $v ");
            } elseif($field_type=="int" || $field_type=="decimal" && $this->_dbValues[$k] != $v) {
                if(is_string($v)) {
                    $v = trim($v);
                    $v = str_replace(",","",$v);
                    $v = str_replace("$","",$v);
                }
            } elseif($field_type=="timestamp" || $k=="modified") {
                if($v=="0000-00-00 00:00:00") $v = date("Y-m-d H:i:s");
            }
            
            if (
                
                is_array($this->_dbValues) 
                && $this->_dbValues[$k] != $v
                
            ) {
                $changed_fields[$k] = $v;
                if($debug) $this->da($changed_fields);
            } else {
                if($debug) $this->da('Field not changed for '.$k.' '.$v);
            }
        }

        $is_update = 0;

        // deal with index fields
        if(is_string($this->_idField)) {
            $idf = $this->_idField;
            if ($id = $this->{$idf}) {
                // $this->da($this);
                if ($this->isId($id)) {
                    if ($this->exists($id)) {
                        $is_update = 1;
                    } else {
                        // add id to fields to insert
                        $had_id = 1;
                        $changed_fields[$idf] = $id;
                    }
                }
                // if there's no id, we just hope for having an id from auto_increment
                $sql_where =  " `{$idf}` = '{$id}' ";
                // $this->dump($sql_where);
            }
        } elseif(is_array($this->_idField)) {
            unset($sql_set);
            $is_update = 0;
            foreach($this->_idField AS $field) {
                if(!empty($this->$field)) {
                    $is_update = 1; // JB changed to 1, since if either of these id fields are set then probably its an update
                }
              $sql_set[] = " $field = '{$this->$field}' " ;
            }
            $is_update = 0;
            $sql_where = implode(" AND ", $sql_set);
        }

        // Don't do the query if we don't seem to have anything new to save.
        if (empty($changed_fields) && !$is_update) {
            // When creating a new record it is legitimate to not set any fields.
            if($this->debug) show_errors("There were no fields to change.");
            return false;
        } else {
            if($this->debug) $this->debug($changed_fields);
        }


        $ss = $this->_setstr($changed_fields);
        $set_sql = "{$ss}";
        if(!$set_sql) {
            // apparently, nothing has changed.
            return false;
        }
        
        if(isset($_REQUEST['copyRecord']) && $_REQUEST['copyRecord']) $is_update = 0;
        
        if ($is_update) {
            $sql = "UPDATE `{$this->_table}` 
                SET {$set_sql} 
            ";
            if($this->_useUUID && !$this->uuid) {
                $sql .= ", uuid = UUID()";
            }
            $sql .= "    WHERE {$sql_where} LIMIT 1";
        } else {
            $sql = "INSERT INTO `{$this->_table}` 
                SET {$set_sql} ";
            if($this->_useUUID && !$this->uuid) {
                $sql .= ", uuid = UUID()";
            }
            if($this->_onDuplicateKey) $sql .= " ON DUPLICATE KEY UPDATE {$set_sql}";
        }

        
        
        
        //$ok = $this->_dbo->query($sql);
        
        if($wpdb) {
            $sql = str_replace('`'.$this->_table.'`','`'.$wpdb->prefix.$this->_table.'`',$sql);
            if($debug) $this->da($sql);
            $wpdb->query($sql);
        } else {
            $ok = $this->_dbo->query($sql);
        }
        
        if($this->debug || $debug) echo "SQL: $sql\n";
        $this->last_sql = $sql;
        /*
        $ok = mysql_db_query($this->_db, $sql);
        */
        if($this->debug) $this->debug($sql);
        if($debug) $this->da($ok);
        
        if ($ok===false) {
            mysql_die($sql);
            return false;
        } else {
            if (!$is_update and !$had_id) {
                // $id = mysql_insert_id();
                if($wpdb) {
                    $id = $wpdb->insert_id;
                } else {
                    $id = $this->_dbo->lastInsertId();
                }
            }

            $this->_triggerUpdateEvent();

            $this->loadFields($id);
            return $id;
        }
    }

    public function _triggerUpdateEvent() {
        
    }
    
    /**
     * @returns array of target table info
     *
     * @version 1.1 - ActiveCore Jonah B  10/17/12 5:46 PM
     * @version 1.0 - OpFAT - Jonah B  10/17/12 5:41 PM
     */
    public function _parseRelation($in) {
        
        $pts = explode('.',$in);
        if(count($pts)==1) {
            $in = $this->_fieldRelations[$in];
            $pts = explode('.',$in);
        }
        if(count($pts)==2) {
                $out['table'] = $pts[0];
                $out['field'] = $pts[1];
        }

        if($this->contains(':',$out['field'])) {
            list($out['field'],$out['select_field']) = explode(':',$pts[1]);
        } else {
            $out['field'] = $pts[1];
        }
        return $out;
    }

    /**
    * @version 1.0 JB  10/8/12 4:20 PM
    */
    public function _saveManyRelationship($in) {
        global $db;
        da($in);
        extract($in);
        foreach($in['selected'] AS $toIdValue) {
            $sql = " 
            INSERT INTO $multiTable SET 
                $fromId = $this->id,
                $joinField = $toIdValue
            ON DUPLICATE KEY UPDATE $fromId=LAST_INSERT_ID($fromId)
            ";
            $db->query($sql);
        }
        // if something is not selected, then delete it.
        if(is_array($in['dbValues'])) {
            foreach($in['dbValues'] AS $dbId=>$info) {
                if(!$in['selected'][$dbId]) {
                    $sql = "DELETE FROM $multiTable WHERE $fromId = $this->id AND $joinField = $dbId ";
                    $db->query($sql);
                }
            }
        }
    }

    /**
    * Expects something like
    * 'userdiversitydata'=>array('userdiversitydata.userId','userdiversitydata.diversitydataId','diversitydata.name,diversitydata.id')
    *
    * @version 1.0 Jonah B -OpFAT luigi - 10/17/12 5:29 PM
    */
    public function _parseManyRelation($field) {
        $info = $this->_fieldRelations[$field];
        list($out['fromTable'],$out['fromId']) = explode(".",$info[0]);
        list($out['toName'],$out['toId']) = explode(",",$info[2]);
        list($out['toTable'],$out['toTableNameField']) = explode(".",$out['toName']);
        list($out['multiTable'],$out['joinField']) = explode(".",$info[1]);
        return $out;
    }


    /**
    * @version 1.0 Jonah B - OpFat mario - 10/17/12 5:27 PM
    */
    public function _loadManyField($field) {
        global $db;
        $debug = false;
        $rel = $this->_parseManyRelation($field);
        if($debug) da($rel);
        extract($rel);
        // See if this user has anything stored in the db.
        if(is_numeric($this->id)) {
            $sql = " SELECT  $toId AS id, $toName AS name
                FROM $multiTable
                    JOIN $toTable ON ($fromTable.$joinField = $toId)
                WHERE $multiTable.$fromId = $this->id
                ";

            $dds = $db->fetchAssoc($sql);
            // da($dds);
            $out['dbValues'] = $dds;
        }
        $out['selected'] = $_POST[$this->_table][$multiTable];
        $out['multiTable'] = $multiTable;
        $out['fromId'] = $fromId;
        $out['joinField'] = $joinField;
        return $out;
    }
    

    /**
    *
    * Whether or not a record with the given ID exists in the DB
    *
    * @return bool
    */

    public function exists($id = '') {
        global $wpdb;
        $idf = $this->_idField;
        if ($id === '') $id = $this->getId();
        // bail if a lookup would obviously fail
        if (!$this->hasId()) return false;
        $sql = "SELECT `{$wpdb->prefix}{$this->_idField}` FROM `{$this->_table}` 
            WHERE `{$this->_idField}` = '".(int)$id."'";
        $foundit = false;
        $r = $this->fetchAll($sql);
        if(count($r)) $foundit = true;
 
        return $foundit;
    }

    /**
     * @author Sam Withrow 2007
     * hasId() - say whether this has a valid id (may or may not exist in DB)
     *
     * default implementation just checks if it's an integer.
     *
     * @version 1.1 JB  8/10/08 4:19 PM
     *  - added mutli-index field support.
     * @return bool
     */

    public function hasId() {
        if(is_string($this->_idField)) {
            if ($this->isId($this->getId())) return true;
            else return false;
        } elseif(is_array($this->_idField)) {
            // multi-field array, make sure they are all set.  JB  8/10/08 4:18 PM
            foreach($this->_idField as $field) {
              if(!$this->$field) return false;
            }
            return true;
        } else {
            return false;
        }
    }

    /**
    * say whether the given value could be a valid ID
    */

    public function isId($val) {
      if ($val and is_numeric($val)) return true;
      return false;
    }

    /**
     * @var $ar - array of the variables to be set field=>value
     * @returns this = that, type string for a sql insert.
     * @version 1.1 added better 'human' formatted date handling. JB  5/22/08 1:06 PM
     */

    private function _setstr($ar) {
        $os = array();
        foreach ($ar as $k=>$v) {
            $ssv = stripslashes($v);
            if(function_exists('mysql_escape_string')) {
                $ev = mysql_escape_string($ssv);
            } else {
                $ev = esc_sql($ssv);
            }

            if ($this->_isDateField($k)) {
                $dv = trim($ev);

                if ($dv == 'now' or $dv=='now()') {
                  $os[] = "`$k` = NOW()";
                } else {
                  // look for 'human' formatting
                  if($this->contains("/",$ev) || $this->contains(",",$ev)) {
                      $timestamp = strtotime($ev);
                      $ev = date("Y-m-d H:i:s", $timestamp);
                  }
                  $os[] = "`$k` = '$ev'";
                }
            } else {
                $os[] = "`$k` = '$ev'";
            }
        }
        $ss = join(", ",$os);
        return $ss;
    }

    protected function _isDateField($f) {
      $tn = $this->_db.'.'.$this->_table;
      if ($ft = self::$_tableInfo[$tn][$f]['type']) {
        if (preg_match('@^(?:date|time)@i',$ft)) {
          return true;
        }
      }
      return false;
    }

      // This is a general function for deleting records that will
      // ask the user if she is sure before doing anything.

      function deleteVerify($name=false) {
        global $sure, $id, $h_section, $delete;
        global $theme, $language, $type, $admin_dir;
        if($name) {
            $title = $name;
        } else {
            $title = $this->nameField;
        }
        if (empty($sure)) {
          start_box();
          ?>
          <br>
          <form method="get" action="<?php echo $PHP_SELF; ?>" name="surething">
          <p align="center">Are you sure you want to delete the record for <b><?= $title ?></b>?</p>
          <p align="center">
            <input type="radio" name="sure" value="1" checked>
            Yes
            <input type="radio" name="sure" value="0">
            No</p>
          <p align="center">
          <input type=hidden name='action' value='delete_record'>
          <input type="hidden" name="table" value="<?= $this->_table ?>">
           <input type=hidden name='id' value='<?= $this->id ?>'>
          <? if(isset($type)) {
            ?><input type="hidden" name="type" value="<?php echo $type; ?>"><?php
          } ?>
          <input type="hidden" name="<?= $name ?>" value="<?php echo $value; ?>">
          <input type="image" alt="Delete" name="Delete" value="Submit" src="/<?= $admin_dir ?>/images_oa/button_delete.gif">
        </p>
        </form>
        <?php
          end_box();
          }
      }

      /*
      * @function dumpRecords
      * @author jonah
      * @options master_id: The record id to force for the master record.
      *
      * Dump the sql records for a particular master record.
      */
      function dumpRecords($opts=false) {
          if(is_array($opts)) extract($opts);

          if($header) { ?><textarea cols="160" rows="200"><? }
          // dump the master record.
          foreach($this->tbl as $f) {

            $sets[] = " $f[field] = '".mes($this->$f[field])."'";
          }

          $out = " INSERT INTO $this->_table SET ".implode(",",$sets);

          echo $out;

          // dump dependent records
          echo "\r\r\r\r";
          $this->getRelated();

          $tables_used = array($this->_table);
          foreach($this->_related as $rel) {
            // get relations

            if($rel['table'] != $this->_table) {
              $sql = "SELECT * FROM ".$rel['table']." WHERE $this->indexField = $this->id ";
              $result = acquery($sql,$rel['db']);
              while($row=mysql_fetch_array($result)) {
                extract($row);
                $link = new DBOS($rel['table'],$row[0],$rel['db']);
                $link->dumpRecords();
                $tables_used[] = $rel['table'];
              }

            }

          }
          if($header) { ?></textarea><? }
      }

    /*
    * Get all related linked fields from this table
    * @version experimental Jonah 2009
    */
    function getRelated($opts=false) {
          global $zdb_schema;
          $sql = "SELECT r.* FROM pma_relation r

              WHERE foreign_db = '$this->_db' AND foreign_table = '$this->_table'
              AND foreign_field = '$this->indexField' ";
          // da($sql);
          $res = $zdb_schema->getAll($sql);
          if(count($res)) {
              foreach($res AS $row) {
                  extract($row);
                  $link['db'] = $row['master_db'];
                  $link['table'] = $row['master_table'];
                  $link['field'] = $row['master_field'];
                  $this->_related[] = $link;
              }
              return $this->_related;
          }
    }

    /**
    * Get the title field of the parent table so you can look up its name
    *
    * @version 1.0 JB  7/13/10 12:01 PM
    */
    function getParentTitleField() {
        global $zdb_schema;
        $warning = true;
        if(!$this->_parentTable) return false;
        if(!$this->_parentDB) $this->_parentDB = $this->_db;
        $sql = "SELECT display_field FROM pma_table_info
            WHERE db_name = '".$this->_parentDB."' AND table_name = '".$this->_parentTable."'
        ";
        $field = $zdb_schema->getOne($sql);

        // If the db lookup fails, try some other methods.  JB  7/14/10 11:16 AM
        if(!$field) {
            switch($this->_parentTable) {
                case"event":
                    $e = new Event();
                    $field = $e->_titleField;
                    break;
            }

        }
        if($warning && !$field) {
            mail("jonah@activistinfo.org","getParentTitleField failed to find name",$sql.print_r($this,true));
        }
        return $field;
    }

    /**
    * Get the name of the parent record
    *
    * @returns string of parent name
    * @version 1.0 JB  7/13/10 12:07 PM
    */
    function getParentName() {
        $debug = false;
        $warning = false;
        if(!$this->_parentTable) {
            if($warning) echo("No parent table found for this class in getParentName");
            return false;
        }
        // Defaults
        if(!$this->_parentDB) $this->_parentDB = $this->_db;
        if(!$this->_parentTitleField) $this->_parentTitleField = $this->getParentTitleField();
        if(!$this->_parentIdField) $this->_parentIdField = $this->_parentTable."_id";
        $record_id = $this->{$this->_parentIdField};

        // Required fields
        if(!$this->_parentTitleField) {
            return false;
        }
        if(!is_numeric($record_id)) {
            return false;
        }
        $sql = "SELECT ".$this->_parentTitleField." FROM ".$this->_parentTable."
            WHERE ".$this->_parentIdField." = ".$record_id." ";
        if($debug) echo($sql);
        mysql_select_db($this->_parentDB);
        $res = mysql_query($sql) or mysql_die($sql);
        $record = mysql_fetch_assoc($res);
        return $record[$this->_parentTitleField];
    }
}