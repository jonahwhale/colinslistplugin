<?php
/**
 * DataBase Object Styler
 *
 * Consilidate with GoOrgantic by JB on  7/19/09 12:15 PM
 *
 * @license LGPL
 * @version $Id: DBOS.class.php 5567 2009-02-28 22:41:36Z jonah $
 * @package ActiveCore
 * @subpackage DBOS
 * @author Jonah
 * @modified  4/9/08 5:21 PM by Jonah
 * 
 * @version 1.7 JB merged gp with opinvoice  11/4/11 1:46 PM
 * @version 1.6 Jonah  2/5/09 10:00 AM - Consolidated with open source version.
 * @link http://activecore-wiki.activistinfo.org/index.php/ActiveCore_Coding_Standards
 */

/**
 * Bring in useful DB stuff from Sam's abstract DO class
 */

require_once(dirname(__FILE__)."/DBObjectAC.class.php");
// require_once($_SERVER['DOCUMENT_ROOT']."/includes/classes/Cache/Cache.class.php");
// require_once($_SERVER['DOCUMENT_ROOT']."/includes/funcs.d/func.date.php");

/**
 *  DataBase Object Styler
 *
 *  Relationships for the data object are driven by data stored by phpmyadmin.
 *  You can define table relationships there, use
 *  the visualization tools, etc.  Then those relationships will be reflected in
 *  the forms and tables.
 *
 *  EX:
 *  class Example extends DBOS { function __construct("table",$id,"db","optional index field if not table_id"); }
 *  You can view pretty documentation at /core/includes/classes/DBOS/docs/index.html
 *  @example DBOS/docs/example.php
 *
 * A typical class extending this would look like:
 *
 * <code>
 * class Example extends DBOS {
 *    var $_db = "db";
 *    var $_table = "example";
 *    var $_idField = "example_id";
 *    var $_controller = "/admin/admin_examples.php";
 * }
 *
 * </code>
 * Interitance:
 * ACTable->DBObjectAC->DBOS
 *
 * @version 1.5 JB  10/6/08 2:03 PM
 * - show full tables, delete records, etc
 * - ajax grid-editing for tables
 *
 * @version 1.4g
 *  - better ajaxField implementation  6/19/08 10:16 AM
 *
 * @version 1.3
 *  - supports Enums, slightly better documentation
 * @version 1.2 GP
 *  - now extends DBObjectAC.  Experimental, I haven't tested using any inherited functions.  JB  2/26/08 9:46 AM
 *
 * @author Jonah  1/2/08
 * @package ActiveCore
 * @link http://activecore-wiki.activistinfo.org/
 */

class DBOS extends DBObjectAC {
    protected static $_tableInfo = array();
    // var $_idField = false;
    public $_idFieldEditable = false;
    /*
    * The following four fields are depreciated.  Always use _ for data object related fields
    * so they don't conflict with db set fields.  JB  8/18/08 6:25 PM
    */
    public $explain = array();
    /**
    * mysql info about the table and fields
    */
    public $tbl = array();
    public $result_content;
    /**
    * Load relationship data from pma_table_info table.
    */
    public $_loadDynamic = true;
    /**
    * Depreciated, use $_hiddenFields
    */
    public $hidden_fields = array();
    public $_passThrough = array();
    /**
    * The admin file that will deal with ajax and CRUD for this table/record.
    */
    public $_controller = "/admin/people/admin_person.php";

    /**
    * The id of the ajax span that is a unique identifier for the field you are about to ajaxField.
    */
    public $_ajaxID;

    /**
    *  array of related fields ie "key_id"=>"db.table.field:name".  Leave off db if in the same db as key table.
    */
    public $_fieldRelations = array();
    /**
    * Jonah B added  10/4/12 2:26 PM
    */
    public $_requiredFields = array();
    /**
    * The class of the label cell on the admin form
    */
    public $_formFieldClass = "fieldlabel";
    
    /**
    * If field name case is getting annoying.
    */
    public $_ignoreCase = true;
    
    /*
    * Load the data via the load function.
    *
    * @return returns the object.
    *
    * @version 1.1 - Jonah B added table conditional, if you don't have table, this won't work.   7/12/13 9:12 AM
    */

    function __construct($table,$id=false,$db=false,$indexField=false) {
        if(is_array($indexField)) {
          $indexField = $indexField[indexField];
        }
        if(is_numeric($table)) {
          // DataObjectAC compatibility
          parent::__construct($table);
        } else {
          $this->load($table,$id,$db,$indexField); 
        }
    }

    /**
     * OBSOLETE: Slated for removal.  JB  2/28/09 11:53 AM
     * Special case override function.  See $this->loadRecord.  JB  2/28/09 11:53 AM
     *    This is the heavy lifting.  Initialize the features of this data object
     *    with this "load" function.
     * This function is actually useful to tack on fields from 'extension' tables.  JB  3/16/09 7:16 PM
     * @modified  3/21/08 3:20 PM JB
     * @return True or false if the record was found
     * @param string $table database table
     *
     * @version 1.1  6/19/08 10:18 AM
     */

    function load($table,$id,$db,$indexField=false) {
        $this->db = $db;
        if(!$this->db) $this->db = PERSON_DB;
        if(!$indexField) $indexField = $this->_indexField;

        $this->table = $table;
        $this->id = $id;

        // set some DBObjectAC vars
        if(!$this->_db) $this->_db = $db;
        if(!$this->_table)   $this->_table = $table;


        if(!$this->tbl)   {
          $this->_loadTableInfo();
          $this->tbl = self::$_tableInfo[$this->table];
        }

        if(!$indexField) {
            // Just assume the first field is the index field.
            if(!is_array($this->tbl)) $this->loadTable();
            foreach($this->tbl as $field) {
                $this->_idField = $field['field'];
                break;
            }
            $this->indexField = $this->_idField;
        } else {
          $this->indexField = $indexField;
        }

        if(!$this->_idField) $this->_idField = $this->indexField;


        // Forms for editing are an array named after the table.
        $this->form_name = $this->_table;

        //  get the record from the database if the id is numeric.
        //  We only support numeric ids right now.
        if(is_numeric($id)) {
            $this->loadRecord($id);
            /*
            $sql = "SELECT * FROM $table WHERE $this->indexField = $id";
            $result=mysql_db_query($db,$sql) or mysql_die($sql); if($debug) { echo "<hr><pre> $sql </pre><hr>"; }
            $row=mysql_fetch_assoc($result);
            if($num=mysql_num_rows($result)) {
                foreach($row AS $field=>$value) {
                  $this->$field = $value;
                }
                $this->_loaded = true;
            }
            */
        } else {
          $this->defaults();
          return false;
        }
    } // end function load

   /**
    *  This function is supposed to output what we know about this
    *  record in a pretty way.  The output is ugly right now.
    *  The point of this function is to be used as the "summary" view
    *  of the record.
    *
    *  @vars
    *    $opts - hide_fields (only hides fields in the summary view, this funciton ignores overall hidden_fields)
    *
    *  @version SJP 08/31/2009 11:58 AM    
    */

    public function html($opts=false) {
        if($opts && is_array($opts)) extract($opts);
        if(!$hide_fields) $hide_fields = array();

        if(!$this->tbl) {
          $this->tbl = $this->_loadTableInfo();
        }

        if(is_array($this->tbl)) {
            if(!$hide_table) { ?><table><? }
            foreach($this->tbl AS $f=>$p) {
                // first do some default field name operations
                $rel = $this->getRelation($f);
                
                if(!in_array($f,$hide_fields)) {
                    if($f == $this->_idField) {
                        // don't show
                    } elseif($f=="hidden") {
                        // don't show
                    } elseif($f == "modified" || $f=="created_by" || $f=="created" || $f=="created_date") {
                        // don't show
                    } elseif($p['type'] == "datetime" || $p['type'] =="date") {
                        // show text only
                        $this->fieldHeader($f);
                        $name= $p['field'];
                        ?><?= $this->_getDate($this->$name) ?></td></tr><?
                    } else {
                        // then decide by type
                        //$this->formField($p);
                        $this->ajaxField($p,array("label"=>true));
                    }
                }
            }
             if(!$hide_table) { ?></table><? }
        } else {
            ?>Table info is not loaded correctly for:<?
            da($this);
        }

    }

   /**
    * Sets a single field.  Takes "field","value"
    *
    *  @returns Id of the record.
    */

    function set($field,$value=false) { 
        if($value == false) {
          $value = $_REQUEST[$field];
        }
        $value = stripslashes($value);
        if(!empty($this->db)) {
            mysql_select_db($this->db);
        }
        // Backwards compatibility
        if($this->table && !$this->_table) {
            $this->_table = $this->table;
        }

        if($this->indexField && !$this->_idField) {
            $this->_idField = $this->indexField;
        }

        if($this->contains("phone",$field)) {
          $info = cleanPhone($value);
          if(is_numeric($info[numeric])) {
            $value = $info[numeric];
          }
        }

        $query = "UPDATE `$this->_table`
                    SET `$field` = '" . addslashes($value) . "'
                    WHERE `$this->_idField` = '" . $this->mes($this->id) . "'";
        mysql_query($query) or mysql_die($query);
        $this->$field = $value;
        return $this->id;
    }
    
    /**
    * Shows links to tags found in field.
    *
    * @version 1.0 Jonah B  2/20/14 12:19 PM
    */
    public function linkTags($field=false) {
        if(!$field && $this->_tagField) {
            $field = $this->_tagField;
        } 
        
        if(!$field) return false;
        $content = strip_tags($this->{$field});
        $content = trim($content);
        if(!$content) return false;
        
        preg_match_all('/#([^\s]+)/', $content, $matches);

        $hashtags = $matches[1];

        if(is_array($hashtags)) {
            
            foreach($hashtags as $tag) {
                if(strpos($tag,';')) continue; // exclude html special characters.
                ?> <a class="fa fa-tag" title="Search for this tag" 
                    href="<?= $this->_controller ?>?<?= $field ?>=<?= urlencode("#".$tag); ?>"> 
                    <?= $tag ?>
                    </a>
                    <?php
            }
            
        }
    }
    
    /**
    * Returns array of ids that match this tag
    *
    * @version 1.0 Jonah B  2/21/14 7:03 PM
    */
    public function getByTag($tag) {
        if(!$tag) return false;
        if(!$this->_tagField) return false;
        if(is_array($this->_idField)) return false;
        $sql = "SELECT $this->_idField FROM $this->_table WHERE $this->_tagField LIKE '%".$this->mes($tag)."%' ";
        $are = $this->_dbo->fetchAll($sql);
    }

   /** Display functions */

   /**
    *  Stand-alone display of a single field.
    * @returns html string
    *  EX: $r->formField("field_name");
    *  Currently only supports enum, add in other types as you need them.  JB  3/20/08 11:23 AM
    *  Because this function is really just a switch for the various field type
    *  output functions, options aren't guaranteed to work universally.  Implement
    *  as needed.
    *  options:
    *    - raw - do not wrap in table html
    *    - bulk or datagrid: Display fields assuming there are multiple records
    *                        being displayed on the same page.
    *    - footer - add this html to the right of the field.
    *
    * @version 1.3 jonah  9/30/08 12:46 PM
    *  - fixed bug where options weren't being handed to text field types
    *  - added footer string to add to edit field display
    *
    * @version 1.2 jonah  9/30/08 12:40 PM
    *  - added datatime field support with popup calendar selection.
    *
    * @version 1.1  4/9/08 5:25 PM
    *  - now supports passing get vars in as defaults when creating new records.
    */

    function formFieldOutput($name,$opts=false) {
        // ob_start();
        $R =& $_REQUEST;
        $debug = false;
        if($opts=="explain") {
            show_errors("name,option - options are: raw, sql");
            die('formFieldOutput died after explain');
        }
        // $this->da($name);
        
        // if(!$this->tbl) $this->tbl = $this->_loadTableInfo();

        if($opts['bulk']) $this->dataGrid = true;
        
        if(is_array($name)) {
            
            $info =& $name;
        } else {
        
            if($this->_ignoreCase) {
                $nameLower = strtolower($name);
                if($nameLower!=$name) {
                    $nameCamelCaps = $name;
                            // Turn camel caps into spaced words
                    $label = preg_replace('/(?<=\\w)(?=[A-Z])/'," $1", $nameCamelCaps);
                    $label = trim($label);
                    if(!$opts['label']) {
                        $opts['label'] = $label;
                    }
                }
                $name = $nameLower;
            }
            
            // $info = $this->_loadFieldInfo($name);
            $info = $opts;
            $info['field'] = $name;
            if($label && !$info['label']) {
                $info['label'] = $label;
            }
            if($debug) echo "<br> Found full field info for".da($info)." in formField.";
        }
        
        if($R[$info['field']]) $this->$info['field'] = $R[$info['field']];
        
        if($this->_ignoreCase) {
             $info['field'] = strtolower($info['field']); // Camel caps support.  Jonah B  1/24/12 12:26 PM
        }
        
        if(is_array($info['rel'])) {
           // there is a table relationship from phpmyadmin data.
           if($debug) echo "relation found for $name, call this->formSelect";
           $this->formSelect($info,$opts);
        } else {
            switch($info['type']) {
                case"double":
                case"smallint":
                case"float":
                case"int":
                case"tinyint":
                   
                   if($info['size']==1) {
                     // Interpret this as a Yes/No field
                     $this->formBoolean($info,$opts);
                   } else {
                     $opts['type'] = 'number';
                     $this->formText($info['field'],$this->{$info['field']},$opts);
                   }
                   break;
                 case"text":
                   $this->formTextArea($info['field'], $this->{$info['field']},$opts);
                   break;
                 case"enum":
                   $this->formEnum($info,$opts);
                   break;
                 case"datetime":
                 case"date":
                   $this->formDate($info['field'], $this->{$info['field']},$opts);
                   break;
                 default:
                    if(is_array($info['rel']) || (is_array($this->_fieldRelations) && key_exists($info['field'],array_change_key_case($this->_fieldRelations)))) {
                       // there is a table relationship from phpmyadmin data.
                       if($debug) echo "relation found for $name, call this->formSelect";
                       $this->formSelect($info,$opts);
                   } else {
                       // we havn't found any other way to display, use default
                       // $this->da($this);
                       $this->formText($info['field'],$this->{$info['field']},$opts);
                   }
             }
         }
         
         if($autocomplete) { ?>
         <div class="auto_complete" id="<?= $this->_table ?>_<?= $info['field'] ?>_complete"></div>
            <script type="text/javascript">
                new Ajax.Autocompleter(
                    '<?= $this->_table ?>_<?= $info['field'] ?>',
                    '<?= $this->_table ?>_<?= $info['field'] ?>_complete',
                    '<?= $this->_controller ?>?action=get_autocomplete&table=<?= $this->_table ?>&field=<?= $info['field']?>', {
                        minChars: 2, 
                        paramName: '<?= $info['field'] ?>' , 
                });
            </script>
         <?php } 
         // $string = ob_get_clean();
         //echo $string;
         // return $string;
    }
    
    function formField($name,$opts=false) {
        // ob_start();
        
        $this->formFieldOutput($name,$opts);
        
        // return ob_get_clean();
    }
   /**
    *
    * @returns an array of the info we know about the field
    *
    * @version 1.1 JB  11/8/11 8:53 AM opinvoice fixed problem with case
    */

    function _loadFieldInfo($name) {
        $debug = false;
        if($this->_ignoreCase) $name = strtolower($name);
        if(!$name) {
            echo "_loadFieldInfo called with blank field name";
            return false;
        }
        // For backwards compatibility JB  8/14/08 2:16 PM
        if(!$this->_table) {
          if($this->table) $this->_table = $this->table;
        }

        if(!$this->_table) {
            show_errors("_loadFieldInfo could not load table info");
            return false;
        }

        if(!is_array($this->tbl[$name])) {
            $this->_loadTableInfo();
            $this->tbl = self::$_tableInfo[$this->_table];
            $tbl = $this->tbl; 
        } else {
            $tbl = $this->tbl;
        }
        
        if($this->_ignoreCase) {
            $tbl = array_change_key_case($tbl);
        }
        
        $fieldInfo =& $tbl[$name];
        if(!is_array($fieldInfo)) {
            echo "<br />DBOS Error: Failed to load field info for $name";
            da($tbl);
        }
        $fieldInfo =& $this->tbl[$name];
        return $fieldInfo;
    }


   /**
    *  Works in conjunction with saveAjax and scripaculous InPlaceSelect
    *   3/25/08 3:08 PM
    * http://wiki.script.aculo.us/scriptaculous/show/InPlaceSelect
    *
    * This tries to be flexible and determine the right way to deal with
    * the field type.  It does not support all field types.
    *
    * @returns nothing
    *
    * @version 0.2 Beta  8/12/08 12:38 PM - Jonah
    * @version 0.1 - alpha Early 08 - Jonah
    * @var options
    *
    */

    public function ajaxField($name,$opts=false) {
        $debug = false;
        // Figure out some of the stuff we need to know about this field to display it.
        if(is_array($name)) {
            // @todo - _fieldInfo instead of info, always be explicit about field vs table.
            $this->_info = $name;
        } else {
            if(!$this->tbl) {
              $this->tbl = $this->_loadTableInfo();
            }
            if($this->_lowercase) {
                $name = strtolower($name);
            }
            $this->_info = $this->tbl[$name];
        }

        if(!is_array($this->_info) && !$this->_gridEdit) {
            show_errors("The function ajaxField failed with field $name");
            da($this->tbl);
            return false;
        }

        if($debug) da($this->_info);

        $this->_opts = $opts;
        if($opts['table']) $this->fieldHeader($this->_info,$opts);

        switch($this->_info['type']) {
            case"enum":
                $this->ajaxFieldEnum($name,$opts);
                break;
            case"int":
            case"tinyint":
            case"smallint":
            case"mediumint":
               if($this->_info['size']==1) {
                 $this->ajaxBoolean($this->_info,$opts);
               } else {
                  if(is_array($this->_info[rel]) 
                    || (is_array($this->_fieldRelations) && array_key_exists($this->_info['field'],$this->_fieldRelations))) {
                    // This is a foreign key field, do the dropdown.
                    $this->ajaxFieldEnum($this->_info,$opts);
                 } else {
                    // "No relation found, do a simple text field.";
                    $this->ajaxText($this->_info,$opts);
                 }
               }
               break;
             case"time":
             case"date":
             case"datetime":
               // @todo, make these date fields use calendar. JB  8/19/08 12:02 PM
             case"decimal":
             case"char":
             case"varchar":
             case"text":
             case"double":
             case"float":
               $this->ajaxText($this->_info,$opts);
               break;
            default:
              $failed = true;
              if(!$this->_gridEdit) echo "Ajax field type {$this->_info['type']} not built.";

        }
        if($opts['table']) $this->fieldFooter($this->_info);
        if($failed) return false;
    }

    function _getDate($in) {
        return date('Y/m/d',strtotime($in)); 
    }

    function _getDateTime($in) {
        
        return date('Y/m/d H:i:s',strtotime($in)); 
    }
    
   /**
    * In place editing of varchar or text fields.
    *
    * @version 1.0 Jonah B  4/22/14 6:40 PM - Support array indexes
    * @version 0.2 Beta JB  8/12/08 1:12 PM
    * @version 0.1 Alpha 5/30/08 12:02 PM
    */

    function ajaxText($info=false,$opts=false) {
        $debug = false;
        if(is_array($opts)) extract($opts);
        if($this->_lowercase) {
            $info['field'] = strtolower($info['field']);
        }
        if($this->contains("phone",$info['field'])) {
           $this->$info['field'] = show_phone($this->$info['field'],array("string"=>true));
        }

        if($info['type']=="text") $textarea = true;
        if($info['type']=="datetime" && $date_only) {
            $this->$info['field'] = $this->_getDate($this->$info['field']);
        } elseif($info['type']=="datetime") {
            $this->$info['field'] = $this->_getDateTime($this->$info['field']);
        } elseif($info['type']=="date") {
            $this->$info['field'] = $this->_getDate($this->$info['field']);
        }


        if($textarea) $config = "rows:20,cols:60,";
        if($this->$info['field']!="") {
          $current_value = $this->$info['field'];
        } else {
          if(is_numeric($this->id)) {
            $current_value = "Edit";
          } else {
            $current_value = "Add";
          }
          $current_value .= " {$info['field']}";

        }

        // Backward compatibility JB  10/7/08 4:19 PM
        if($this->indexField && !$this->_idField) {
            $this->_idField = $this->indexField;
        }

        /*
        * Attempt to locate idField from array -- SJP 06/16/2010 07:58PM
        */
        if($this->_idField && is_array($this->_idField)) {
            if(in_array("person_id",$this->_idField)) {
                $this->_idField = $this->_idField[1];
            } else {
                $this->_idField = $this->_idField[0];
            }
        }

        if(!$index && !is_numeric($this->id) && is_numeric($this->person_id)) {
            $index = $this->person_id;
        } elseif(!$index) {
            $index = $this->id;
        }
        // Array support
        if(is_array($index)) {
            $index = implode('_',$index);
        }
        if($label) $this->fieldHeader($info);
        
        /**
        * Id can be a string of a single field, or an array of many fields.
        */
        if(is_string($this->_idField)) {
            $id_uri =  $this->_idField.'='.$this->id;
        } elseif(is_array($this->_idField)) {
            foreach($this->_idField AS $field) {
                $id_uri .= '&'.$field.'='.$this->{$field};
            }
            
        }
        // Display only 150 characters -- SJP 02/27/2009 01:12 PM
        if($textarea) {
            $value_length = strlen($current_value);
            if($value_length > 150) {
                $short_value = substr($current_value, 0, 150) . "...";
            }
            ?><span id="<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?>_long<?= $token ?>" style="display:none"><?= stripslashes($current_value) ?></span><?
        }
        ?><span class="Editable AjaxEdit TextArea" id="<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>"><? if($short_value) { ?><?= stripslashes($short_value) ?><? } else { ?><?= stripslashes($current_value) ?><? } ?></span>
        <script type="text/javascript">
            new Ajax.InPlaceEditor(
                '<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>',
                '<?= $this->_controller ?>?table=<?= $this->_table ?>&field=<?= $info['field'] ?>&action=ajaxField&<?= $id_uri ?><? if($this->person_id) { echo "&person_id=$this->person_id"; }?>', {
                    <?= $config ?>
                    clickToEditText:"Click to change value."
                    <? if($textarea) { ?>
                        ,onEnterEditMode: getEnterEditMode_<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>
                        ,onComplete: getLeaveEditMode_<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>
                    <? } ?>
            });
            <? if($textarea) { ?>
                function getEnterEditMode_<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>(form_edit, value_edit) {
                    var long_value = $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?>_long<?= $token ?>').innerHTML;
                    $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>').update(long_value);
                    $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>').innerHTML;
                }
                function getLeaveEditMode_<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>(form_edit, value_edit) {
                    var long_value = $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>').innerHTML;
                    long_value = long_value.replace(/\\'/g,'\'');
                    long_value = long_value.replace(/\\"/g,'"');
                    long_value = long_value.replace(/\\\\/g,'\\');
                    long_value = long_value.replace(/\\0/g,'\0');
                    var value_length = long_value.length;
                    var display_value = long_value;
                    if(value_length > 150) {
                        display_value = display_value.substr(0,150) + '...';
                    }
                    $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>').update(display_value);
                    $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?><?= $token ?>').innerHTML;
                    $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?>_long<?= $token ?>').update(long_value);
                    $('<?= $this->_table ?>_<?= $index ?>_<?= $info['field'] ?>_long<?= $token ?>').innerHTML;
                }
            <? } ?>

        </script><?
        if($this->_tagSearch) {
            
            $this->_getTags($current_value);
        }
        if($label) $this->fieldFooter($info);
    }
    
    /**
    * For now this will just show the tags, but eventually we can split out get and show functions.
    *
    * @version 1.0 Jonah B  8/12/13 11:22 AM
    */
    public function _getTags($in) {
        
        if (preg_match_all('/(^|\s)#(\w*[a-zA-Z_]+\w*)/', $in, $arrHashtags) > 0) {
            ?><span id="tagContainer"><span id="tagHeader">Tags:</span><?php
          foreach ($arrHashtags[2] as $strHashtag) {
            ?><a class="badge" id="hash<?= $strHashtag ?>" href="<?= $this->_tagSearch ?><?= urlencode($strHashtag); ?>"><?= $strHashtag; ?></a> <?php
            // Check each tag to see if there are letters or an underscore in there somewhere
          }
          ?></span><?php
        }        
    }
    
   /**
    *  Wrap field display in a table row/cell.
    *
    * @options
    *  - raw: No display wrapping at all.
    *  - span: Use span instead of table.
    */
    
    function fieldHeader($info,$opts=false) {
        $debug = false;
        if($debug) echo "<br />Debug: fieldHeader called with $info <br />";
        if(is_array($opts)) extract($opts);

        if($div) { $span_type = "div"; }
        if($span) { $span_type = "span"; }

        if(is_string($info)) {
            $field = $info;
            if(is_array($this->tbl[$info])) {
                $info = $this->tbl[$info];
            }
            if(!is_array($info)) {
                
                $info = $this->_loadFieldInfo($info);
                
            }
        }
        $info['field'] = $field;
        if($debug) {
            $this->da($info);
            echo"Called fieldHeader function<br /><br /><br />";
        }
        if(!$label) {
            
            $label = $this->camelcaps($info['field']);
            $label = str_replace(" Id","",$label);
        }
        if(!$raw)
        {
            if($span_type)
            {
                echo "<$span_type ";
            } else
            {
                echo "<tr id='DBOSfieldHeader' class='".$this->_table."'> <td align=\"right\"";
              }
             ?> class="field_label" > <?php
                      if($info['comment']) {
                        $display_comment = $info['comment'];
                      } elseif($comment) {
                        $display_comment = $comment;
                      }
                      if($display_comment) {
                         echo acTooltip("_".$info['field']."_comments",$display_comment);
                      }
                    ?><label class="dbosFieldHeaderLabel control-label"><?= $label ?></label>
                <? if($span_type) { ?>
                    </<?= $span_type ?>><<?= $span_type ?>>
                <? } else { ?>
                    </td>
                    <td><?
                }

        }

        return $info;
    }

    /**
    * HTML field footer
    * @retuns nothing
    */

    function fieldFooter(&$info,$opts=false) {
        if(is_array($opts)) extract($opts);
        if($div) $span_type = "div";
        if($span) $span_type = "span";
        if($footer) echo $footer;
        if(!$raw) {
            if($span_type) {
              ?></<?= $span_type ?>><?
            } else {
              ?></td></tr><?
            }
        }
    }

    /**
    * Ajax select for a yes/no field type
    *
    * @version 1.2 JB  6/6/12 11:58 AM - Configure more and support _postURL
    * @version 1.1 SJP 08/31/2009 12:57 PM
    */
    function ajaxBoolean($info,$opts=false) {
        if(is_array($opts)) extract($opts);
        // http://wiki.script.aculo.us/scriptaculous/show/InPlaceSelect
        if(!is_array($info)) {
          $info =& $this->tbl[$info];
        }
        $name = $info['field'];

        if(!$select) $select = array("Yes","No");
        $keys = "1,0";
        $labels = "'".implode("','",$select)."'";

        if($current=$this->$name) {
          if($current==1) {
            $current = "Yes";
            $badge = "badge success";
          } else {
            $current = "No";
          }
        } else {
          $current = "No";
        }

        if($label) $this->fieldHeader($info,$opts);
        if(is_array($this->id)) {
            $uniqueId = implode("_",$this->id);
        } else {
            $uniqueId = $this->id;
        }
        ?>
        <span id="<?= $this->_table ?>_<?= $name ?>_<?= $uniqueId ?>" class="currentValue Editable AjaxEdit"><?= $current ?></span>
        <script type="text/javascript">

         new Ajax.InPlaceSelect('<?= $this->_table ?>_<?= $name ?>_<?= $uniqueId ?>', '<?= $this->_controller ?>?action=ajaxField&field=<?= $info['field'] ?>&type=binary&table=<?= $this->_table ?>&<?= $this->_postURL ?><?
         if(is_array($this->_idField)) {
            foreach($this->_idField as $field) {
                ?>&<?= $field ?>=<?= $this->$field ?><?
            }
         } else {
            ?>&<?= $this->_idField ?>=<?= $this->id ?><?
         }
         ?>', [<?= $keys ?>], [<?= $labels ?>],
          { paramName: 'value', parameters: "field=<?= $name ?>" } );
        </script>
        <?
        if($label) $this->fieldFooter($info,$opts);
    }

   /***
    *  Works in conjunction with saveAjax and scripaculous InPlaceSelect
    *
    * Currently in production with the Call Campaign grid and people records,
    * Along with events for hotseat easy editing.
    *
    * Arguments:
    * - name : either the name of the field or array of data describing the field
    * - opts:
    *   - keys/labels : arrays of keys and labels to display in the dropdown
    *
    * - opts:
    *   - select_options : associative array (keys,labels) (to manually supply the select menu)
    *
    *
    * @version 1.2 - Fixed ajadID conditional that broke the survey group form. - JB  8/2/11 8:15 AM
    * @version 1.1 - RJP 04/14/2011
    * @version 1.0 - Jonah - 7/13/08 2:46 PM
    *   - Allow multiple event fields per page.
    */
    function ajaxFieldEnum($name,$opts=false) {
        // http://wiki.script.aculo.us/scriptaculous/show/InPlaceSelect
        global $zdb_schema;
        $debug = false;
        if(is_array($opts)) extract($opts);
        if(is_array($name)) {
            $info =& $name;
        } else {    
            if($this->_lowercase) {
                $name = strtolower($name);
            }
            $info =& $this->tbl[$name];
        }
        
        if($debug) da($info);
        // Backwards compaitibility
        if($this->indexField && !$this->_idField) {
          $this->_idField = $this->indexField;
        }
        if($label) $this->fieldHeader($info,$opts);

        if(!$this->ajaxID) {
            // Unique identifier for this div
            $this->ajaxID = $info['field']."_".$this->id;

            // If you are showing multiple people with multiple fields on the same page.
            if($this->person_id) $this->ajaxID = $this->ajaxID."_pid_".$this->person_id;
        }


        if(!$this->_postURL) {
            if($this->person_id && $this->_idField!="person_id") {
                $this->_postURL = "&person_id=".$this->person_id;
            }
        }
        
        if(is_array($this->_fieldRelations) && $this->_fieldRelations[$info['field']]) {
            $rel = $this->_parseRelation($info['field']);
            /// da($rel);
            $info['rel']['foreign_table'] = $rel['table'];
        }
        if(!is_array($keys)) {
            switch($info['type']) {
                case"enum":
                    $select = $this->formEnum($info,array("get_array"=>true));
                    $keys = implode(",",array_keys($select));
                    $labels = "'".implode("','",$select)."'";
                    $keys = $labels;
                    break;
                default:
                    // Non-enums assume to be related key tables.
                    // make $select array.
                    $className = str_replace("_","",$info['rel']['foreign_table']);
                    
                    if(class_exists($className)) {
                        $foreign = new $className($this->$info['field']);
                        $titleField = $foreign->_titleField;
                        // getRecordTitle
                        if($this->contains('CONCAT',$titleField)) {
                            $titleField = str_replace("CONCAT","",$titleField);
                            $titleField = str_replace("(","",$titleField);
                            $titleField = str_replace(")","",$titleField);
                            $pts = explode(",",$titleField);
                            foreach($pts AS $pt) {
                                if($foreign->$pt) {
                                    $current .= " ".$foreign->$pt;
                                }
                            }
                        } else {
                            $current = $foreign->$titleField;
                        }
                        if(!$info['rel']['foreign_db']) $info['rel']['foreign_db'] = $foreign->_db;
                    } else {
                        // see if phpmyadmin knows the title fields.
                        $sql = "SELECT display_field FROM pma_table_info
                            WHERE db_name = '{$rel['foreign_db']}' AND table_name = '{$info['rel']['foreign_table']}' ";
                        $foreign->_titleField = $zdb_schema->fetchOne($sql);
                        // @todo - Make this smarter.  JB  12/4/08 4:21 PM
                        $foreign->_idField = $info['rel']['foreign_table']."_id";
                    }
                    if(!$foreign->_titleField) $foreign->_titleField = $foreign->_idField;
                    if(is_array($select_options)){ // $opts[select_options]
                        $select=$select_options;
                        if($current) $current=$select_options[$current];
                        // stab in the dark?
                        if(!isset($info['field']) && !is_array($name)){
                            $info['field']=$name;
                            $this->ajaxID=$info['field'].$this->ajaxID;
                        }
                    } else {
                        $sql = "
                            SELECT $foreign->_idField AS id, $foreign->_titleField AS title 
                            FROM {$info['rel']['foreign_table']} ";
                        if($foreign->_limitField && $this->{$foreign->_limitField}) {
                            $sql .= " WHERE $foreign->_limitField = ".$this->{$foreign->_limitField}." ";
                        }
                        $sql .= " ORDER BY $foreign->_titleField ";
                        // $sql .= " ORDER BY title ";
                        
                        $res = acqueryz($sql,$info['rel']['foreign_db']);
                        
                        $options = $res->fetchAll();
                        // Jonah B added count if.   10/25/12 4:35 PM
                        if(count($options)) {
                            foreach($options AS $op) {
                                $select[$op['id']] = $op['title'];
                            }
                        } else {
                            /*
                            show_warning("Could not find foreign keys to choose from.  DBOS line 897");
                            da($sql);
                            */
                        }
                    }
                    // Jonah B  10/30/12 11:58 AM - Added if is_array check.
                    if(is_array($select)) {
                        $keys = implode(",",array_keys($select));
                        $labels = "'".implode("','",$select)."'";
                    }
                    break;
            }
        }
        if(!$current) {
            if($current=$this->$info['field']) { /* Should this be if($current==$this->info['field']) ? RJP */
            } else {
                $current = "none";
            }
        }

        ?>
        <span id="<?= $this->ajaxID ?>" class="Editable AjaxEdit Enum"><?= $current ?></span>
        <script type="text/javascript">

         new Ajax.InPlaceSelect('<?= $this->ajaxID ?>', '<?= $this->_controller ?>?&field=<?= $info['field'] ?>&action=ajaxField&table=<?= $this->_table ?>&<?= $this->_idField ?>=<?= $this->id ?>&<?= $this->_postURL ?>', ['0',<?= $keys ?>], ['none',<?= $labels ?>],
          { paramName: 'value', parameters: "field=<?= $info['field'] ?>" } );
        </script>
        <?php
        unset($this->ajaxID);
    }
    
    /**
    * @version 1.1 Jonah B  5/5/15 9:48 AM
    */
    function showCheckbox($name, $selected="0",$opts=false) {
        
        if(!is_numeric($selected)) {
            $value = $selected;
            unset($selected);
            if($_REQUEST[$name]) $selected = true;
        } else {
            $value = 1;
        }
        if(!$value) $value = 1;
        if($opts['label']) {
            ?><label title="<?= htmlentities($description); ?>" for="<?= $name ?>"><?= $opts['label'] ?></label><?php
        }
        ?>
        <input 
            class="form-control checkbox" 
            name="<?= $name ?>" id="<?= $name ?>" 
            type="checkbox" 
            value="<?= htmlentities($value) ?>" <?php if($selected) echo "checked"; ?> />
            <?php
    } // end function checkbox

   /***
    * @returns string of the onchange function to be called when changed.
    *
    */
    function fieldOnChange() {
        $onchange = "onchange=\"ACUpdateRecord(".$this->table.",<?= $p->person_id ?>);\"";
    }

   /**
    *
    * $info is from the get table info function.
    * opts:
    *  raw - no table wrapper
    *  choose - show the "choose..." option from the pulldown, off by default
    *  get_array - no output, just return array of enum choices
    *
    * @version 1.0 JB  3/20/08 6:02 PM
    */
    function formEnum(&$info,$opts=false) {
        global $R;
        if(is_array($opts)) extract($opts);
        if(!$this->$info['field'] && $default) $this->$info['field'] = $default;
        // figure out the array
        $select = str_replace("'","",$info[size]);
        // turn string into an array
        $select = explode(",",$select);
        if($get_array) return $select;
        if(!$raw) { ?>
            <tr id="formEnum" class="formRow">
            <td align="right" valign="top" class="fieldlabel" >
              <p><label><nobr><?= camelcaps($info['field']); ?>:</nobr></label></p>
            </td><td><?
        }
        // Now do the menu itself.


        if($choose) {
            //  The following seems very retarded.  Probably a better way.  JB  3/25/08 11:20 AM
            // $select = array_merge(array(""=>"Choose..."),$select);
            // $select = array_unshift($select,"Choose...");
            if($choose===true) $choose = "Choose...";
            $selectchoose = array(""=>$choose) + $select;
            unset($select);
            foreach($selectchoose as $option) {
                if($this->contains("choose",strtolower($option)) OR $choose==$option) {
                    $select[''] = $option;
                } else {
                    $select[$option] = $option;
                }
            }
        }

        $fname = $this->getFname($info['field']);
        if($this->debug || isset($R['debug'])) da("Default is :".$this->$info['field']. " for field {$info['field']} with id {$this->id}.");
        if(!$read_only) {
            array_menu($select,$fname,$this->$info['field']);
        } else {
            echo $this->$info['field'];
        }
        if(!$raw) { ?></td></tr><? }
    }

    /**
     * A checkbox type field, turned on automatically for tinyint.
     *
     * @version  4/29/08 3:05 PM by Jonah
     */
    function formBoolean(&$info,$opts=false) {
        global $R;
        $fname = $this->getFname($info['field']);
        if(is_array($opts)) extract($opts);

        if(!$default) {
            if($R[$info['field']]) {
              $default = $R[$info['field']];
            } elseif($this->$info['field']) {
              $default = $this->$info['field'];
            }
        }
        // For a new record attempt to set the default to the mysql field default. JB  7/14/09 3:47 PM
        if(!$default && !is_numeric($this->{$this->_idField})) {
            $default = $info['default'];
        }

        $this->fieldHeader($info,$opts);

        $this->showCheckbox($fname,$default);
        if($this->dataGrid) {
          $this->person_id ? $id_value = $this->person_id : $id_value = $this->id;
          // For checkboxes, we need to know if they were shown in order to know if they were unchecked.  JB  5/30/08 12:04 PM
          ?><input type="hidden" name="records_shown[<?= $info['field'] ?>][]" value="<?= strip_tags($id_value) ?>" /><?
        } else {
          ?><input type="hidden" name="checkbox_shown[]" value="<?= strip_tags($info['field']) ?>" /><?
        }

        $this->fieldFooter($info,$opts);

    }

   /**
    *  Get the name of the array that this edit form is going to
    *  build with the variables being submitted. 
    */
    function getFname($name,$opts=false) {
        if($opts && is_array($opts)) extract($opts);
        if($this->form_name)  {
            if($return_id) {
                $fname = $this->form_name."_".$name;
            } else {
                $fname = $this->form_name."[".$name."]";
            }
        } elseif($this->_table) {
            $this->form_name = $this->_table;
            if($return_id) {
                $fname = $this->_table."_".$name;
            } else {
                $fname = $this->_table."[".$name."]";
            }
        } else {
            $fname = $name;
        }
        // assumes person id.  Might be a better way.
        if($this->dataGrid) {
            if($return_id) {
                $fname .="_$this->person_id";
            } else {
                $fname .="[$this->person_id]";
            }
        }
        return $fname;
    }
    
   /**
    *  Get the name of the array that this edit form is going to
    *  build with the variables being submitted.
    */
    function getIname($name) {
        $iname = $this->getFname($name,array("return_id"=>true));
        return $iname;
    }

    /**
    *  Supports 'raw' and 'frozen' opts.
    *
    * - Added html 5 number types  1/21/14 10:14 AM Jonah B
    */

    function formText($name, $default=false, $opts=false) {
        $debug = false;
        if($debug) echo "formText called with $name";
        if($opts && is_array($opts)) extract($opts);
        // Find the default default value.
        if(is_numeric($this->id) && !$default) $default = $this->$name;

        $fname = $this->getFname($name);
        $iname = $this->getIname($name);

        if($opts['size']) {
            $size = $opts['size'];
        } elseif($strl=strlen($default)) {
            $size = $strl+2;
        }
        if($size>74) $size = 74;
        $info = $this->fieldHeader($name,$opts);
        
        if(!$opts['frozen'])
        { ?>
            <input type="text" id="<?= strip_tags($iname) ?>" name="<?= $fname ?>" value="<?= $default ?>" maxlength="255" size="<?= $size ?>" >
            <? if(in_array($name,array_keys($this->explain))) { ?><span class="caption"><?= $this->explain[$name] ?></span><? } ?>
          <?
        } else { ?>
            <?= stripslashes($default) ?><?
        }
        $this->fieldFooter($info,$opts);
    }

    /**
     * depends on function datetime_field for fancy css/js calendar.
     *
     * @version 1.1 SJP 12/11/2010 11:28PM Format date output
     */
    function formDate($name, $default=false,$opts=false) {
       if(is_numeric($this->id) && !$default) $default = $this->$name;
       $fname = $this->getFname($name);

        // Causing conflicts with displaying time selector in js calendar -- SJP 03/23/2009 09:57 AM
        //if($default=="0000-00-00 00:00:00" || $default=="0000-00-00") {
        //  unset($default);
        //}

        if(is_array($opts)) extract($opts);

        // Do the output
        $info = $this->fieldHeader($name,$opts);
        if(!$opts['frozen']) {
            $this->showDateTimeField($fname,$default);
        } else {
            echo $this->showDateTime($default);
        }
        $this->fieldFooter($info,$opts);
    }
    
    public function getDateTime($in) {
        return date('m/d/Y H:i:s',strtotime($in));
    }
    
    public function getDate($in) {
        return date('m/d/Y',strtotime($in));
    }
    
    /**
     * 
     * @var opts
     *   - date_only (show only the date)
     * @version 1.1 SJP 02/13/2009 05:43 PM
     *   - Added time selector in the js calendar
     *   - Added date_only opts
     */                         
    public function showDateTimeField($name="date",$default=false,$opts=false) {
         global $call_datetime_field,$count_datetime_field;
         $count_datetime_field++;
         if(is_array($opts)) extract($opts);
         $parts = explode(" ",$default);
         if($date_only) {
             $showsTime = false;
         } elseif(!$showsTime && count($parts) > 1) { // is a date-time field
             $showsTime = true;
         } elseif(!$showsTime) {
             $showsTime = false;
         }
         if($default && !$this->contains("0000",$default)) {
             if($showsTime) {
                 $default = $this->getDateTime($default,array("format" => "calendar"));
             } else {
                 $default = $this->getDate($default);
             }
         } else {
             unset($default);
         }
         if($showsTime) {
             if(!$size) $size = 20;
         } else {
             if(!$size) $size = 10;    
         }
         if(!$call_datetime_field) {
           ?>    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                 <link rel="stylesheet" href="/resources/demos/style.css">
                 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
     
         <?php
           $call_datetime_field = true;
         }
         ?>
        <script>
       $( function() {
         $( ".datepicker" ).datepicker();
       } );
       </script>
       
         <input 
             type="text" 
             name="<?= $name ?>"
             class="datepicker"
             id="<?= $name ?>" 
             value="<?= $default ?>" 
             <?php if ($size){ ?>size="<?= $size ?>"<? } ?> 
         />
        <?php
    }    
    
    /**
    * 
    */
    public function formState($name, $default=false,$opts=false) {
        if(is_numeric($this->id) && !$default) $default = $this->$name;
        $fname = $this->getFname($name);
        ?>
        <tr id="DBOSformState">
            <td align="right" valign="top" class="fieldlabel">
              <label><p><?= camelcaps($name); ?>:</p></label>
            </td>
            <td align="left" valign="top">
              <? state_menu($default, $fname); ?>
            </td>
        </tr>    
        <?
    }

    /*
    * Create a dropdown menu from the field
    *
    * $p is the array of info we know about a field from the table info function.
    *  TODO: support for "raw" option to hide table html
    *  TODO: support for "string" option to return string instead of output
    * @requires pulldown
    *
    * @version 1.1 Jonah B  10/9/12 8:51 PM - Added _relatedFields support
    * @version 1.0 Jonah  10/18/08 5:45 PM
    */

    public function formSelect($p,$opts=false) {
        if(is_array($this->_fieldRelations)) {
            $this->_fieldRelations = array_change_key_case($this->_fieldRelations);
        }
        
        if(is_array($this->_fieldRelations)) {
            if(array_key_exists($p['field'],$this->_fieldRelations)) {
                $relation = $this->_fieldRelations[$p['field']];
                $pts = explode('.',$relation);
                if(count($pts)==2) {
                    $p[rel]['foreign_table'] = $pts[0];
                    if($this->contains(':',$pts[1])) {
                        list($field,$select_field) = explode(':',$pts[1]);
                    } else {
                        $field = $pts[1];
                    }
                    $p[rel]['foreign_field'] = $field;
                }
                
            }
        }
        
        if(is_array($this->_requiredFields) && in_array(strtolower($p['field']),array_change_key_case($this->_requiredFields))) {
            $opts['required'] = true;
        }
        
        if($p[rel]['foreign_table']) {
            $className = $p[rel]['foreign_table']."Table";
            $className = str_replace("_","",$className);
            if(class_exists($className)) {
                $foreignTableObject = new $className;
                if($foreignTableObject->_limitField) {
                    $opts['where'] .= " AND $foreignTableObject->_limitField = '".mysql_escape_string($this->{$foreignTableObject->_limitField})."' ";
                }                
            }
        }
        $fname = $this->getFname($p['field']);
        $this->debug = false;
        if(is_array($opts)) {
            extract($opts);
        } else { unset($opts); }
        if($this->debug) da($opts);

        if(!$opts['required']) {
            if(!$choose) $choose = "Choose...";
        } elseif(!$choose) {
            $choose = false;
        }
        $this->fieldHeader($p,$opts);
        if(!$p['rel']['foreign_db']) { // Fix on the fly.  JB  1/24/12 1:30 PM
            $this->tbl[$fname]['rel']['foreign_db'] = $this->_db;
            $p['rel']['foreign_db'] = $this->_db;
        }
        

        DBOS::pulldown(
                   $p[rel]['foreign_table'],
                   array(
                        "id_field"=>$p['rel']['foreign_field'],
                        "choose"=>$choose,
                        "fname"=>$fname,
                        "default"=>$this->$p['field'],
                        "db"=>$p['rel']['foreign_db'],
                        "where"=>$opts['where'],
                        "frozen"=>$opts['frozen'],
                        "name_field"=>$select_field
                   ),
                   $opts
                 );
        
         $this->fieldFooter($p,$opts);
    }

    /**
     * Show a label for a field assuming a table structure.
     */
    public function showLabel($label) {
        ?>
        <td align="right" valign="top" class="fieldlabel">
            <p><?= camelcaps($label); ?>:</p>
        </td>
        <?php
    }

    /**
     * Show a text area, expects the name of the field, the default value, and any options.
     * possible options: size
     */
    public function formTextArea($name, $default=false, $opts=false) {
        $cols = "80";
        switch($opts[size]) {
          case"large":
            $cols = "80";
            $rows = "30";
            break;
          case"mediumsmall":
            $cols = "80";
            $rows = "12";
            break;
          case"medium":
          case"small":
            $rows = "8";
            $cols = "50";
            break;
          case"mini":
            $rows = "5";
            $cols = "40";
            break;
          default:
            $rows = "15";
            break;
        }
        $fname = $this->getFname($name);
        
        $this->fieldHeader($name,$opts);
        
        if(!$opts['frozen']) {
        ?>
            <textarea 
                name="<?= $fname ?>" 
                cols="<?= $cols ?>" 
                rows="<?= $rows ?>" 
                wrap="VIRTUAL"><?= stripslashes($this->$name) ?></textarea>
        <?php
        } else {
            echo stripslashes($this->$name);
        }
        $this->fieldFooter($name,$opts);
    }
    
    /**
     * Show a checkbox
     *
     * $name = name of field
     * $d = default value
     * $opts = none defined
     */
     
    public function formCheckbox($name, $d, $opts=false) {
        if(is_array($opts)) extract($opts);
        $fname = $this->getFname($name);
        if($d) $checked = "checked";
        ?>
        <tr>
          <td align="right" valign="top" class="fieldlabel">
            <p><?= camelcaps($name); ?>:</p>
          </td>
          <td align="left" valign="top">
            <input type="checkbox" class="formCheckbox form-control" id="<?= $fname ?>" name="<?= $fname ?>" value="1" <?= $checked ?> />
          </td>
        </tr><?
    }

    public function formHidden($name, $default=false, $opts=false) {
       $fname = $this->getFname($name);
       ?><input type="hidden" name="<?= $fname ?>" value="<?= $default ?>" /><?
    }

    public function defaults() {
        return false;
    }

    /*
    * Get the form name.  Poorly named function.
    * I think this is a dup function. JB  8/26/08 5:55 PM
    */

    function fName($field_name) {
        if($this->form_name) {
          $fname = $this->form_name."[".$field_name."]";
        } elseif($this->_table) {
          $this->form_name = $this->_table;
          $fname = $this->form_name."[".$field_name."]";
        } else {
          $fname = $field_name;
        }
        return $fname;
    }

    // This is a general function for deleting records that will
    // ask the user if she is sure before doing anything.

    function deleteVerify($name=false) {
      global $sure, $id, $h_section, $delete;
      global $theme, $language, $type;

      $title = $this->nameField;
      if (empty($sure)) {
        start_box();
        ?>
        <br>
        <form method="get" action="<?php echo $PHP_SELF; ?>" name="surething">
        <p align="center">Are you sure you want to delete the record for <b><?= $this->$title ?></b>?</p>
        <p align="center">
          <input type="radio" name="sure" value="1" checked>
          Yes
          <input type="radio" name="sure" value="0">
          No</p>
        <p align="center">
        <input type=hidden name='action' value='delete'>
         <input type=hidden name='id' value='<?= $this->id ?>'>
        <? if(isset($type)) {
          ?><input type="hidden" name="type" value="<?php echo $type; ?>"><?php
        } ?>
        <input type="hidden" name="<?= $name ?>" value="<?php echo $value; ?>">
        <input type="image" alt="Delete" name="Delete" value="Submit" src="/admin_live/images_oa/button_delete.gif">
      </p>
      </form>
      <?php
        end_box();
        }
    }


    /**
    * Generate html form to edit the object.
    *
    * @var opts
    *  - hide form removes opening and closing form so you can add fields and do yourself.
    *
    * @version 1.2 JB  11/7/11 2:06 PM - Added "bulk" config to hide action if you want multiple forms on a page.
    * @version 1.1   4/9/08 5:38 PM by Jonah
    * @version 1.0 JB  7/26/11 1:52 PM
    */

    function edit($opts=false) {
        $debug = false;
        $R =& $_REQUEST;
        if(is_array($opts)) extract($opts);
        if($opts['frozen'] || $opts['bulk']) $hide_form = true;

        if(!$this->tbl) {
            $this->tbl = $this->_loadTableInfo();
            // $this->tbl = self::$_tableInfo[$this->table];
          
        }
        if($debug) $this->dump($this->tbl); 
        
        if(!$hide_form) { ?>  <form action="<?= $this->_controller ?>" method="GET" id="acEditForm<?= $this->_table ?>" name="form"> <? }
        ?>
        <table id="editTable">
        <?
            if(is_array($this->tbl)) {
                foreach($this->tbl AS $f=>$p) {
                  // first do some default field name operations
                  if($debug) $this->dump($f);
                  if(!in_array($f,$this->hidden_fields)) {
                      if($f==$this->_idField && $this->_idFieldEditable==false) {
                        $this->$f ? $ivalue = $this->$f : $ivalue = "new";
                        ?><input type="hidden" name="<?= strip_tags($f) ?>" value="<?= $ivalue ?>" /> <?
                      } elseif($f=="hidden") {
                        $this->formCheckbox($f, $this->$f);
                      } elseif($f == "modified" || $f=="created_by" || $f=="created" || $f=="created_date") {
                        // don't show
                      } else {
                        // then decide by type
                          $this->formField($f,$p);
                      }
                  }
                }
            } else {
                
              ?>Table info is not loaded correctly for edit:<?
              $this->dump($this);
            }
            
            ?>
          </table>
          <? if(is_string($footer)) { echo $footer; } ?>
          <?php if(!$bulk) { ?>
          <input type="hidden" name="action" value="db_edit_<?= $this->_table ?>" />
          <?php } ?>
          <input type="hidden" name="table" value="<?= $this->_table ?>" />
          <?php if(is_array($this->_passThrough)) {
            foreach($this->_passThrough AS $pass_field) {
                ?><input type="hidden" name="<?= $pass_field ?>" value="<?= $_REQUEST[$pass_field] ?>" /> <?
            }
          } ?>
         <? if(!$hide_form) { ?>
                <input type='hidden' name='page' value='<?= $this->_page ?>' />
          Copy: <input type="checkbox" name="copyRecord" id="copyRecord" value="1" />
                
                <input type="submit" class="btn btn-primary" id="dbosSaveRecord" value="Save" />
                
          </form>
          <? }
          
          ?><br /><br /><br /><?php
          
    }

    /*
    *  Alias to the "edit" function
    */
    function showForm($opts=false) {
        $this->edit($opts);
    }

    function editForm($opts=false) {
        $this->edit($opts);
    }


    /**
     * @returns record_id
     *    
     * @var $in = associative array of input data.
     * @version 1.2 JB 7/08
     *   - Added 'on duplicate key' to inserts.
     * @version 1.1
     *   - Stomped on DBObjectAC save function, changed name to saveForm instead.
     */

    public function saveForm($in=false) {
        global $session_user;
        $debug = $_REQUEST['debug'];
        if(!is_array($in)) {
          $in = $_REQUEST[$this->_table];
        }

        if(!is_array($in)) {
          $this->errors[] = "No data given to DBOS->saveForm()";
          show_errors($this->errors);
          return false;
        }

        /**
        * Because this function allows for partially displayed forms, we need to know
        * what fields were shown, and if a checkbox has been unchecked, then no field
        * data is passed.  This is a special field that overcomes that.
        */
        $checkbox_shown = $_REQUEST['checkbox_shown'];
        
        if(is_array($checkbox_shown)) {
            foreach($checkbox_shown AS $cfield) {
                if(empty($in[$cfield])) $in[$cfield] = "";
                if($debug) echo("\n* Unset checkbox for ".$cfield);
            }
        }

        /**
        * I'm not sure why the following would work, since the fields will probably not be
        * in the form itself, thus not set in the $in array.  JB  2/5/09 3:23 PM
        */
        if(in_array("created_by",array_keys($in)) && !$in['created_by']) {
            $in['created_by'] = $session_user->user_name;
        }

        if(in_array("creator_user_id",array_keys($in))) $in[creator_user_id] = $session_user->id;

        // process incoming variables
        foreach($in AS $f=>$v) {
            // deal with dates
            if(!self::$_tableInfo[$this->_table][$f]) $tbl = $this->loadTable();

            // The following came from CCIE, possibly dealt with someplace else.  JB  3/9/09 4:05 PM
            if(self::$_tableInfo[$this->table][$f]['type']=="datetime") {
              if($v) {
                  $timestamp = strtotime($v);
                  $v = date("Y-m-d H:i:s", $timestamp);
               }
            }

            $sql_sets[] = " $f = '".$this->escape(stripslashes($v))."' ";
            $this->$f = $v;
        }

        if($debug) $this->dump($in);
        

        $this->save($in);
        if($debug) $this->da($this);
        return $this->id;
    }
    
    public function escape($in) {
        return addslashes($in);
    }
    
    public function show() {
        da($this);
    }
    
    public function logError($in) {
        $this->showError($in);
    }
    
    public function showError($in) {
        ?><div class='alert alert-danger'><?= $in ?></div><?php
    }
    
    /**
    * Use phpmyadmin relationship info along with any mysql table and field data.
    *
    * @returns array of table info
    * @overrides DBObjectAC::_loadTableInfo()
    *
    * @version 1.0 Jonah B  8/12/08 12:44 PM
    */

    public function _loadTableInfo($className=false) {
        $debug = false;
        $size = 0;
        if($debug) echo "ran DBOS::_loadTableInfo()";
        // non _fields are depreciated, but for backwards compatibility.  JB  8/12/08 12:45 PM
        if($this->_table) {
            $tn = $this->_table;
        } elseif($this->table) {
            $tn = $this->table;
        }

        if($this->_db) {
            $db = $this->_db;
        } else {
            $db = $this->db;
        }

        if(!$tn) {
            $this->showError("The function _loadTableInfo couldn't find the table.  Be sure to set _table for the class.");
             return false;
        }

        // Don't do this more than once if it's already set.  JB  8/12/08 12:56 PM
        if (
            isset(self::$_tableInfo[$tn]) 
            && is_array(self::$_tableInfo[$tn]) 
            && count(self::$_tableInfo[$tn])
        ) {
            if(is_array(self::$_tableInfo[$tn])) {
                if(count(self::$_tableInfo[$tn])) {
                    if($debug) echo "returned cached table value.";
                    return self::$_tableInfo[$tn];
                }
            }
        } else {
            // Do a db call to load it.  Consider a session cache for this.  JB  8/12/08 1:08 PM
            $sql = "SHOW FULL COLUMNS FROM `$tn`";
          
            if($debug) echo $sql;
            global $wpdb;
            
            // $columns = $this->fetchAll($sql);
            if($this->_columns) {
                $columns = $this->_columns;
            } else {
                $columns = json_decode(file_get_contents(dirname(__FILE__)."/../tables/".$this->_table.".json"));
                $this->_columns = $columns;
                // $this->da($columns);
            }
            if($debug) $this->dump($columns);
            
            /*
            if (!isset($columns) || !count($columns)) {
                $this->showError('No columns found for table '.$this->_table);
                return false;
            }
            */
            $ti = array();
          
            foreach($columns as $row) {
                if(is_object($row)) {
                    $field = $row->name;
                } else {
                    $field = $row['Field'];
                }
                
                if($this->_lowercase) {
                    $fn = strtolower($field);
                } else {
                    $fn = $field;
                }
                
                $ti[$fn] = array();
                if(is_array($row)) {
                    foreach ($row as $k => $v) {
                        if($this->_lowercase) {
                            $k = strtolower($k);
                        }
                        // break type into more useful type and size components
                        if($k=="type") {
                            $ps = explode("(",$v);
                            if(count($ps)) {
                              $k = $ps[0];
                              if(count($ps)>1) {
                                  $size = substr($ps[1],0,-1);
                              }
                              $ti[$fn]['size'] = $size;
                            }
                            $ti[$fn]['type'] = $k;
                        } else {
                            $ti[$fn][$k] = $v;
                        }
                        // look for phpmyadmin relationship data
      
                    }
                } else {
                    $ti[$fn]['type'] = $row->type;
                    // $this->da($row);
                    $ti[$fn]['size'] = $row->length;
                }
                
                if($rel = $this->getRelation($fn)) {
                    $ti[$fn]['rel'] = $rel;
                }
            }
            
            self::$_tableInfo[$tn] = $ti;
            $this->tbl = $ti;
            // mysql_free_result($res);
            // $this->da($ti);
            return $ti;
        }
    }
    
    /**
     * Assumes doctrine ORM or $wpdb
     *
     * @version 1.1 Jonah B  9/11/18 7:27 PM
     * @version 1.0 Jonah B  5/1/16 9:47 PM p24
     */
    function fetchAll($sql=false) {
        global $wpdb;
        
        
        if(!$sql) {
            $sql = "SELECT * FROM `{$wpdb->prefix}{$this->_table}` ";
        }
        return $wpdb->get_results($sql);
    }
    
    function wpFetchAll($sql) {
        global $table_prefix;
        global $wpdb;
        return $wpdb->get_results($sql);
    }
    
    /**
    * Assumes doctrine ORM
    *
    * @version 1.0 Jonah B  5/1/16 9:47 PM p24
    */
    function fetchOne($sql=false) {
        if(!$sql) {
            $sql = "SELECT * FROM `$this->_table` ";
        }
        
        $statement = $this->_dbo->prepare($sql);
        $statement->execute();
        $out = $statement->fetchAll();
        
        return $out[0];
    }

    /*
    * @alias to _loadTableInfo();
    * @author JB  8/15/08 10:03 PM
    */

    public function loadTable($table=false) {
        if(!$this->_titleField) {
            $this->_titleField = $this->_idField;
        }
        return $this->_loadTableInfo();
    }

    /*
    * @author Jonah  10/25/08 12:09 PM
    * @returns an associative array of fields matched with request input values.
    * Under construction, only supports custom fields.
    */

    static function get_input($fields=false) {
        if(is_array($fields)) {
            foreach($fields AS $f) {
                $in[$f] = $_REQUEST[$f];
            }
            return $in;
        } else {
            // @todo, load fields from table metadata.
        }

    }

    /*
    * Under construction.  JB  8/4/08 5:31 PM
    */

    public function _get_record_name($id=false,$table=false,$db,$id_field) {
        $sql = "SELECT * FROM pma_table_info WHERE 'db_name' = '$db' AND table_name = '' ";

        // $display_field

        $sql = " SELECT $display_field FROM $table WHERE ";
    }

    /**
    * @todo - Consolidate this into a single call for a db table, so you don't do so many calls. JB  12/18/08 9:02 PM
    * @returns Array describing foreign table/key relationship of this field, or false.
    */
    public function getRelation($field) {
        global $zdb_schema;
        $use_cache = false;
        
        if($use_cache) {
            $cache = new ACCache();
            $cache->key = $this->_db."_".$this->_table."_".$field;
        }
        // echo $cache->key."<br />";
        $debug=false;
        if(!is_object($zdb_schema)) return false;
        // now see if we have some phpmyadmin relational data

        $sql = " SELECT * FROM pma_relation
        WHERE master_db='{$this->_db}' AND master_table = '{$this->_table}' AND master_field = '{$field}' ";

        if($use_cache) {
            $result = $cache->get();
            if($result!==false) {
                 // hit
            } else {
                 $result = $zdb_schema->fetchAll($sql);
                 $cache->addValue($result,60*10);
            }
        } else {
            $result = $zdb_schema->fetchAll($sql);
        }

        if($num=count($result)) {
            $row=$result[0];
            return $row;
        } else {
            return false;
        }

    }

    /**
    * This works with scriptaculous in-place editor for text data types only.
    *
    * @todo - Return the field name of the related table record in the case of relationships.
    *
    * @version 1.0 Jonah  10/25/08 12:30 PM
    * @returns the value given to the function that was just updated in the db.
    */

    public function saveAjax($opts=false) {
        $debug = false;
        $R =& $_REQUEST;  // Why?  Because it's a lot faster to type.  JB  7/27/10 9:37 AM
        if($opts && is_array($opts)) extract($opts);

        $idField = $this->_idField;

        //  validate some variables
        if(!isset($R[value])) {
          $errors[] = "Value field not provided to DBOS:ajax_db, information was not saved.  Use standard edit link.";
        } else { $value=$R[value]; }

        if(!($field=$R['field'])) {
          $errors[] = "Field var not provided to DBOS:ajax_db, information was not saved.  Use standard edit link.";
        }
        /*
        if(!is_numeric($this->$idField) && !is_array($this->_idField)) {
            $errors[] = "DBOS:ajax_db must have numeric id to update. ".$this->$idField."";
        }
        */
        if($errors) {
          show_errors($errors);
          return;
        }

        // Some data types will be translated
        if($R['type']=="binary") {
          if(strtolower($value) == "yes" || $value==1) {
              $value=1;
              $out_value = "Yes";
           } else {
              $value=0;
              $out_value = "No";
           }
        } else {
            // $out_value = $R[value];
        }
        // ok, we have sane data, give it a try:
        // $sql = " UPDATE  $this->table SET $field = '".mes($value)."' WHERE $this->indexField = '$this->$this->indexField' LIMIT 1 ";
        // $this->set($field,$value);
        $this->$field = $value;
        
        // Set the id fields if this is an array index.  The following seems to have no effect.  Jonah B  4/22/14 7:16 PM
        if(is_array($this->_idField)) {
            foreach($this->_idField AS $idField) {
                if(!$this->{$idField} && $_REQUEST[$idField]) {
                    $this->{$idField} = $_REQUEST['idField'];
                }
            }
        }
        $this->save();
        // da($this);
        // $this->db_query($sql);
        if(!$out_value) $out_value = $value;
        ?><?= $out_value ?><?
    }

    /*
    * @returns MySQL result;
    */

    public function db_query($sql) {
        if($this->db && !$this->_db) $this->_db = $this->db;
        if(!$this->_db) $this->_db = PERSON_DB;
        $result=mysql_db_query($this->_db,$sql) or mysql_die($sql); if($debug) { echo "<hr><pre> $sql </pre><hr>"; }
        return $result;
    }

    // just a helper function to facilitate writing SQL
    protected function _sql_pairs($ar) {
        return sql_pairs($ar);
    }


    /**
    * Show records from another linked table that are related to this one.
    *
    * @author jonah  8/27/08 1:14 PM
    * @version 0.1 experimental  8/27/08 1:14 PM JB
    * @function ACRecord->showRelated
    * @param string foreign_table
    * @param array opts get_result
    *    
    * @version 1.0 jonah  8/27/08 1:14 PM
    * @version 0.1 experimental  8/27/08 1:14 PM JB
    */
    function showRelated($foreign_table=false,$opts=false) {
    
        if(is_string($opts)) {
            $targetField = $opts;
        }
        if(is_array($opts)) extract($opts);
        
        if(!$targetField) {
            $targetField = $this->_indexField;
        }
        if($foreign_table) {
            $sql = " SELECT * FROM `$foreign_table` WHERE `$targetField` = $this->id ";

            if($this->debug) { echo $sql; }
            
            if($this->_dbo) {
                $rows = $this->fetchAll($sql);
            } else {
                $result=mysql_db_query($this->_db,$sql) or mysql_die($sql); if($debug) { echo "<hr><pre> $sql </pre><hr>"; }
            }
            if($get_result) return $result;

            // It would be nice to use the object here, and be able to customize with the $this->summary_link function.
            if(class_exists($foreign_table_disabled)) {
                print_r($foreign_table);
                
                foreach($rows AS $row) {
                    $this->da($row);
                    $object = DBOS::factory($foreign_table,$row['id']);
                    if(is_object($object)) {
                        $object->summary_link();
                    }
                }
            } else {
                ?>
                <div class='showRelated'>
                <?= $this->showTableLabel($foreign_table); ?>
                <table class='table items table-striped'><?php
                foreach($rows AS $count=>$row) {
                    if($count==0) {
                        ?><thead>
                        <?php foreach($row AS $f=>$v) { ?>
                            <th><?= $this->showFieldLabel($f);  ?></th>
                        <? } ?>
                        </thead>
                        <tbody>
                        <?php
                    }
                    ?><tr><?php
                    foreach($row AS $f=>$v) {
                        if($f=="id" && $v) {
                            // echo "id_field $f $v $foreign_table ";
                            $c = DBOS::factory($foreign_table,$v);
                            if(!is_object($c)) {
                                if(class_exists($foreign_table)) {
                                    $c = new $foreign_table($v);
                                }
                            }
                        } elseif($v) {
                            $c = DBOS::factory($f,$v);
                        }
                        if(is_object($c)) {
                            $v = $c->summary_link();
                        }
                    
                        ?><td><?= $v ?></td><?php
                        
                    }
                    ?></tr><?php
                    
                }
                ?>
                </tbody>
                </table>
                </div><?php
            }

        }
    }
    
    function showTableLabel($in) {
        ?><h3 class='dbosTableLabel'><?= $this->camelCaps($in) ?></h3><?php
    }
    function showFieldLabel($in) {
        echo $in;
    }
    
    /**
     * Show the action cell for the admin table
     *
     * @version 1.1 JB added some bulk edit features 12/8/11 11:39 AM
     * @version 1.0 Jonah  9/2/08 7:34 AM
     *
     */

    function adminActionCell($row) {
        if(is_object($row)) {
            $row = get_object_vars($row);
        }
        
        if(!is_array($row)) {
            $row = get_object_vars($this);
        }
        if(is_array($this->_idField)) {
          foreach($this->_idField AS $field) {
              $unique_field_get .= "&".$field."=".$row[$field];
          }
        } else {
            $unique_field_get = "&".$this->_idField."=".$row[$this->_idField];
        }

        // The following is slow, but brings a lot more info.  Necessary for delete functionality and multi-index tables.  JB  9/4/08 11:48 AM
        // $record = DBOS::factory($this->_table,$row[$this->_idField]);

        ?>
        <span class="acRecordActionsWrapper">
            <nobr>
            <?php if($this->_allowBulkEdit!==false) { ?> 
                <input type="checkbox" name="recordsSelected[<?= $this->id ?>]" value="1" id="" /> |
            <?php } ?>
            <nobr>
            <a 
                class='btn btn-primary'
                href="<?= $this->_controller ?>&action=edit_<?= $this->_table ?><?= $unique_field_get ?>&table=<?= $this->_table ?>">
                <i class='fa fa-edit'></i> Edit</a>
            </nobr>
            
            
            <?php if(!$this->_hideViewLink) { ?>
            <span class='divider'>|</span> 
            <nobr>
            <a href="<?= $this->_controller ?>&action=<?= $this->_table ?>_summary&<?= $unique_field_get ?>&table=<?= $this->_table ?>">
                <i class='fa fa-list'></i> View</a>
            </nobr>
            <?php } ?>
            <?php $this->adminActionCellExtended($row); ?>
            <?php if($this->_allowDelete) { ?>
              <?php if($this->_disableConfirmDelete) {
                $deleteUrl = $this->_controller."&action=delete&table=".$this->_table.$unique_field_get;
              } else {
                $deleteUrl = "javascript:confirm_delete('".$this->_controller."?action=delete&table=".$this->_table.$unique_field_get."','this record');'";
              } ?>
              | 
              <nobr class='actionCellDeleteWrapper'>
              <a class="deleteMe"
                  href="<?= $deleteUrl ?>"
                  ><i class='fa fa-trash-o'></i> Delete</a>
                </nobr>
            <?php } ?>
            </nobr>
        </span>
        <?php
    }

    /**
    * Add actions into the admin row for this field.
    *
    * @version 1.0 JB  5/2/11 9:26 AM
    */
    public function adminActionCellExtended($row) {
        ?><span id="adminActionCellExtended"></span><?php
    }
    
    function getEditLink() {
        $uniqueFieldGet = $this->uniqueFieldGet();
        ob_start();
        ?><a href="<?= $this->_controller ?>?action=edit_<?= $this->_table ?><?= $unique_field_get ?>&table=<?= $this->_table ?>">Edit</a><?php
        return ob_get_clean();
    }

    /**
    * @version 1.0 JB  7/26/11 11:18 AM
    */
    function uniqueFieldGet() {
        if(is_array($this->_idField)) {
          foreach($this->_idField AS $field) {
              $unique_field_get .= "&".$field."=".$this->$field;
          }
        } else {
            $unique_field_get = "&".$this->_idField."=".$this->{$this->_idField};
        }
        return $unique_field_get;
    }
    /*
    * Make some attempt to show the record as html.
    *
    * @function ACRecord->toHTML
    * @version 1.0 alpha JB  8/14/08 2:21 PM
    */

    function toHTML($opts=false) {
        global $session_user;
        if(is_array($this->_hiddenFields)) {
            foreach($this->_hiddenFields AS $hide) {
                unset($this->{$hide});
            }
        }
        unset($this->ajaxId);
        $this->pretty($this);
    }

    /*
    * Show the link to this record in admin.
    * @returns html with the link
    *
    * @version 1.1 Jonah B  10/9/12 9:23 PM - Only arrow is linked.
    */

    function summary_link($opts=false) {
        ob_start();
        if($opts && is_array($opts)) extract($opts);
        if($title) {
            $recordTitle = $title;
        } elseif($this->_title) {
            $recordTitle = $this->_title;
        } else {
            $titleField = $this->_titleField;
            $recordTitle = $this->$titleField;
        }
        if(!$recordTitle) {
            $recordTitle = $this->id; 
        }
        // da($record_title);
        ?>
        <span class="summary_link">
            <?= $recordTitle ?><a target="_top" id="recordSummary" href="<?= $this->_controller ?>?table=<?= $this->_table ?>&action=summary&<?= $this->_idField ?>=<?= $this->id ?>">&nbsp;&gt;</a>
        </span>
        <?
        return ob_get_clean();
    }
    
    /**
    * @version 1.0 Jonah B  2/18/14 12:02 PM
    */
    function ajaxDeleteButton() {
        ob_start();
        ?><a href="#" class="deleteMeAjax" title='Delete this record' onclick="deleteRecord('<? $this->_table ?>',<?= $this->id ?>)"><i class='fa fa-trash-o'></i></a><?php
        return ob_get_clean();
    }
    
    /**
    * @returns The object associated with this table, with the record associated with the id given.
    *
    * @version 1.0 JB  8/27/08 1:56 PM
    */

    static function factory($table,$id) {
        // Only put the oddballs in this switch.  JB  10/5/08 7:43 PM
        global $wpdb;
        $table = strtolower($table);
        $table = str_replace($wpdb->prefix,'',$table);
        
        switch($table) {
            case"engagements":
                return new EngagementObject($id);
                break;
            case"report":
               return new ACReport($id);
               break;
            case"report_column":
                return new ReportColumn($id);
                break;
            case"user":
            case"owner_id":
            case"user_id":
              return new ACUser($id);
              break;
                          
            case"bug":
              return new Task($id);
              break;
            default:
              $table = ucwords($table);
              $table = str_replace(" ","",$table);
              if(class_exists($table)) {
                  return new $table($id);
            } else {
                  echo("\n* Could not find class for ".$table);
            }
        }
    }


    /**
    * Output header when displaying an iframed widget that needs to load javascript and css
    *
    * @version 1.0 JB  2/8/11 7:02 PM
    */

    function widgetHeader($opts=false) {
        if(is_array($opts)) extract($opts);
        ?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
    <title><?= $title ?></title>
    <link href="/admin/includes/admin_style.css" rel="stylesheet" type="text/css" />
    <script src="/core/includes/js/scriptaculous/scriptaculous-lite-mnv16rc1a.js" type="text/javascript"></script>
    <script src="/admin/includes/js/admin_functions.js" type="text/javascript"></script>
</head>
        <?php
    }

    /**
    * @version 0.9 beta JB  7/25/11 2:00 PM
    */
    function uploadImageLink($opts=false) {
        if($opts && is_array($opts)) extract($opts);
        if($file_name) $url .= "&file_name=$file_name";
        ?><a class="addMe" id="add_image" href="/admin/action/admin_action.php?action=upload_image_form&table=<?= $this->_table ?>&<?= $this->_idField ?>=<?= $this->id ?><?= $url ?>">Add Image</a><?php
    }

    /**
    * @version 1.0 JB  7/25/11 1:42 PM
    */
    function addImageForm($field=false,$opts=false) {
        if(!$field) $field = "image";
        ?>
         <form name="newad" method="post" enctype="multipart/form-data"  action="<?= $_SERVER['PHP_SELF'] ?>">
         <input type="hidden" name="<?= $this->_idField ?>" value="<?= $_REQUEST[$this->_idField] ?>" />
         <input type="hidden" name="action" value="put_uploaded_image" />
         <input type="hidden" name="table" value="<?= $this->_table ?>" />
         <table>
            <tr>
                <td></td>
                <td><input type="file" name="image"></td>
            </tr>
            <tr>
                <td class="field_label">File name</td>
                <td><input type="text" name="file_name" value="<?= $_REQUEST['file_name']; ?>" size="80" /></td></tr>
            <tr><td><input name="Submit" type="submit" value="Upload image"></td></tr>
         </table>
         </form>
         <?php
         $this->toHTML();
    }

    /**
    * @returns The path to the uploaded image.
    *
    * @version 1.0 JB  7/25/11 1:46 PM
    */
    function putUploadedImage($field=false) {
        $quality = 9;
        if(!$field) $field = "image";
        //define a maxim size for the uploaded images in Kb
         define ("MAX_SIZE","1000");
         if($this->_uploadDir) {
            $image_dir = $this->_uploadDir;
         } else {
            echo("\n* Error: please specify _uploadDir for this class.");
            return false;
         }
        //This variable is used as a flag. The value is initialized with 0 (meaning no error  found)
        //and it will be changed to 1 if an errro occures.
        //If the error occures the file will not be uploaded.
         $errors=0;
        //checks if the form has been submitted
        if(isset($_POST['Submit']))
        {
            //reads the name of the file the user submitted for uploading
            $image=$_FILES['image']['name'];
            //if it is not empty
            if ($image)
            {
                //get the original name of the file from the clients machine
                $filename = stripslashes($_FILES['image']['name']);
                //get the extension of the file in a lower case format
                $extension = DBOS::getImageExtension($filename);
                $extension = strtolower($extension);
                $this->imageExtension = $extension;
                //if it is not a known extension, we will suppose it is an error and will not  upload the file,
                //otherwise we will do more tests
                if (($extension != "jpg") && ($extension != "jpeg") && ($extension != "png") && ($extension != "gif"))
                {
                    //print error message
                    echo '<h1>$extension is Unknown image extension!</h1>';
                    $errors=1;
                }
                else
                {
                    //get the size of the image in bytes
                    //$_FILES['image']['tmp_name'] is the temporary filename of the file
                    //in which the uploaded file was stored on the server
                    $size=filesize($_FILES['image']['tmp_name']);

                    //compare the size with the maxim size we defined and print error if bigger
                    if ($size > MAX_SIZE*1024)
                    {
                       echo '<h1>You have exceeded the size limit!</h1>';
                       $errors=1;
                    }

                    //we will give an unique name, for example the time in unix time format
                    $image_name=time().'.'.$extension;
                    //the new name will be containing the full path where will be stored (images folder)
                    if($_REQUEST['file_name']) {
                        $image_path = $image_dir.$_REQUEST['file_name'].".".$extension;
                    } else {
                        $image_path=$image_dir.$_FILES['image']['name'];
                    }
                    //we verify if the image has been uploaded, and print error instead
                    $copied = copy($_FILES['image']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].$image_path);
                    if($this->_sourceImageType=="png") {
                        if($extension=="jpg") {
                            $image = imagecreatefromjpeg($_FILES['image']['tmp_name']);
                            imagepng($image, $_SERVER['DOCUMENT_ROOT'].$image_dir.$_REQUEST['file_name'].".png", $quality);
                            imagedestroy($image);
                        }
                    }
                    if (!$copied)
                    {
                        echo '<h1>Copy unsuccessfull!</h1>';
                        $errors=1;
                    }
                }
            }
        }

        //If no errors registred, print the success message
         if(isset($_POST['Submit']) && !$errors)
         {
            echo "<h1>File Uploaded Successfully to {$image_path}! </h1>";
            ?><img src="<?= $image_path ?>" /><?php
            return $image_path;
         }

    }

    /**
    * This function reads the extension of the file. It is used to determine if the file  is an image by checking the extension.
    */
    function getImageExtension($str) {
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }

    /**
    * HTML Pulldown function for the very lazy.  Useful for fast and easy admin forms, but
    * not recommended for high traffic public stuff.  Just hand it a table name and it
    * just might give you a pulldown of the records in it.
    *
    * @package ActiveCore
    * @author jonah 2007
    *
    * @version 1.4 GP Jonah  10/27/08 5:16 PM
    *  - Fixed bug where it would not default to blank or 0.
    * @version 1.3 GP Jonah  10/20/08 9:18 AM
    *  - ability to change the value of the 'choose' field.
    * @version 1.2 GP  4/25/08 6:17 PM
    *  - added phpmyadmin display field support
    */

    static function pulldown($table, $opts=false,$opt=false) {
        global $db_name, $errors;
        // Assume that the table has a table_id field, and use that as the "value"
        if(is_string($opts)) {
            $id_value = $opts;
            unset($opts);
        }
        // Dumb, but needed for backwards compatibility. JB 2007
        if(!$opts) $opts = array();
        if(is_array($opt)) $opts = array_merge($opt,$opts);

        if($opts[id_field]) {
            $id_field = $opts[id_field];
        } else {
            $id_field = $table."_id";
        }

        if($opts['choose'] === true || $opts['choose'] == 1) {
            $opts['choose'] = "Choose...";
        }

        if(isset($opts['default'])) {
          $id_value = $opts['default'];
        } elseif($_REQUEST[$id_field]) {
          $id_value = $_REQUEST[$id_field];
        } elseif(!$id_field) {
          global $$id_field;
          $id_value = $$id_field;
        }
        if($opts['frozen']) $opts['read_only'] = true;
        // determine the db
        if($opts[db]) {
          $db_name = $opts[db];
        } else {
          $db_name = $db_name;
        }
        // try to determine name field
        if(!$opts[name_field]) {
            $fields=get_table_fields($table, $db_name);
            if(in_array("name", $fields)) {
              $name_field = "name";
            } elseif(in_array("title", $fields)) {
              $name_field = "title";
            } elseif(in_array($table."_name", $fields)) {
              $name_field = $table."_name";
            } elseif(in_array($table."_title", $fields)) {
              $name_field = $table."_title";
            } elseif(in_array($table."_label", $fields)) {
              $name_field = $table."_label";
            } else {
              // last ditch effort to see if this has been defined in phpmyadmin
              $sql = " SELECT display_field FROM pma_table_info WHERE db_name = '$db_name' AND table_name = '$table' ";
              $result=mysql_db_query("phpmyadmin",$sql) or mysql_die($sql); if($debug) { echo "<hr><pre> $sql </pre><hr>"; }
              $row=mysql_fetch_assoc($result);
              $name_field = $row[display_field];
            }
        } else {
          $name_field = $opts[name_field];
        }

        // Failed to find a good name field, use id instead.  JB  9/4/08 2:04 PM
        if(!$name_field) {
          $name_field = $id_field;
        }
        // now do the html output

        // Make the sql
        if(!$opts[read_only]) {
          $sql = "SELECT $id_field";
          if($name_field) {
              if(is_array($name_field)) {                
                $sql .= ", LEFT(".$name_field['column_id'].",40) AS title";
              } else {
                $sql .= ", LEFT($name_field,40) as $name_field ";
              }
          } else {
              // fairly useless, but still functional.
              $name_field = $id_field;
              $sql .= ", $id_field ";
          }
          $sql .= " FROM $table  WHERE 1=1 {$opts[where]} ";
          if($name_field) $sql .= " ORDER BY $name_field ";
          $sql .= " LIMIT 5000 ";
          if($fname=$opts['fname']) {
              // they have already created the array, use that
              if(DBOS::contains("[",$fname)) {
                $name = $fname;
              } else {
                // they used fname as the name of the array, now add the field to it
                $name="{$opts[fname]}[{$id_field}]";
              }
          } else {
              $name = $id_field;
              if($opts['multiselect']) $name .= "[]";
          }
          $result = acquery($sql,$db_name);
          $num=mysql_num_rows($result);
          if($num>1000 && !$this->contains("[]",$name)) {  // use an autocomplete
              /*
              * This doesn't really work, the only reason I have it at all is because a pulldown of more than a thousand records
              * would break anyway.  JB  4/14/09 10:50 AM
              * @todo Make pulldowns autocomlete search when more than 400 records. JB  7/19/09 12:32 PM
              */
              ?>
              <input type="text" id="<?= $name ?>" name="<?= $name ?>" value="<?= $id_value ?>"/><div id="<?= $table ?>_choosen"></div>
              <div id="<?= $table ?>_menu" class="auto_complete"></div>
              <span class="caption">Start typing and select an option from the list that appears.</span>
              <script language="javascript">
                new Ajax.Autocompleter('<?= $name ?>','<?= $table ?>_menu','/admin/xml/?action=search&table=<?= $table ?>&field=<?= $name ?>',{ tokens: ['\n'],  minChars: 2 });
              </script>

              <?
              // if($id_value) echo get_record_name($table, $id_value, $name_field);
          } else {
              if($opts[multiselect]) $multiple = 'multiple="multiple"';
              ?><select name="<?= $name ?>" <?= $multiple ?> >
                <? if($opts[choose]!=false && !$opts[multiselect]) { ?><option value=""><?= $opts[choose] ?></option><? }
              echo select_options($sql, $id_value, false, false, array("db"=>$db_name,"multiselect"=>$opts[multiselect]));
              ?></select><?
          }
        } else {
            if($id_value) { 
                echo DBOS::get_record_name($table, $id_value,$id_field,$db_name); 
            }
        }
    }

    /**
    * @returns string with record name and link to admin area.
    */
    static function get_record_name($table, $id, $field=false,$db=false) {
        global $errors;
        if(!$field) $field = "title";
        if(!$table) $errors[] = "No table value supplied to get_record_name function";
        if(!is_numeric($id)) $errors[] = "No id value supplied to get_record_name function";
        
        if(class_exists($table)) {
            $recordObject = new $table($id);
            return $recordObject->summary_link();
        } else {
            $recordObject = DBOS::factory($table,$id);
            return $recordObject->summary_link();
        }
        /**
        * The following is a crapshoot and is obsolete code.  JB  7/19/09 12:49 PM
        */
        $sql = "SELECT $field FROM $table WHERE ".$table."_id = $id ";
        if(!$db) {
            $result=mysql_query($sql) or mysql_die($sql); if($debug) { echo "<hr><pre> $sql </pre><hr>"; }
        } else {
            $result=mysql_db_query($db,$sql) or mysql_die($sql); if($debug) { echo "<hr><pre> $sql </pre><hr>"; }
        }
        $row=mysql_fetch_array($result);
        return $row[0];
    }
    
    /**
    * @version 1.0 Jonah B  7/8/13 12:32 PM
    */
    
    public function prettyJSON($in) 
    {
        $result = '';
        $level = 0;
        $prev_char = '';
        $in_quotes = false;
        $ends_line_level = NULL;
        $json_length = strlen( $json );
    
        for( $i = 0; $i < $json_length; $i++ ) {
            $char = $json[$i];
            $new_line_level = NULL;
            $post = "";
            if( $ends_line_level !== NULL ) {
                $new_line_level = $ends_line_level;
                $ends_line_level = NULL;
            }
            if( $char === '"' && $prev_char != '\\' ) {
                $in_quotes = !$in_quotes;
            } else if( ! $in_quotes ) {
                switch( $char ) {
                    case '}': case ']':
                        $level--;
                        $ends_line_level = NULL;
                        $new_line_level = $level;
                        break;
    
                    case '{': case '[':
                        $level++;
                    case ',':
                        $ends_line_level = $level;
                        break;
    
                    case ':':
                        $post = " ";
                        break;
    
                    case " ": case "\t": case "\n": case "\r":
                        $char = "";
                        $ends_line_level = $new_line_level;
                        $new_line_level = NULL;
                        break;
                }
            }
            if( $new_line_level !== NULL ) {
                $result .= "\n".str_repeat( "\t", $new_line_level );
            }
            $result .= $char.$post;
            $prev_char = $char;
        }
    
        return $result;
    }
    
    public function dump($v) {
        $this->da($v);
    }   
    
    public function getFields() {
        return $this->tbl;
    }
}  // end class DBOS

/**
* ACTree might end up extending all of this for tree/node type tables.
*/
include_once(dirname(__FILE__)."/ACTree.class.php");

//
//  Some shared functions
//

/**
* Obsolete
*/
function dbos_confirm_delete($table,$id,$db,$indexField=false) {
    // TODO: use a factory to figure out if a specific Data Object has been defined for this table.
    $dbo = new DBOS($table,$id,$db,$indexField);
    start_box("", array("style"=>"width:300;margin:auto;text-align:center;"));
    // TODO: use phpmyadmin "title" metadata to populate this.
    ?>
    Are you sure you want to delete record <?= $dbo->title ?> from table <b><?= $table ?></b>?<br /><br />
    <form action="<?= $PHP_SELF ?>" method="GET" name="form">
    <input type="hidden" name="id" value="<?= $id ?>">
    <input type="hidden" name="table" value="<?= $table ?>" />
    <input type="hidden" name="action" value="dbo_delete" />
    <input type="submit" value="Delete Record">
    </form>
    <?
    end_box();
}

function dbos_delete($table,$id,$db,$indexField=false) {
    $dbo = new DBOS($table,$id,$db,$indexField);
    $dbo->delete();
    show_success("Deleted record $id from table $table");
}
