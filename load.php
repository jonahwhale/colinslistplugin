<?php
// wp post list --post_type=place
// loadHoods();
// https://photos.google.com/share/AF1QipPWBjja3PD4AOXLbUsthf3Jm1BqdjsTsUwvd0M3LiuhzJozTaFyLIF8pG6psBBD1A?key=ZFdWQnNfQW82a1Y1TS1UanVWQWtFX2xLZlpmaFpR

function showFields() {
    $head = "
    Neighborhood
    Confirmed	
    Place	
    Picture	
    Colin's List	
    Why Not / Caveat	
    Address	
    Website	
    Facebook	
    Instagram	
    Twitter	
    Changing Table	
    High Chairs
    Drinks	
    Description	
    Tip";
    
    $fields = explode("\t",$head);
    foreach($fields as $field) {
        print_r("\n".$field);
    }
}

function loadHoods() {
$hoods = "
Belltown
Broadmoor
Capitol Hill
Central District
Denny-Blaine
Downtown
Eastlake
First Hill
International District
Leschi
Madison Park
Madison Valley
Madrona
Montlake
Pioneer Square
South Lake Union
Northeast Seattle
Bryant
Hawthorne Hills
Lake City
Laurelhurst
Maple Leaf
Matthews Beach
Northgate
Ravenna
Roosevelt
U-District
View Ridge
Wedgwood
Windermere
Northwest Seattle
Ballard
Blue Ridge/North Beach
Broadview
Crown Hill
Fremont
Green Lake
Greenwood
Haller Lake
Loyal Heights
Magnolia
Phinney Ridge
Queen Anne
Wallingford
Westlake
Whittier Heights
South Seattle
Beacon Hill
Columbia City
Georgetown
Mount Baker
Rainier Beach
Seward Park
South Park
West Seattle
Admiral
Alki
Fauntleroy
Junction
";
    $hs = explode("\n",trim($hoods));
    foreach($hs AS $h) {
        $cmd = "wp post create --from-post=31 --post_title='".trim($h)."'";
        // $cmd = "wp post create --post_type=neighborhood --post_title='".trim($h)."' --post_status=published ";
        echo r($cmd);
    }
}

$hood = 'Alki';
$alki = "
Alki	3/2019	Alki Cafe		Y		2726 Alki Ave SW, Seattle, WA 98116	https://www.alkicafe.com/	https://www.facebook.com/Alki-Cafe-365747850105/	https://www.instagram.com/alkicafe/	https://twitter.com/AlkiCafe	?		BWH	Breakfast & Lunch Cafe	
Alki	3/2019	Ampersand Cafe		Y		2530 Alki Ave SW, Seattle, WA 98116	-	https://www.facebook.com/ampersandalki/	-	-	Y		BW	Coffee Shop with Sandwiches	
Alki	3/2019	Blue Moon Burgers		Y		2504 Alki Ave SW, Seattle, WA 98116	http://bluemoonburgers.com/	https://www.facebook.com/BlueMoonBurgers	-	https://twitter.com/BlueMoonBurgers	N		BW	Burger Joint	
Alki	3/2019	Cactus		Y		2820 Alki Ave SW, Seattle, WA 98116	https://cactusrestaurants.com/location/alki-beach/	https://www.facebook.com/CactusRestaurants	https://www.instagram.com/cactusrestaurants/	https://twitter.com/eatatCACTUS	Y		BWH	Southwest & Mexican Restaurant	
Alki	3/2019	Christo's On Alki		Y		2508 Alki Ave SW, Seattle, WA 98116	https://christosonalki.com/	https://www.facebook.com/christosonalki	-	https://twitter.com/christosonalki	W Only	Y	BWH	Greek & Italain Restaurant	
Alki	3/2019	Duke's Seafood & Chowder	Y	Y		2516 Alki Ave SW, Seattle, WA 98116	https://www.dukesseafood.com/locations/alki/	https://www.facebook.com/DukesSeafoodandChowder	https://www.instagram.com/dukesseafoodandchowder/	https://twitter.com/dukeseafood	Y		BWH	Seafood Restaurant	
Alki	3/2019	El Chupacabra		Y		2620 Alki Ave SW, Seattle, WA 98116	http://www.elchupacabraseattle.com/locations/west-seattle/	https://www.facebook.com/ChupacabraGreenwood	https://www.instagram.com/chupacabraseattle/	-	Y		BWH	Mexican Restaurant	
Alki	3/2019	Pegasus Pizza & Pasta		Y		2770 Alki Ave SW, Seattle, WA 98116	http://www.pegasuspizza.com/	https://www.facebook.com/Pegasuspizza	https://www.instagram.com/pegasusalki/	https://twitter.com/PegasusAlki	Y		BWH	Pizzeria	
Alki	3/2019	West Seattle Brewing Co. Tap Shack		Y		2536 Alki Ave SW, Seattle, WA 98116	https://westseattlebrewing.com/	https://www.facebook.com/westseattlebrewingco/	https://www.instagram.com/westseattlebrewingco/	https://twitter.com/ws_brewing	N		BW	Brewery Taproom with Beer Garden and Pizza	
Alki	3/2019	Alki Beach Pub		N	No Minors	2722 Alki Ave SW, Seattle, WA 98116								Bar & Grill	
Alki	3/2019	Alki Spud Fish & Chips		N	No Alcohol	2666 Alki Ave SW, Seattle, WA 98116								Quick Seafood Spot	
Alki	3/2019	Homefront Smoothies & Ice Cream		N	No Alcohol	2622 Alki Ave SW #101, Seattle, WA 98116								Ice Cream Shop	
Alki	3/2019	Pepperdock		N	No Alcohol	2618 Alki Ave SW, Seattle, WA 98116								Fish & Chips Spot with Ice Cream	
Alki	3/2019	Starbucks		N	No Alcohol	2742 Alki Ave SW, Seattle, WA 98116								Coffee Shop	
Alki	3/2019	Sunfish		N	No Alcohol	2800 Alki Ave SW, Seattle, WA 98116								Quick Seafood Spot	
Alki	3/2019	Top Pot		N	No Alcohol	2758 Alki Ave SW, Seattle, WA 98116								Coffee Shop with Doughnuts	
";


function loadData($data) {
    $ls = explode("\n",$data);
    foreach($ls AS $s) {
        echo $s;
    }
}
$data .= "

Broadview	5/2019	Broadview Tap House	Y	Y		217 N 125th St, Seattle, WA 98133	https://www.broadviewtaphouse.com/	https://www.facebook.com/Broadview-Tap-House-157577551670991	https://www.instagram.com/broadviewtaphouse/	-	Y	Y (coming soon)	B	Bottle Shop with Taps	There's a whole area dedicated to kids!
";

$data .= "
Ballard	3/2019	Barnicle		?		4743 Ballard Ave NW, Seattle, WA 98107	https://www.thebarnaclebar.com	https://www.facebook.com/thebarnaclebar	https://www.instagram.com/barnaclebar/	-	?		BWCH	Cocktail Bar	
Ballard	3/2019	Marine Hardware		Y		4741 Ballard Ave NW, Seattle, WA 98107	https://www.ethanstowellrestaurants.com/locations/marine-hardware/	https://www.facebook.com/marinehardwareseattle	https://www.instagram.com/marinehardwareseattle/	-	?		BWCH	Small Plates & Cocktails	
Ballard	3/2019	Walrus & Carpenter		?		4743 Ballard Ave NW, Seattle, WA 98107	https://www.thewalrusbar.com/	https://www.facebook.com/thewalrusbar	https://www.instagram.com/thewalrusbar/	https://twitter.com/thewalrusbar	?		BWCH	Oyster Bar	
Ballard	2/2019	418 Public House	Y	Y	All Ages Until 8pm	418 NW 65th St, Seattle, WA 98117	http://www.418publichouse.com/	https://www.facebook.com/418-Public-House-196916440347556/	https://www.instagram.com/418publichouse/	https://twitter.com/418PublicHouse	?		B/W/C/H	Bar with Mexican Food	
Ballard	2/2019	Asadero		Y		5405 Leary Ave NW, Seattle, WA 98107	https://asaderoprime.com/	https://www.facebook.com/AsaderoBallard/	https://www.instagram.com/asaderoprime/	https://twitter.com/AsaderoBallard	?		B/W/C/H	Mexican Restaurant	
Ballard	3/2019	Bad Albert's	Y	Y		5100 Ballard Ave NW, Seattle, WA 98107	-	-	https://www.instagram.com/badalberts	https://twitter.com/badalbertsgrill	N		BWCH	Sports Bar & Grill	
Ballard	2/2019	Ballard Annex Oyster House		Y		5410 Ballard Ave NW, Seattle, WA 98107	https://ballardannex.com/	https://www.facebook.com/BallardAnnex	https://www.instagram.com/ballardannex/	https://twitter.com/ballardoysters	?		B/W/C/H	Seafood Restaurant	
Ballard	2/2019	Ballard Coffee Works		Y		2060 NW Market St, Seattle, WA 98107	https://www.seattlecoffeeworks.com/	https://www.facebook.com/BallardCoffeeWorks/	www.instagram.com/seattlecoffeeworks	-	Y		B/C	Coffee Shop	
Ballard	3/2019	Ballard Pizza Company		Y		5107 Ballard Ave NW, Seattle, WA 98107	http://www.ballardpizzacompany.com/	https://www.facebook.com/BallardPizzaCo/	https://www.instagram.com/ballardpizzaco/	https://twitter.com/BallardPizzaCo	?		BWC	Pizzeria	
Ballard	2/2019	Barking Dog Alehouse	Y	Y		705 NW 70th St, Seattle, WA 98117	http://thebarkingdogalehouse.com/	https://www.facebook.com/barkingdogalehouse/	https://www.instagram.com/barkingdogale/	https://twitter.com/barkingdogale	Y		B/W/C/H	Neighborhood Pub	
Ballard	2/2019	Bastille		Y		5307 Ballard Ave NW, Seattle, WA 98107	http://www.bastilleseattle.com/	https://www.facebook.com/BastilleSeattle	https://www.instagram.com/bastilleseattle/	https://twitter.com/bastilleseattle	?		B/W/C/H	French Restaurant	
Ballard	2/2019	Billy Beach Sushi & Bar		Y		5463 Leary Ave NW, Seattle, WA 98107	http://www.billybeachsushi.com/	https://www.facebook.com/BillyBeachSushiAndBar	https://www.instagram.com/billybeach_sushi/	https://twitter.com/BillyBeachSushi	?		B/W/C/H	Sushi & Sake	
Ballard	3/2019	Bitterroot		Y		5239 Ballard Ave NW, Seattle, WA 98107	https://www.bitterrootbbq.com/	https://www.facebook.com/BitterrootSeattle	https://www.instagram.com/bitterrootbbq/	https://twitter.com/BitterrootBBQ	?		BWCH	BBQ Restaurant & Bar	
Ballard	2/2019	Boar's Nest		Y		2008 NW 56th St, Seattle, WA 98107	http://www.ballardbbq.com/menu/	https://www.facebook.com/The-Boars-Nest-304245062924623/	https://www.instagram.com/ballardbbq/	https://twitter.com/ballardbbq	?		B/W/C	BBQ Joint	
Ballard	3/2019	Bramling Cross		Y		5205 Ballard Ave NW, Seattle, WA 98107	https://www.ethanstowellrestaurants.com/locations/bramling-cross/	https://www.facebook.com/bramlingballard/	https://www.instagram.com/bramlingballard/	https://twitter.com/bramlingballard	?		BWCH	Gastropub	
Ballard	2/2019	Cafe Mox		Y		5105 Leary Ave NW, Seattle, WA 98107	https://www.moxboardinghouse.com/	https://www.facebook.com/MoxBoardingHouse/	https://www.instagram.com/moxboardinghouse/	https://twitter.com/cafemox	?		B/W/C	Board Game Bar with Food	
Ballard	2/2019	Caffe Fiore		Y		5405 Leary Ave NW, Seattle, WA 98107	http://www.caffefiore.com/	https://www.facebook.com/CaffeFioreSeattle	-	https://twitter.com/CaffeFiore	N		B/W	Coffee Shop	
Ballard	2/2019	Chocolat Vitale		Y		6257 3rd Ave NW, Seattle, WA 98107	https://chocolatvitale.com/	https://www.facebook.com/ChocolatVitale/	https://www.instagram.com/chocolatvitale/	https://twitter.com/ChocolatVitale	?		W	Chocolate and Wine Shop	Wine Tastings Available
Ballard	2/2019	Choice Deli & Grocery	Y	Y		6415 8th Ave NW, Seattle, WA 98107	http://ballardchoice.com/	https://www.facebook.com/ballardchoice/	-	-	N		B/C	Convenience Store Turned Beer Stop	
Ballard	2/2019	Dane	Y	Y		8000 15th Ave NW, Seattle, WA 98117	https://www.thedaneseattle.com/	https://www.facebook.com/thedaneseattle/	www.instagram.com/thedaneseattle	https://twitter.com/theDaneSeattle	Y	Y	B/W/C	Danish Coffee and Beer Cafe	Happy Hour prices during Sounders and Seahawks games.  Storytime Thursdays at 11:30am.
Ballard	2/2019	Delancey		Y		1415 NW 70th St, Seattle, WA 98117	http://delanceyseattle.com/	https://www.facebook.com/delanceyseattle/	https://www.instagram.com/delanceyseattle/	-	?		B/W/C/H	Pizzeria	
Ballard	2/2019	Dray		Y		708 NW 65th St, Seattle, WA 98117	https://thedray.com/	https://www.facebook.com/thedrayseattle/	https://www.instagram.com/thedraycafe/	https://twitter.com/thedraycafe	N		B/W/C	Beer Bar with Food	Kids welcome, but very small.
Ballard	2/2019	Egan's Ballard Jam House		Y	All Ages Until 11pm	1707 NW Market St, Seattle, WA 98107	http://www.ballardjamhouse.com/index.html	https://www.facebook.com/Egans-Ballard-Jam-House-41095079711/		-	?		B/W/C	Jazz Venue with Food	
Ballard	4/2019	Egg & Us		Y		4609 14th Ave NW #108, Seattle, WA 98107	https://m.mainstreethub.com/index.php/eggnusseattle#	https://www.facebook.com/eggnseattle/	-	https://twitter.com/eggandusseattle	Y	?	BWCH	Brunch Cafe	Large brunch menu with many mexican items
Ballard	2/2019	El Borracho		Y	All Ages Until 4pm and All Day Sunday	5465 Leary Ave NW, Seattle, WA 98107	https://www.elborracho.co/	https://www.facebook.com/elborrachofrelard/	-	-	?		B/W/C/H	Mexican Restaurant	
Ballard	2/2019	Fat Hen		Y		1418 NW 70th St, Seattle, WA 98117	https://thefathenseattle.com/	https://www.facebook.com/thefathen/	https://www.instagram.com/thefathen/	https://twitter.com/fathenseattle	?		B/W/C/H	Breakfast & Lunch Cafe	
Ballard	2/2019	Full Tilt		Y		5453 Leary Ave NW, Seattle, WA 98107	https://www.fulltilticecream.com/	https://www.facebook.com/Full-Tilt-Ballard-132620266840898/	https://www.instagram.com/fticecream/	https://twitter.com/FTicecream	?		B/C	Ice Cream & Pinball	
Ballard	2/2019	Gather Kitchen & Bar		Y		5605 22nd Ave NW, Seattle, WA 98107	https://gatherkitchenandbar.com/	https://www.facebook.com/gatherballard/	https://www.instagram.com/gatherballard/	-	?		B/W/C/H	Cocktail Restaurant	
Ballard	2/2019	Gracia		Y		5313 Ballard Ave NW, Seattle, WA 98107	http://www.graciaseattle.com/	https://www.facebook.com/Graciaseattle/	https://www.instagram.com/graciaseattle/	-	?		B/W/C/H	Mexican Restaurant	
Ballard	2/2019	Grumpy D's Coffee House	Y	Y		7001 15th Ave NW, Seattle, WA 98117	http://www.grumpydsballard.com/	https://www.facebook.com/grumpydsballard/	https://www.instagram.com/grumpydsballard/	https://twitter.com/grumpydsballard	?		B/W/C	Coffee Shop	
Ballard	3/2019	Hattie's Hat		Y	All Ages Until 9pm	5231 Ballard Ave NW, Seattle, WA 98107	http://hatties-hat.com/	https://www.facebook.com/hatties.hat/	https://www.instagram.com/hattieshat/	-	?		BWCH	Bar with American Food	
Ballard	2/2019	Heritage Distilling		Y		1836 NW Market St, Seattle, WA 98107	https://heritagedistilling.com/	https://www.facebook.com/HeritageDistilling/	www.instagram.com/heritagedistilling	https://twitter.com/HeritageDistill	?		H	Distillery Tasting Room	2oz Tastings Per Adult
Ballard	2/2019	Hi Life		Y		5425 Russell Ave NW, Seattle, WA 98107	https://chowfoods.com/hi-life	https://www.facebook.com/TheHiLifeBallard	www.instagram.com/chowfoodsseattle	https://twitter.com/hilifeballard	?		B/W/C/H	American Grill	
Ballard	2/2019	Hot Cakes		Y		5427 Ballard Ave NW, Seattle, WA 98107	https://getyourhotcakes.com/	https://www.facebook.com/getyourhotcakes/	https://www.instagram.com/getyourhotcakes/	https://twitter.com/getyourhotcakes	?		H	Molten Chocolate Cakery	Boozy Shakes
Ballard	2/2019	India Bistro		Y		2301 NW Market St, Seattle, WA 98107	http://www.seattleindiabistro.com/	https://www.facebook.com/IndiaBistro/	www.instagram.com/indiabistroballard	-	?		B/W/C/H	Indian Restaurant	
Ballard	2/2019	Joli		Y		618 NW 65th St, Seattle, WA 98117	https://joliseattle.com/	https://www.facebook.com/Joli-2491357324212420/	https://www.instagram.com/joliseattle/	-	?		B/W/C/H	New American Restaurant	
Ballard	1/2019	Kangaroo & Kiwi	Y	Y	All Ages Until 9pm On Patio and One Inside Room	2026 NW Market St, Seattle, WA 98107	http://kangarooandkiwi.com	https://www.facebook.com/Kangaroo-and-Kiwi-181849332562/	www.instagram.com/kangarooandkiwi	https://twitter.com/KandKseattle	?		B/W/C/H	Australian Pub	Bring a friend because you need to order at the bar, but can't bring kids there.
Ballard	2/2019	Katsu Burger & Sushi		Y		2034 NW 56th St, Seattle, WA 98107	https://www.katsuburger.com/ballard	https://www.facebook.com/katsuburgerandsushiballlard/	https://www.instagram.com/katsuburger/	https://twitter.com/KatsuBurger	?		B/W/C/H	Japanese Restaurant with Deep Fried Burgers	Kid's Play Area in Back
Ballard	1/2019	Kiss Cafe	Y	Y		2817 NW Market St, Seattle, WA 98107	http://ballardkisscafe.com/	https://www.facebook.com/BallardKissCafe/	-	https://twitter.com/BallardKissCafe	N		B/W/C	Cafe with Sandwiches and Beer	Choose a can from the fridge!
Ballard	2/2019	La Carta de Oaxaca		Y		5431 Ballard Ave NW, Seattle, WA 98107	-	https://www.facebook.com/lacartadeoaxaca/	https://www.instagram.com/lacartadeoaxaca/	-	?		B/W/C/H	Mexican Restaurant	
Ballard	2/2019	La Isla		Y		2320 NW Market St, Seattle, WA 98107	http://laislacuisine.com	https://www.facebook.com/pg/laislacuisine	www.instagram.com/laislacuisine	-	?		B/W/C/H	Puerto Rican Restaurant	
Ballard	2/2019	Lagunita's Taproom		Y		1550 NW 49th St, Seattle, WA 98107	https://www.lagunitas.com/taproom/seattle	https://www.facebook.com/LagunitasSeattle/	https://www.instagram.com/lagunitasbeer/	https://twitter.com/lagunitasbeer	Y		B/C	Brewery Taproom with Food Truck	
Ballard	2/2019	Little Woody's	Y	Y		2040 NW Market St, Seattle, WA 98107	http://lilwoodys.com	https://www.facebook.com/lilwoodysseattle	www.instagram.com/lilwoodys	https://twitter.com/lilwoodys	?		B/W/C	Burger Joint	
Ballard	2/2019	Locust Cider		Y		5309 22nd Ave NW Ste D, Seattle, WA 98107	https://www.locustcider.com/locations/seattle/	https://www.facebook.com/locustciderballard/	https://www.instagram.com/locustcider	https://twitter.com/LocustCider	?		C	Cider Taproom	
Ballard	2/2019	Lucky Envelope	Y	Y		907 NW 50th St, Seattle, WA 98107	https://www.luckyenvelopebrewing.com/	https://www.facebook.com/luckyenvbrewing	https://www.instagram.com/luckyenvbrewing/	https://twitter.com/LuckyEnvBrewing	?		B/C	Brewery Taproom with Food Trucks	
Ballard	3/2019	Macleod's		Y	Fish & Chips spot only, not Scottish Pub	5206 Ballard Ave NW, Seattle, WA 98107	https://www.macleodsballard.com/	https://www.facebook.com/MacleodsBallard	https://www.instagram.com/macleodsballard/	https://twitter.com/MacleodsBallard	?		BWCH	Scottish Pub & Restaurant	
Ballard	4/2019	MOD Pizza	Y	Y		6010 15th Ave NW, Seattle, WA 98107	https://modpizza.com/locations/ballard/	https://www.facebook.com/MODPizza/	https://www.instagram.com/modpizza/	https://twitter.com/MODPizza	?		BW	Pizzeria	$2 draft beers on Tuesdays
Ballard	2/2019	No Bones Beach Club		Y		5410 17th Ave NW, Seattle, WA 98107	https://nobonesbeachclub.com/	https://www.facebook.com/nobonesbeachclub	www.instagram.com/nobonesbeachclub	https://twitter.com/NoBonesTruck	?		B/W/C/H	Tiki-themed Vegan Restaurant	
Ballard	2/2019	NW Peaks Taproom	Y	Y		4818 17th Ave NW, Seattle, WA 98107	http://www.nwpeaksbrewery.com/	https://www.facebook.com/NWPeaksBrewery	https://www.instagram.com/nwpeaksbrewery/	https://twitter.com/NWPeaksBrewery	N		B/W/C	Brewery Taproom	
Ballard	2/2019	Other Coast Cafe		Y		5315 Ballard Ave NW, Seattle, WA 98107	http://othercoastcafe.com/	https://www.facebook.com/othercoastcafe.ballard	https://www.instagram.com/othercoastcafe/	-	?		B	Sandwich Shop	
Ballard	2/2019	Palermo Pizza and Pasta		Y		2005 NW Market St, Seattle, WA 98107	http://palermoballard.squarespace.com/	https://www.facebook.com/Palermo-Ballard-114650375212537/	www.instagram.com/palermoballard	-	?		B/W	Family Italian Restaurant	
Ballard	2/2019	Papa's Hot Potatoes		Y		500 NW 65th St, Seattle, WA 98117	https://www.papashotpotatoes.com/	https://www.facebook.com/papashotpotatoes/	https://www.instagram.com/papashotpotatoes/	-	?		B/W/C	Vegan Baked Potatoes	
Ballard	2/2019	Paxti's Pizza		Y		5323 Ballard Ave NW, Seattle, WA 98107	https://www.patxispizza.com/location/ballard-pizza/	https://www.facebook.com/patxispizza	https://www.instagram.com/patxispizza/	-	?		B/W/C/H	Pizzeria	
Ballard	2/2019	Peddler Brewing	Y	Y		1514 NW Leary Way, Seattle, WA 98107	http://www.peddlerbrewing.com	https://www.facebook.com/peddlerbrewing	https://www.instagram.com/peddlerbrewing	https://twitter.com/PeddlerBrewing	Y	N	B/C	Brewery Taproom with Food Trucks	First Sundays is \"Babies in Arms Happy Hour\" for $1/pint off
Ballard	2/2019	Pestle Rock		Y		2305 NW Market St, Seattle, WA 98107	http://pestlerock.com/	https://www.facebook.com/PestleRock/	-	-	?		B/W/C/H	Isan Thai Restaurant	
Ballard	2/2019	Pink Bee		Y		2010 NW 56th St, Seattle, WA 98107	http://www.pinkbeethai.com/	https://www.facebook.com/Pinkbeecurry/	https://www.instagram.com/pinkbeecurrysandwiches/	-	?		B/W/C	Thai Curry and Sandwiches	
Ballard	2/2019	Populuxe Brewing	Y	Y		826 NW 49th St, Seattle, WA 98107	https://www.populuxebrewing.com/	https://www.facebook.com/PopuluxeBrewing/	https://www.instagram.com/populuxebrewing/	https://twitter.com/PopuluxeBrewing	Y		B/C	Brewery Taproom with Food Trucks	Pinball machines!
Ballard	2/2019	Porkchop & Co		Y		5451 Leary Ave NW, Seattle, WA 98107	https://www.eatatporkchop.com/	https://www.facebook.com/porkchopandco	https://www.instagram.com/porkchopandco	https://twitter.com/eatatPorkchop	?		B/W/C/H	New American Restaurant	
Ballard	2/2019	Reuben's Brews - Taproom		Y		5010 14th Ave NW, Seattle, WA 98107	http://www.reubensbrews.com/	https://www.facebook.com/Reubensbrews/	https://www.instagram.com/reubensbrews/	https://twitter.com/Reubensbrews	Y		B/C	Brewery Taproom with Food Trucks	
Ballard	2/2019	Reuben's Brews - Brewtap	Y	Y		800 NW 46th St, Seattle, WA 98107	http://www.reubensbrews.com/	https://www.facebook.com/Reubensbrews/	https://www.instagram.com/reubensbrews/	https://twitter.com/Reubensbrews	Y		B/C	Brewery Taproom	
Ballard	3/2019	Sawyer		Y		5309 22nd Ave NW, Seattle, WA 98107	http://sawyerseattle.com/	https://www.facebook.com/sawyer.seattle/	https://www.instagram.com/sawyer.seattle/	-	?		BWCH	New American Restaurant & Bar	
Ballard	2/2019	Sen Noodle Bar		Y		2307 NW Market St, Seattle, WA 98107	https://www.sennoodlebar.com/	https://www.facebook.com/sennoodlebar/	-	-	?		?	Southeast Asian Noodle and Soup Dishes	
Ballard	2/2019	Shelter Lounge	Y	Y		4910 Leary Ave NW, Seattle, WA 98107	http://www.shelterlounge.com/ballard/	https://www.facebook.com/shelterloungeballard/	www.instagram.com/shelterlounge	https://twitter.com/shelterlounge	N		B/W/C/H	Lounge with Food and a Fire	Go during off-hours and enjoy complementary smores by the fire.
Ballard	2/2019	Shiku Sushi		Y		5310 Ballard Ave NW, Seattle, WA 98107	http://www.shikusushi.com/	https://www.facebook.com/ShikuSushi/	-	https://twitter.com/ShikuSushi	?		B/W/C/H	Sushi Bar	
Ballard	3/2019	Staple & Fancy		Y		4739 Ballard Ave NW, Seattle, WA 98107	https://www.ethanstowellrestaurants.com/locations/staple-fancy/	https://www.facebook.com/StapleFancy	https://www.instagram.com/stapleandfancy/	https://twitter.com/StapleFancy	?		BWCH	Italian Eatery	
Ballard	3/2019	Stoneburner		Y		5214 Ballard Ave NW, Seattle, WA 98107	https://www.stoneburnerseattle.com/	https://www.facebook.com/stoneburnerseattle	https://www.instagram.com/stoneburnerseattle/	https://twitter.com/EatStoneburner	Y		BWCH	Mediterranean Restaurant	
Ballard	2/2019	Stoup Brewing	Y	Y		1108 NW 52nd St, Seattle, WA 98107	http://www.stoupbrewing.com/	https://www.facebook.com/StoupBrewing/	www.instagram.com/stoupbrewing	https://twitter.com/stoupbrewing	No		B/C	Brewery Taproom with Food Trucks	
Ballard	2/2019	Sushi I		Y		2010 NW 56th Street Seattle, WA 98107	http://sushiiballard.com/	https://www.facebook.com/Sushi-I-at-Ballard-997635323757965/	www.instagram.com/sushi.i.at.ballard	-	?		B/W/C/H	Japanese Sushi Restaurant	
Ballard	2/2019	Thai Thani Kitchen		Y		2021 NW Market St B, Seattle, WA 98107	http://www.thaithanikitchen.com/	-	-	-	?		B/W/C/H	Thai Restaurant	
Ballard	2/2019	Trailbend		Y	All Ages Until 9pm	1118 NW 50th St, Seattle, WA 98107	http://trailbendtaproom.com/	https://www.facebook.com/trailbendtaproom/	https://www.instagram.com/trailbendtaproom/	https://twitter.com/trailbend	?		BWC	Beer Bar with Food	Kids welcome, but no high-chairs or kid ammenities
Ballard	2/2019	Tropicos Breeze		Y		1744 NW Market St, Seattle, WA 98107	https://www.tropicosbreeze.com/	https://www.facebook.com/tropicosbreezeballard/	www.instagram.com/tropicosbreezeballardofficial	https://twitter.com/TropicosBreezeB	?		B/W/C/H	Latin American Restaurant	
Ballard	2/2019	Anchored Ship		N	No Alcohol	5308 Ballard Ave NW, Seattle, WA 98107								Coffee Shop	
Ballard	2/2019	Ballard Beer Company		N	No Minors, Except To-Go Purchases	2050 NW Market St, Seattle, WA 98107								Beer Bar	
Ballard	2/2019	Ballard Smoke Shop Lounge		N	No Minors	5439 Ballard Ave NW, Seattle, WA 98107								Bar	
Ballard	2/2019	Ballard Station Public House		N	No Minors	2236 NW Market St, Seattle, WA 98107								Bar	
Ballard	2/2019	Bauhaus		N	No Alcohol	2001 NW Market St, Seattle, WA 98107								Coffee Shop	
Ballard	2/2019	Blue Glass		N	No Minors	704 NW 65th St, Seattle, WA 98117								Gastropub	
Ballard	2/2019	Cafe Bambino		N	No Alcohol	405 NW 65th St, Seattle, WA 98117								Coffee Shop	
Ballard	2/2019	Caffe Umbria		N	No Alcohol	5407 Ballard Ave NW, Seattle, WA 98107								Coffee Shop	
Ballard	2/2019	Carnivore		N	No Minors	5313 Ballard Ave NW, Seattle, WA 98107								New American Restaurant	
Ballard	3/2019	Conor Byrne		N	No Minors	5140 Ballard Ave NW, Seattle, WA 98107								Irish Pub	
Ballard	2/2019	Corner Spot		N	No Minors	1556 NW 56th St, Seattle, WA 98107								Cocktail Lounge	
Ballard	2/2019	Cupcake Royale		N	No Alcohol	2052 NW Market St, Seattle, WA 98107								Cupcake Shop	
Ballard	2/2019	D'Ambrosio Gelato		N	No Alcohol	5339 Ballard Ave NW, Seattle, WA 98107								Ice Cream Parlor	
Ballard	2/2019	Emerald City Smoothie		N	No Alcohol	1545 NW Market St Suite D, Seattle, WA 98107								Smoothie Shop	
Ballard	2/2019	Essex		N	No Minors	1421 NW 70th St, Seattle, WA 98117								Cocktail Lounge	
Ballard	2/2019	Fitzgerald's		N	No Minors	5811 24th Ave NW, Seattle, WA 98107								Sports Bar	
Ballard	2/2019	Frankie & Jo's		N	No Alcohol	1411 NW 70th St, Seattle, WA 98117								Vegan Ice Cream Shop	
Ballard	3/2019	Gerald		N	No Minors	5210 Ballard Ave NW, Seattle, WA 98107								Cocktail Lounge	
Ballard	2/2019	Hazlewood		N	No Minors	2311 NW Market St, Seattle, WA 98107								Cocktail Lounge	
Ballard	2/2019	Hotel Albatross		N	No Minors	2319 NW Market St, Seattle, WA 98107								Cocktail Lounge	
Ballard	3/2019	King's Hardware		N	No Minors	5225 Ballard Ave NW, Seattle, WA 98107								Bar	
Ballard	2/2019	Larsen's Bakery		N	No Alcohol	8000 24th Ave NW, Seattle, WA 98117								Coffee Shop & Bakery	
Ballard	2/2019	Little Tin		N	No Minors	5335 Ballard Ave NW, Seattle, WA 98107								Cocktail Lounge	
Ballard	3/2019	Lock & Keel		N	No Minors	5144 Ballard Ave NW, Seattle, WA 98107								Bar	
Ballard	2/2019	Market Arms		N	No Minors	2401 NW Market St, Seattle, WA 98107								English Pub	
Ballard	2/2019	Matador		N	No Minors	2221 NW Market St, Seattle, WA 98107								Mexican Restaurant	
Ballard	2/2019	Mighty-O Donuts		N	No Alcohol	1555 NW Market St, Seattle, WA 98107								Donut and Coffee Shop	
Ballard	2/2019	Mike's Chili		N	No Minors	1447 NW Ballard Way, Seattle, WA 98107								Tavern with Chili	
Ballard	2/2019	Miro Tea		N	No Alcohol	5405 Ballard Ave NW, Seattle, WA 98107								Tea Shop	
Ballard	2/2019	Molly McGuire's		N	No Minors	610 NW 65th St, Seattle, WA 98117								Irish Pub	
Ballard	2/2019	Noble Fir		N	No Minors	5316 Ballard Ave NW, Seattle, WA 98107								Beer Bar	
Ballard	2/2019	Obec Brewing		N	No Minors	1144 NW 52nd St, Seattle, WA 98107								Brewery Taproom with Food Trucks	
Ballard	2/2019	Ocho		N	No Minors	2325 NW Market St, Seattle, WA 98107								Tapas Bar	
Ballard	2/2019	Olaf's		N	No Minors	6301 24th Ave NW, Seattle, WA 98107								Pub	
Ballard	2/2019	Old Pequliar		N	No Minors	1722 NW Market St, Seattle, WA 98107								Pub	
Ballard	3/2019	Percy's & Co		N	No Minors	5233 Ballard Ave NW, Seattle, WA 98107								Cocktail Lounge	
Ballard	2/2019	Pho Than Brothers		N	No Alcohol	2021 NW Market St A, Seattle, WA 98107								Pho Restaurant	
Ballard	2/2019	Pie Bar		N	No Minors	2218 NW Market St, Seattle, WA 98107								Sweet & Savory Pies and Drinks	
Ballard	2/2019	Poke Square		N	No Alcohol	1701 NW Market St, Seattle, WA 98107								Poke Restaurant	
Ballard	2/2019	Rosellini's		N	No Alcohol	1413 NW 70th Street, Seattle WA 98117								Coffee Shop & Bakery	
Ballard	2/2019	Salt & Straw		N	No Alcohol	5420 Ballard Ave NW, Seattle, WA 98107								Ice Cream Parlor	
Ballard	2/2019	Sexton		N	No Minors	5327 Ballard Ave NW, Seattle, WA 98107								Cocktail Lounge	
Ballard	2/2019	Shingletown		N	No Minors	2016 NW Market St, Seattle, WA 98107								Western Bar	
Ballard	2/2019	Sip and Ship		N	No Alcohol	1752 NW Market St, Seattle, WA 98107								Coffee Shop and Shipping	
Ballard	2/2019	Starbucks		N	No Alcohol	2200 NW Market St, Seattle, WA 98107								Coffee Shop	
Ballard	2/2019	Stepping Stone		N	No Minors	5903 24th Ave NW, Seattle, WA 98117								Pub	
Ballard	2/2019	Sunset Tavern		N	No Minors	5433 Ballard Ave NW, Seattle, WA 98107								Bar and Music Venue	
Ballard	2/2019	Take 5 Urban Market		N	No Alcohol	6757 8th Ave NW, Seattle, WA 98117								Neighborhood Market	
Ballard	2/2019	Tarasco		N	No Minors	1452 NW 70th St, Seattle, WA 98117								Mexican Restaurant & Bar	 
Ballard	2/2019	Tin Hat		N	No Minors	512 NW 65th St, Seattle, WA 98117								Bar	
Ballard	4/2019	Un Bien		N	No Alcohol	7302.5 15th Ave NW, Seattle, WA 98117								Caribbean Sandwich Shack	
Ballard	2/2019	Verve Bowls		N	No Alcohol	1764 NW 56th St, Seattle, WA 98107								Acai Bowls & Smoothies	
Ballard	2/2019	Waterwheel Lounge		N	No Minors	7034 15th Ave NW, Seattle, WA 98117								Bar	
";

$data .= "
Central District	2/2019	Chuck's Hop Shop CD		Y		2001 E Union Street, Seattle, WA 98122	https://chuckscd.wordpress.com/	https://www.facebook.com/ChucksCD/	https://www.instagram.com/chuckshopshopcd/	https://twitter.com/ChucksHopShopCD	Y	B/W/C	Bottle Shop with Food Trucks	Santa visits every December
";

$data .= "
Hillman City	2/2019	Hill City Tap House & Bottle Shop		Y		5903 Rainier Ave W, Seattle, WA 98118	https://www.hillcitytapandbottle.com/	https://www.facebook.com/hillcitytapbottle/	https://www.instagram.com/hillcitytap_bottle/	-	Y	B/C	Bottle Shop, BYO food	Has a kids play area!
";

$data .= "
Downtown	1/2019	Taphouse Grill	Y	Y	Minors Allowed, except at bar and in billiards room.	1506 6th Ave, Seattle, WA 98101	http://taphousegrill.com/	https://www.facebook.com/pages/Taphouse-Bar-Grill-Seattle/178288082222349	www.instagram.com/taphousegrill	https://twitter.com/taphousegrill	Y	B/W/C/H	Clean, vibrant restaurant with 160 beers on tap.	Get the nachos and flight of 4.
Downtown 	1/2019	SPIN Seattle	Y	N	No Minors	1511 6th Ave, Seattle, WA 98101								
Downtown	1/2019	Elephant & Castle	Y	Y		1415 5th Ave, Seattle, WA 98101	https://www.elephantcastle.com/home/seattle	https://www.facebook.com/elephantcastle	www.instagram.com/elephantcastle	https://twitter.com/elephantcastle	Y	B/W/C/H	English pub with full food and drink menu.	Order a Black and Tan!
Downtown	4/2019	Triple Door		N	No Minors	216 Union St, Seattle, WA 98101								
Downtown	4/2019	Old Stove Brewery	Y	Y		1901 Western Ave, Seattle, WA 98101	https://www.oldstove.com/	https://www.facebook.com/OldStoveBrewingCompany	https://www.instagram.com/oldstovebeer/	https://twitter.com/oldstovebeer	Y	BWCH	Brewery Taproom with Kitchen	Great views of the waterfront!
";

$data .= "
Fremont	4/2019	Barrel Thief		Y		3417 Evanston Ave N #102, Seattle, WA 98103	https://bthief.com/	https://www.facebook.com/TheBarrelThief/	https://www.instagram.com/thebarrelthief/	https://twitter.com/BarrelThiefSEA	?	?	BWCH	Wine & Whiskey Bar	
Fremont	4/2019	Red Door		Y		3401 Evanston Ave N, Seattle, WA 98103	http://reddoorseattle.com/	https://www.facebook.com/reddoorseattle	https://www.instagram.com/reddoorseattle/	https://twitter.com/RedDoorSeattle	Y	Y	BWCH	Neighborhood Bar	
";

$data .= "
Georgetown	4/2019	BOPBOX		Y		5633 Airport Way S, Seattle, WA 98108	https://www.bopboxco.com/	https://www.facebook.com/bopboxco/	https://www.instagram.com/bopboxco/	-	N	N	BH	Korean Restaurant	Serves beer and soju cocktails.
Georgetown	4/2019	Brother Joe		Y		5629 Airport Way S, Seattle, WA 98108	https://www.brotherjoegt.com/	https://www.facebook.com/brotherjoegt/	https://www.instagram.com/brotherjoegt/	-	N	Y	BWCH	Coffee Shop with Brunch	
Georgetown	4/2019	Cutting Board		Y		5503 Airport Way S, Seattle, WA 98108	http://www.cuttingboardseattle.com	https://www.facebook.com/pages/Cutting-Board/112342738820548	-	-	?	?	BW	Japanese Restaurant	Serves beer, wine, and sake.
Georgetown	4/2019	Flying Squirrel Pizza Co.		Y		5701 Airport Way S, Seattle, WA 98108	http://www.flyingsquirrelpizza.com/	https://www.facebook.com/fspcgtown/	https://www.instagram.com/flyingsquirrel_georgetown/	https://twitter.com/fsquirrelpizza	?	?	BWCH	Pizzeria	
Georgetown	4/2019	Fonda La Catrina		Y		5905 Airport Way S, Seattle, WA 98108	http://www.fondalacatrina.com/	https://www.facebook.com/fondalacatrinageorgetown/	https://www.instagram.com/fondalacatrina/	https://twitter.com/fondalacatrina	Y	Y	BWCH	Mexican Restaurant & Bar	
Georgetown	4/2019	Jellyfish Brewing Company	Y	Y		917 S Nebraska St, Seattle, WA 98108	http://jellyfishbrewing.com/	https://www.facebook.com/jellyfishbrewingcompany/	https://www.instagram.com/jellyfishbrewing/	https://twitter.com/JellyfishBeer	Y	Y	BC	Brewery Taproom	
Georgetown	4/2019	Lowercase Brewing	Y	Y		6235 Airport Way S, Seattle, WA 98108	http://www.lowercasebrewing.com/	https://www.facebook.com/lowercasebeer	https://www.instagram.com/lowercasebeer/	https://twitter.com/lowercasebeer	Y	N	BWC	Brewery Taproom	
Georgetown	4/2019	Machine House Brewery	Y	Y		5840 Airport Way S #121, Seattle, WA 98108	http://www.machinehousebrewery.com/	https://www.facebook.com/MachineHouseBrewery	https://www.instagram.com/machinehouse/	https://twitter.com/machinehouse	N	N	BC	Brewery Taproom	Exclusively cask ales.
Georgetown	4/2019	Mercer Wine Estates		Y		6235 Airport Way S Suite 102, Seattle, WA 98108	http://www.mercerwine.com/	https://www.facebook.com/MercerEstates	https://www.instagram.com/mercer_wine/	https://twitter.com/mercerestates	Y	N	W	Winery Tasting Room	
Georgetown	4/2019	Sneaky Tiki		Y	All Ages Until 10pm	6031 Airport Way S, Seattle, WA 98108	-	https://www.facebook.com/pages/category/Bar/Sneaky-Tiki-2259119237668101/	https://www.instagram.com/sneakytikiseattle/	-	N	Y	BWCH	Tiki Bar & Dance Club	
Georgetown	4/2019	Square Knot Diner		Y		6015 Airport Way S, Seattle, WA 98108	http://squareknotdiner.com/	https://www.facebook.com/TheSquareKnot/	https://www.instagram.com/thesquareknotdiner/	-	N	Y	BWCH	Diner	Open 11am til late!
Georgetown	4/2019	Stellar Pizza	Y	Y		5513 Airport Way S, Seattle, WA 98108	http://www.stellarpizza.com/	https://www.facebook.com/Stellar-Pizza-Ale-363277583831985/	-	https://twitter.com/StellarPizza	W Only	Y	BWCH	Pizzeria	Men can use the Women's single use restroom to use the changing table.
Georgetown	4/2019	Via Tribunali		Y		6009 12th Ave S, Seattle, WA 98108	http://viatribunali.com/georgetown/	https://www.facebook.com/ViaTribunali/	https://www.instagram.com/via_tribunali/	https://twitter.com/ViaTribunali	N	Y	BWCH	Pizzeria	
Georgetown	4/2019	9lb Hammer		N	No Minors	6009 Airport Way S, Seattle, WA 98108								Bar	
Georgetown	4/2019	All City Coffee		N	No Alcohol	1205 S Vale St, Seattle, WA 98108								Coffee Shop	
Georgetown	4/2019	Bar Ciudad		N	No Minors	6118 12th Ave S, Seattle, WA 98108								Cocktail Bar with Large Outdoor Area	
Georgetown	4/2019	El Sirenito		N	No Minors	5901 Airport Way S, Seattle, WA 98108								Mexican-Style Seafood Restaurant & Lounge	
Georgetown	4/2019	Flip Flip, Ding Ding		N	No Minors	6012 12th Ave S, Seattle, WA 98108								Arcade & Bar	
Georgetown	4/2019	Full Throttle Bottles		N	No Minors	5909 Airport Way S, Seattle, WA 98108								Bottle Shop with Taps	
Georgetown	4/2019	Georgetown Liquor Company		N	No Minors	5501 Airport Way S B, Seattle, WA 98108								Bar	
Georgetown	4/2019	Jues Maes Saloon		N	No Minors	5919 Airport Way S, Seattle, WA 98108								Neighborhood Pub with Music	
Georgetown	4/2019	Palace Theater and Art Bar		N	No Minors	5813 Airport Way S, Seattle, WA 98108								Gay Bar with Drag & Burlesque	
Georgetown	4/2019	Seattle Freeze		N	No Alcohol	6014 12th Ave S, Seattle, WA 98108								Ice Cream & Donut Shoppe	
Georgetown	4/2019	Seattle Tavern & Pool Hall		N	No Minors	5811 Airport Way S, Seattle, WA 98108								Bar	
Georgetown	4/2019	Smarty Pants		N	No Minors	6017 Airport Way S, Seattle, WA 98108								Sandwich Shop & Bar	
Georgetown	4/2019	Star Brass Works Lounge		N	No Minors	5813 Airport Way S, Seattle, WA 98108								Bar	
Georgetown	4/2019	Tu Cantinas		N	No Minors	6105 13th Ave S, Seattle, WA 98108								Taco Bar	
";

$data .= "
Green Lake	1/2019	Cocina Oaxaca		Y		7900 East Green Lake Dr N #107, Seattle, WA 98115	http://www.cocinaoaxacafresh.com/	https://www.facebook.com/freshcookin/	-	-	?		BWCH	Mexican Restaurant and Bar	
Green Lake	1/2019	Duke's Seafood & Chowder		Y		7850 Green Lake Dr N, Seattle, WA 98103	https://www.dukesseafood.com/locations/green-lake/	https://www.facebook.com/DukesSeafoodandChowder	https://www.instagram.com/dukesseafoodandchowder/	https://twitter.com/dukeseafood	?		BWCH	Seafood Restaurant	
Green Lake	1/2019	Jak's Alehouse	Y	Y		7900 East Green Lake Dr N, Seattle, WA 98103	http://jaksalehouse.com/	https://www.facebook.com/JaKsAlehouse/	https://www.instagram.com/jaks_alehouse/	-	Y		BWCH	American Pub	
Green Lake	1/2019	Lucia		Y		7102 Woodlawn Ave NE, Seattle, WA 98115	http://www.iheartlucia.com/	https://www.facebook.com/luciagreenlake/	https://www.instagram.com/luciagreenlake/	https://twitter.com/luciagreenlake	?		BWCH	Italian Restaurant	
Green Lake	1/2019	Lunchbox Lab		Y		7200 East Green Lake Dr N, Seattle, WA 98115	http://www.lunchboxlab.com/	https://www.facebook.com/LunchboxLab/	https://www.instagram.com/lunchbox_lab/	https://twitter.com/Lunchbox_Lab	Y		BWCH	Burger Restaurant & Bar	
Green Lake	1/2019	Ming China Bistro		Y		7119 Woodlawn Ave NE, Seattle, WA 98115	http://mingchinabistroseattle.com/	https://www.facebook.com/mingchinabistro/	https://www.instagram.com/mingchinabistrosea/	https://twitter.com/ming_bistro	?		BW	Chinese Restaurant	
Green Lake	1/2019	Mykonos Greek Grill		Y		310 NE 72nd St, Seattle, WA 98115	https://mykonosgreenlake.com/	https://www.facebook.com/Mykonos-Greek-Grill-138792092810644/	-	-	N		BW	Greek Restaurant	
Green Lake	1/2019	Nell's		Y		6804 East Green Lake Way N, Seattle, WA 98115	http://www.nellsrestaurant.com	https://www.facebook.com/Nells-Restaurant-111811830196/	-	-	?		BWCH	New-American Restaurant	Fancy restaurant with white tablecloths.
Green Lake	1/2019	O'Ginger Bistro		Y		6900 East Green Lake Way N, Seattle, WA 98115	http://www.ogingerbistro.net	https://www.facebook.com/pages/Oginger-Bistro/707157579343072	-	-	?		BW	Thai Restaurant	
Green Lake	1/2019	Retreat	Y	Y		6900 East Green Lake Way N, Seattle, WA 98115	https://www.retreat-greenlake.com/	https://www.facebook.com/retreatgreenlake	https://www.instagram.com/retreatgreenlake/	-	?		BWCH	Coffee Shop & Cocktail Bar with Small Plates	
Green Lake	1/2019	Revolutions Coffee		Y		7208 East Green Lake Dr N, Seattle, WA 98115	http://www.revolutionscoffee.com/	https://www.facebook.com/revolutionscoffee/	https://www.instagram.com/revolutionscoffee/	-	?		BW	Coffee Shop	
Green Lake	1/2019	Rosita's Mexican Grill		Y		7210 Woodlawn Ave NE, Seattle, WA 98115	https://www.rositasrestaurant.com/	https://www.facebook.com/rositasgreenlake/	https://www.instagram.com/rositas_greenlake/	-	?		BWCH	Mexican Restaurant and Bar	
Green Lake	1/2019	Shelter		Y		7110 East Green Lake Dr N, Seattle, WA 98115	http://www.shelterlounge.com/greenlake/	https://www.facebook.com/shelterloungeballard/	https://www.instagram.com/shelterlounge/	https://twitter.com/shelterlounge	?		BWCH	Lounge with Food	
Green Lake	2/2019	Spud Fish & Chip	Y	Y		6860 East Green Lake Way N, Seattle, WA 98115	https://www.spudgreenlake.com/	https://www.facebook.com/Spud-Fish-Chip-Green-Lake-108118682563034/	-	-	N		BW	Quick Seafood Spot	
Green Lake	2/2019	Tacos Guymas		Y		6808 East Green Lake Way N, Seattle, WA 98115	http://www.tacosguaymas.com/TG-Green-Lake/	https://www.facebook.com/TacosGuaymas	-	-	Y		BWCH	Mexican Bar, Restaurant, and Quick Food	
Green Lake	2/2019	Teddy's Bigger Burgers		Y		6900 East Green Lake Way N, Seattle, WA 98115	https://www.teddysbb.com/	https://www.facebook.com/teddysbiggerburgers	https://www.instagram.com/teddysburgers/	https://twitter.com/teddysburgers	Y		B	Burger Joint	
Green Lake	2/2019	Tumpike Pizza		Y		6900 East Green Lake Way N, Seattle, WA 98115	http://www.turnpikepizza.com/	https://www.facebook.com/turnpikepizza/	https://www.instagram.com/turnpikepizza/	-	Y		BW	Pizzeria	
Green Lake	2/2019	Zeek's Pizza		Y		7900 East Green Lake Dr N, Seattle, WA 98103	http://zeekspizza.com/locations-hours/zeeks-pizza-greenlake/	https://www.facebook.com/zeekspizza/	https://www.instagram.com/zeekspizza/	https://twitter.com/zeekspizza	Y		BWC	Pizzeria	
Green Lake	2/2019	Ben & Jerry's		N	No Alcohol	7900 E Green Lake Dr N Suite 104, Seattle, WA 98103								Ice Cream Shop	
Green Lake	2/2019	Blank Space Cafe		N	No Alcohol	7214 Woodlawn Ave NE, Seattle, WA 98115								Coffee Shop	
Green Lake	2/2019	Chocolati Cafe		N	No Alcohol	7810 East Green Lake Drive North, Seattle, WA 98115								Coffee Shop	
Green Lake	2/2019	Kitanda Espresso & Acai		N	No Alcohol	428 NE 71st St, Seattle, WA 98115								Coffee Shop with Acai Bowls	
Green Lake	2/2019	Little Red Hen		N	No Minors	7115 Woodlawn Ave NE, Seattle, WA 98115								Country Bar	
Green Lake	2/2019	Menchie's Frozen Yogurt		N	No Alcohol	424 NE 71st St, Seattle, WA 98115								Froyo	
Green Lake	2/2019	Peet's Coffee		N	No Alcohol	6850 East Green Lake Way N, Seattle, WA 98115								Coffee Shop	
Green Lake	2/2019	Starbucks		N	No Alcohol	7100 East Green Lake Dr N, Seattle, WA 98115								Coffee Shop	
Green Lake	2/2019	Urban Bakery		N	No Alcohol	7850 East Green Lake Dr N, Seattle, WA 98103								Cafe with Sandwiches and Baked Goods	
Green Lake	2/2019	Zoëyogurt		N	No Alcohol	6900 East Green Lake Way N Suite E, Seattle, WA 98115								Yogurt Shop	
";

$data .= "
Lake City	2/2019	Elliott Bay Public House & Brewery		Y		12537 Lake City Way NE, Seattle, WA 98125	https://www.elliottbaybrewing.com/locations/lake-city/	https://www.facebook.com/ElliottBayBrewingCompany	https://www.instagram.com/ElliottBayBrewingCo/	https://twitter.com/elliottbay_beer	Y	?	Brewery with Restaurant	Their beers are all organic.
Lower Queen Anne	4/2019	Seattle Center Armory	Y	Y		305 Harrison St, Seattle, WA 98109	http://www.seattlecenter.com/food/	https://www.facebook.com/SeattleCenter	https://www.instagram.com/seattlecenter/	https://twitter.com/seattlecenter	Y	BWCH	Food Court	Lots of food options!  Some places have alcohol.
Laurelhurst / Bryant	2/2019	Burke-Gilman Brewing Co		Y		3626 NE 45th St #102, Seattle, WA 98105	http://www.burkegilmanbrewing.com/	https://www.facebook.com/burkebrewing/	https://www.instagram.com/burkegilmanbrewingcompany	https://twitter.com/burkebrewing	Y	BC	Brewery Taproom	No food so BYO or stop by a place nearby.
Maple Leaf	2/2019	Growler Guys		Y		8500 Lake City Way NE, Seattle, WA 98115	https://www.thegrowlerguys.com/locations/seattle-northeast/	https://www.facebook.com/thegrowlerguysseattlenortheast/	https://www.instagram.com/tggseattlene/	https://twitter.com/tggseattlene	N	BC	Beer Bar with Food	Indoor Seating is limited; great for kids in spring/summer/early fall (good outdoor space).
Maple Leaf	2/2019	The Maple		Y		8929 Roosevelt Way NE, Seattle, WA 98115	https://www.themaplepub.com/	https://www.facebook.com/themapleseattle/	https://www.instagram.com/themapleseattle/		N	BWCH	Neighborhood Pub	Has a covered beer garden
Northgate	2/2019	Watershed Pub & Kitchen		Y		10104 3rd Ave NE, Seattle, WA 98125	https://watershedpub.com/	https://www.facebook.com/watershedpub	https://www.instagram.com/watershedpub/	https://twitter.com/WatershedPub	Y	BWCH	American Restaurant	
";

$data .= "
Phinney Ridge / Greenwood	2/2019	Ada's		Y		5910 Phinney Ave N, Seattle, WA 98103	http://www.adasrestaurantandbar.com/	https://www.facebook.com/adasonphinney/	https://www.instagram.com/adasonphinney	-	?	B/W/C/H	Italian-American Restaurant	
Phinney Ridge / Greenwood	2/2019	Angry Beaver		Y	All Ages Until 10pm	8412 Greenwood Ave N, Seattle, WA 98103	http://www.angrybeaverseattle.com/	https://www.facebook.com/angrybeaverseattle/	https://www.instagram.com/theangrybeaverbar/	https://twitter.com/angrybeaverbar	N	BWCH	Canadian Hockey Bar	
Phinney Ridge / Greenwood	2/2019	Bluebird Ice Cream		Y		7400 Greenwood Ave N, Seattle, WA 98103	https://squareup.com/store/bluebirdicecream	https://www.facebook.com/bluebirdicecrm/	https://www.instagram.com/bluebirdicecream/	https://twitter.com/bluebirdicecrm	?	B	Ice Cream Shop	
Phinney Ridge / Greenwood	2/2019	Caffe Vita		Y		7402 Greenwood Ave N, Seattle, WA 98103	http://www.caffevita.com/	https://www.facebook.com/caffevita	https://www.instagram.com/caffevita/	https://twitter.com/CaffeVita	?	B/W	Coffee Shop	
Phinney Ridge / Greenwood	3/2019	Chuck's Hop Shop	Y	Y		656 NW 85th St, Seattle, WA 98117	https://chucks85th.wordpress.com/	https://www.facebook.com/Chucks-Hop-Shop-Greenwood-116575481734791/	https://www.instagram.com/chuckshopshopgw/	https://twitter.com/Chucks85th	Y	B/W/C	Bottle Shop with Food Trucks	50 Taps!
Phinney Ridge / Greenwood	2/2019	Cornuto		Y		7404 Greenwood Ave N, Seattle, WA 98103	http://www.cornutopizzeria.com/	https://www.facebook.com/cornutopizzeria	https://www.instagram.com/cornutopizzeria/	-	?	B/W/C/H	Vera Pizzeria	
Phinney Ridge / Greenwood	2/2019	El Chupacabra		Y		6711 Greenwood Ave N, Seattle, WA 98103	http://www.elchupacabraseattle.com/locations/greenwood/	https://www.facebook.com/ChupacabraGreenwood	https://www.instagram.com/chupacabraseattle/	-	?	B/W/C/H	Mexican Restaurant	Strollers have to stay outside on the ramp.
Phinney Ridge / Greenwood	2/2019	Flying Bike Cooperative Brewery	Y	Y		8570 Greenwood Ave N, Seattle, WA 98103	https://www.flyingbike.coop/	https://www.facebook.com/flyingbikecoop	https://www.instagram.com/flyingbikecoopbrewery/	https://twitter.com/flyingbikecoop	Y	B/C	Brewery Taproom	Spacious interior, and have some kids toys.
Phinney Ridge / Greenwood	2/2019	Hecho Cantina		Y		7314 Greenwood Ave N, Seattle, WA 98103	http://www.hechoinseattle.com/	https://www.facebook.com/hechoseattle/	https://www.instagram.com/hechoinseattle/	-	?	B/W/C/H	Mexican Restaurant	
Phinney Ridge / Greenwood	2/2019	Modern Japanese		Y		6108 Phinney Ave N, Seattle, WA 98103	http://www.modern-seattle.com/	https://www.facebook.com/ModernSeattle	https://www.instagram.com/modern_japanese_cuisine/	-	?	B/W/H	Japanese Pastry & Sushi Cafe	Sake!
Phinney Ridge / Greenwood	2/2019	Oliver's Twist		Y		6822 Greenwood Ave N, Seattle, WA 98103	http://www.oliverstwistseattle.com/	https://www.facebook.com/Olivers-Twist-40139511117/	https://www.instagram.com/oliverstwistseattle/	https://twitter.com/mrsowerberry	?	B/W/C/H	Cocktail Lounge	
Phinney Ridge / Greenwood	2/2019	Park Pub	Y	Y		6114 Phinney Ave N, Seattle, WA 98103	http://theparkpub.com/	https://www.facebook.com/The.Park.Pub	https://www.instagram.com/theparkpub/	-	N	B/W/C/H	Neighborhood Pub	
Phinney Ridge / Greenwood	2/2019	Prost!		Y		7311 Greenwood Ave N, Seattle, WA 98103	http://www.prosttavern.net/	https://www.facebook.com/ProstPhinneyRidge/	https://www.instagram.com/prostseattle/	https://twitter.com/ProstTavern	?	B/W/C/H	German Pub	
Phinney Ridge / Greenwood	2/2019	Ridge Pizza	Y	Y		7217 Greenwood Ave N, Seattle, WA 98103	http://www.ridgepizza.com/welcome-1#welcome	https://www.facebook.com/Ridge-Pizza-207327139298833/	https://www.instagram.com/ridgepizza/	https://twitter.com/RidgePizza	Y	B/W/C/H	Pizzeria	
Phinney Ridge / Greenwood	2/2019	Stage Door	Y	Y		208 N 85th St, Seattle, WA 98103	https://onecup.org/	https://www.facebook.com/StageDoorbyOneCup/	https://www.instagram.com/the_stage_door_/	-	Y	B/W/C	Coffee Shop	Roomy coffee shop with space for strollers.
Phinney Ridge / Greenwood	2/2019	Tangerine Thai		Y		5914 Phinney Ave N, Seattle, WA 98103	http://thetangerinethai.com/	https://www.facebook.com/TangerineThai	-	-	?	B/W	Thai Restaurant	
Phinney Ridge / Greenwood	2/2019	Teasome		Y		6412 Phinney Ave N, Seattle, WA 98103	https://www.teasomeseattle.com/	https://www.facebook.com/teasomepoke	https://www.instagram.com/teasome_seattle/	-	?	B/W/C	Asian Fusion Restaurant & Tea Room	
Phinney Ridge / Greenwood	2/2019	Whit's End	Y	Y		6510 Phinney Ave N, Seattle, WA 98103	https://www.thewhitsendbar.com/	https://www.facebook.com/thewhitsend/	https://www.instagram.com/thewhitsendbar/	-	N	B/W/C/H	Neighborhood Pub	
Phinney Ridge / Greenwood	2/2019	Yanni's Greek		Y		7419 Greenwood Ave N, Seattle, WA 98103	http://www.yannis-greek-restaurant.com/	https://www.facebook.com/Yannis-Greek-Restaurant-263479731373/	https://www.instagram.com/yannisgreekrestaurant/	-	?	B/W/C/H	Greek Restaurant	
Phinney Ridge / Greenwood	2/2019	Yard	Y	Y		8313 Greenwood Ave N, Seattle, WA 98103	http://www.theyardcafe.com/	https://www.facebook.com/theyardcafeseattle/	-	https://twitter.com/theyardcafe	Y	BWCH	Beer Bar with Mexican Food	
Phinney Ridge / Greenwood	2/2019	Zeeks Pizza	Y	Y		6000 Phinney Ave N, Seattle, WA 98103	http://zeekspizza.com/locations-hours/zeeks-pizza-phinney-ridge/	https://www.facebook.com/zeekspizza/	https://www.instagram.com/zeekspizza/	https://twitter.com/zeekspizza	Y	B/W/C	Pizzeria	
Phinney Ridge / Greenwood	2/2019	74th St Ale House		N	No Minors	7401 Greenwood Ave N, Seattle, WA 98103							Neighborhood Pub	
Phinney Ridge / Greenwood	2/2019	A la Mode Pies		N	No Alcohol	5821 Phinney Ave N, Seattle, WA 98103							Pie Shop	
Phinney Ridge / Greenwood	2/2019	Celine Patisserie		N	No Alcohol	6801 Greenwood Ave N #113A, Seattle, WA 98103							French Pastry Shop	
Phinney Ridge / Greenwood	2/2019	Chef Liao		N	No Alcohol	6012 Phinney Ave N, Seattle, WA 98103							Asian Fusion Restaurant	
Phinney Ridge / Greenwood	2/2019	Coolie Counter		N	No Alcohol	7415 Greenwood Ave N, Seattle, WA 98103							Vegan Ice Cream & Cookie Shop	
Phinney Ridge / Greenwood	2/2019	Fresh Flours		N	No Alcohol	6015 Phinney Ave N, Seattle, WA 98103							Pastery Bakery & Cafe	
Phinney Ridge / Greenwood	2/2019	Herkimer Coffee		N	No Alcohol	7320 Greenwood Ave N, Seattle, WA 98103							Coffee Shop	
Phinney Ridge / Greenwood	2/2019	Nutty Squirrel		N	No Alcohol	7212 Greenwood Ave N, Seattle, WA 98103							Gelato Shop	
Phinney Ridge / Greenwood	2/2019	Red Mill		N	No Alcohol	312 N 67th St, Seattle, WA 98103							Burger Joint	
Phinney Ridge / Greenwood	2/2019	Starbucks		N	No Alcohol	316 N 67th St, Seattle, WA 98103							Coffee Shop	
Phinney Ridge / Greenwood	2/2019	Sully's Snow Goose Saloon		N	No Minors	6119 Phinney Ave N, Seattle, WA 98103							Bar	
";

$data .= "
Pioneer Square	2/2019	Collins Pub	Y		526 2nd Ave, Seattle, WA 98104	https://collinspubseattle.com/	https://www.facebook.com/pages/The-Collins-Pub/113863558645438		N	BWCH	Neighborhood Pub
Ravenna	2/2019	Ravenna Brewing	Y		5408 26th Ave NE, Seattle, WA 98105	http://www.ravennabrewing.com/	https://www.facebook.com/RavennaBrewing/	https://www.instagram.com/ravennabrewing/	Yes	BC	Brewery Taproom with Food Truck	
Roosevelt	4/2019	The Westy	Y	Y	All Ages Until 10pm	1215 NE 65th St, Seattle, WA 98115	https://www.thewestyseattle.com/	https://www.facebook.com/TheWestySportsAndSpirits/	https://www.instagram.com/thewestyseattle	https://twitter.com/thewestyseattle	Y	BWCH	Sports Bar	Lots of TVs, back room has skee ball
Roosevelt	4/2019	Toronado		N	No Minors	1205 NE 65th St, Seattle, WA 98115								
Sand Point	2/2019											
SODO	2/2019	Seapine Brewery	Y		2959 Utah Ave S, Seattle, WA 98134	http://www.seapinebrewing.com/	https://www.facebook.com/SeapineBrewingCompany/	https://www.instagram.com/seapinebrewing/	?	B	Brewery Tasting Room	
South Lake Union	3/2019	Bar Harbor	Y		400 Fairview Ave N #105, Seattle, WA 98109	http://www.barharborbar.com/	https://www.facebook.com/barharborbar/	https://www.instagram.com/barharborseattle/	?	BWCH	Seafood & Cocktail Lounge	
University	2/2019	Floating Bridge Brewery		Y		722 NE 45th St, Seattle, WA 98105	http://www.floatingbridgebrewing.com/	https://www.facebook.com/floatingbridgebrewing	https://www.instagram.com/floatingbridgebrewing/		Y	B/C	Brewery Taproom with Sandwiches	Changing table is only in the Women's restroom
West Seattle	4/2019	The Westy		N	No Minors										
Waterfront	4/2019	The Crab Pot		Y		1301 Alaskan Way, Seattle, WA 98101	https://www.thecrabpotseattle.com/	https://www.facebook.com/minerslandingpier57/	https://www.instagram.com/thecrabpotrestaurant/	https://twitter.com/MinersLanding	Y	Y	BWCH	Seafood Restaurant	
Waterfront	4/2019	The Salmon Cooker	Y	Y		1301 Alaskan Way, Seattle, WA 98101	https://minerslanding.com/	https://www.facebook.com/minerslandingpier57/	https://www.instagram.com/minerslandingpier57/	https://twitter.com/MinersLanding	W Only	N	BW	Seafood Shack	
Waterfront	4/2019	The Fisherman's Restaurant & Bar		Y		1301 Alaskan Way, Seattle, WA 98101	http://www.thefishermansrestaurant.com/	https://www.facebook.com/fishermansseattle/	https://www.instagram.com/fishermansseattle/	https://twitter.com/MinersLanding	Y	Y	BWCH	Seafood Restaurant	
";

showData($alki);

function fetchAll() {
    $json = r('wp post list --format=json --post_type=place ');
    // print_r($json);
    $all = json_decode($json);
    return $all;
}

function showData($data) {
    $all = fetchAll();
    print_r($all);
    echo("\n* Loading data");
    $ls = explode("\n",trim($data));
    foreach($ls AS $l) {
        unset($id);
        $ps = explode("\t",$l);
        print_r($ps);
        $d['neighborhood'] = $ps['0'];
        $d['confirmed'] = $ps[1];
        $d['place'] = trim($ps[2]);
        $place = $d['place'];
        $d['picture'] = $ps[3];
        $d['content'] = $ps[14];
        foreach($all as $pl) {
            if($pl->post_title == $d['place']) {
                $id = $pl->ID;
                echo("\n* Found ".$place." with id ".$id);
                $action = 'update '.$id;
            } else {
                echo("\n* Creating new place for ".$place);
                $action = 'create';
            }
            $cmd = 'wp post '.$action.' --from-post="24" --post_title="'.$place.'" --post_author="jonah" --post_content="'.$d['content'].'"';
            $cmd .= '  --post_status="published" --post_type="place"   --tags_input="Alki"';
            echo("\n* ".$cmd);
        }
    }
    print_r($d);
}

function r($in) {
    echo("\n* ".$in);
    ob_start();
    passthru($in);
    return ob_get_clean();
}