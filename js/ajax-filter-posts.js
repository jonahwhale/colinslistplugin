jQuery(document).ready(function($) {
  
  window.onhashchange = function(){
    hash_params = parseTheHash();
    // Update the UI with the correct state elements
    updateTheUI ( hash_params[0], hash_params[1], hash_params[2] );
    // If the post type or topic is empty, fill each with available options
    if(hash_params[0].length == 0){
      hash_params[0] = getFilters( 'post-type' );
    }
    if(hash_params[1].length == 0){
      hash_params[1] = getFilters( 'topic' );
    }
    makeTheAjaxCall ( hash_params[0], hash_params[1], hash_params[2], hash_params[3] );
  }
  
  // A filter in the left navigation changed
  $('.sidebar').on('change', 'input[type="checkbox"]', function(event) {
    // Update the hash
    var hash = document.location.hash,
      hash_key = event.target.className,
      hash_value = event.target.id,
      ischecked = event.target.checked;
    query = updateTheHash( hash, hash_key, hash_value, ischecked);
    document.location.hash = query;
  });
	
	// "Clear Filter" in left navigation was clicked, so clear the filters
  $('.sidebar .filters-clear').on('click', 'a.subtleBtn', function(event) {
    // Prevent defualt action - opening tag page
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
    // Clear the checkboxes
    document.location.hash = '/';
  });
  
	// Reorder tab at the top of the result set was clicked, reorder things
  $('.menu.list-nav').on('click', 'a', function (event) {
    // Prevent defualt action - opening tag page
		if (event.preventDefault) {
			event.preventDefault();
		} else {
			event.returnValue = false;
		}
		// Get the order
		order = $(this).attr('href');
		// Pass the order
		query = updateTheHash ( document.location.hash, 'order', order, false );
		document.location.hash = query;
    
  });
  
	// Pagination at the bottom of the result set, paginate
  $(document).on('click', '.page .pagination a', function(event) {
    // Prevent default action - opening tag page
  	if (event.preventDefault) {
  		event.preventDefault();
  	} else {
  		event.returnValue = false;
  	}
  	paged = event.target.text;
		// Update the hash
		query = updateTheHash ( document.location.hash, 'paged', paged, false );
		document.location.hash = query;
		$('html, body').animate({ scrollTop: 500 }, 'slow');
  });
  
  function updateTheUI( post_types, topics, order ) {
    // Check the post types checkboxes
    checkTheCheckboxes( post_types, 'post-type' );
    // Check the topics checkboxes
    checkTheCheckboxes( topics, 'topic' )
    // Make the proper order active
    switch (order) {
      case 'DESC':
        $('#desc').addClass('active');
        $('#asc').removeClass('active');
        break;
      default:
        $('#asc').addClass('active');
        $('#desc').removeClass('active');
    }
  }
  
  function parseTheHash() {
    // Returns an array of all settings from the hash
    the_hash = document.location.hash;
    // If post type is in the hash, get its values; otherwise set it to available options
    post_types = ( the_hash.indexOf('post-type=') != -1 ) ? _getHashParams(the_hash, 'post-type').replace('post-type=', '').split('&') : [];
    // If topic is in the hash, get its values; otherwise set it to available options
    topics = (the_hash.indexOf('topic=') != -1 ) ? _getHashParams(the_hash, 'topic').replace('topic=', '').split('&') : [];
    // Get the order from the hash, if it's not there set to the default (desc)
    order = (the_hash.indexOf('order=ASC') == -1) ? 'DESC' : 'ASC';
    // Get the page from the hash, if it's not there set to the default (1)
    paged = (the_hash.indexOf('paged=') != -1) ? _getHashParams(the_hash, 'paged').replace('paged=', '') : 1;
    
    return [ post_types, topics, order, paged ];
  }
  
  function checkTheCheckboxes( checkedBoxes, filters ) {
    // Uncheck all check boxes
    $("input:checkbox"+"."+filters).each(function () {
      $(this).prop('checked', false);
    });
    // Then, check the checkboxes in the array
    for( i = 0; i < checkedBoxes.length; i++ ) {
       checkboxID = '#' + checkedBoxes[i];
      $(checkboxID).attr('checked', true);
    }
  }
  
  function getFilters( filter_domain ) {
    // Returns an array of the selected or available options by filter domain
    var filters = [];
    // Return the checked filters
    $('input:checkbox.' + filter_domain).each(function () {
       if(this.checked) {
         filters.push( $(this).val() );
       }
    });
    // If none are checked, return all filter options
    if (filters.length <= 0) {
      $('input:checkbox.' + filter_domain).each(function () {
         if(!this.checked) {
           filters.push( $(this).val() );
         }
      });
    }
    return filters;
  }
  
  function updateTheHash( hash, hash_key, hash_value, ischecked) {
    // Change the hash according to the triggered event
    hashpaged = '';
    switch (hash_key) {
      case 'post-type':
        hashtypes     = ( ischecked ) ? _addHashParam() : _removeHashParam();
        hashtopics    = _getHashParams ( hash, 'topic' );
        hashorder     = _getHashParams ( hash, 'order' );
        break;
      case 'topic':
        hashtypes     = _getHashParams ( hash, 'post-type' );
        hashtopics    = ( ischecked ) ? _addHashParam() : _removeHashParam();
        hashorder     = _getHashParams ( hash, 'order' );
        break;
      case 'order':
        hashtypes     = _getHashParams ( hash, 'post-type' );
        hashtopics    = _getHashParams ( hash, 'topic' );
        hashorder     = hash_key + '=' + hash_value;
        break;
      case 'paged':
        hashtypes     = _getHashParams ( hash, 'post-type' );
        hashtopics    = _getHashParams ( hash, 'topic' );
        hashorder     = _getHashParams ( hash, 'order' );
        hashpaged     = hash_key + '=' + hash_value;
        break;
      default:
        hashtypes     = _getHashParams ( hash, 'post-type' );
        hashtopics    = _getHashParams ( hash, 'topic' );
        hashorder     = _getHashParams ( hash, 'order' );
    }
    
    if(hashtypes != '') {
      hashtypes += '/';
    }
    if(hashtopics != '') {
      hashtopics += '/';
    }
    if(hashorder != '') {
      hashorder += '/';
    }
    if( hashpaged != '') {
      hashpaged += '/';
    }
    // Normalize the hash to start and end with a slash
    new_hash = '/' + hashtypes + hashtopics + hashorder + hashpaged;
    
    return new_hash;
    
    // Helper functions for adding and removing hash elements
    function _addHashParam ( ) {
      // Add the given hash value to the hash key
      hash_params = '';
      hash_params = _getHashParams (hash, hash_key);
      // If the key already exists, append an ampersand and the hash value
      if ( hash_params != '' ) {
        new_hash_params = hash_params + '&' + hash_value;
      // Otherwise, add the new key and value
      } else {
        new_hash_params = hash_key + '=' + hash_value;
      }
      return new_hash_params;
    }
    
    function _removeHashParam() {
      // Remove the given hash value from hash key
      hash_params = '';
      hash_params = _getHashParams(hash, hash_key);
      // If the parameter is the first and only parameter, remove the whole key
      if( hash_params.indexOf('=' + hash_value) != -1 && hash_params.indexOf('&') == -1 ){
        new_hash_params = '';
      // If the filter is first with others following, replace it and the following ampersand with an empty string
      } else if ( hash_params.indexOf('=' + hash_value + '&') != -1 ) {
        new_hash_params = hash_params.replace( hash_value + '&', '');
      // If the filter is middle of the pack or at the end, replace it and the preceding ampersand with an empty string
      } else if ( hash_params.indexOf('&' + hash_value) != -1 ) {
       new_hash_params = new_hash_params.replace( '&' + hash_value, '');
      }
      return new_hash_params;
    }
  }
  
  function _getHashParams( hash, hash_key ) {
    // If the hash key doesn't exist, return an empty string
    if ( hash.indexOf( hash_key ) == -1 ) {
      return '';
    }
    // The hash key does exist, return it's value 
    hash_array = hash.split("/");
    for( i = 0; i < hash_array.length; i++ ) {
      if( hash_array[i].indexOf( hash_key ) != -1) {
        hash_params = hash_array[i];
      }
    }
    return hash_params;
  }
    
  function makeTheAjaxCall ( posttypes, topics, order, paged ) {
    // Let the user know we are changing the data
    data = {
  		'action': 'filter_posts',
  		'afp_nonce': afp_vars.afp_nonce,
  		'posttypes': posttypes,
  		'topics': topics,
  		'order': order,
  		'paged': paged
  	};
  	$('.tagged-posts').fadeOut();
  	$.post( afp_vars.afp_ajax_url, data, function(response) {
      if (response) {
        $('.tagged-posts').html( response );
        $('.tagged-posts').fadeIn();
      } else {
        $('.tagged-posts').html('Whoops! Well, that is embarrassing.');
      }
    });
  }
});