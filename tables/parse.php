<?php
/**
 * @version 1.0 Jonah B  4/18/19 3:17 PM
 */
require_once 'vendor/autoload.php';
use iamcal\SQLParser;

$tables = ['places'];

foreach($tables AS $table) {
    parse_table($table);

}

function parse_table($table) {
    $parser = new SQLParser();
    $sql = file_get_contents(dirname(__FILE__)."/".$table.".sql");
    if(!$sql) return false;
    echo("\n* ".$sql."\n");
    $parser->parse($sql);
    print_r($parser->tables[$table]['fields']);

    foreach($parser->tables[$table]['fields'] AS $field) {
        $fields[$field['name']] = $field;
    }

    $json = json_encode(
        $fields,
        JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE
    );


    file_put_contents(dirname(__FILE__)."/".$table.".json",$json);
}
