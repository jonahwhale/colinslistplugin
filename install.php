<?php
$from = '~/wf-scripts/site/plugin/';
$classes = ['Venue','Neighborhood','Drinks'];

r('mkdir model');
foreach($classes as $name) {
    r('cp '.$from.'model/Country.class.php model/'.$name.'.class.php');
}

r('mkdir dbos');
r('cp -r '.$from.'dbos/ dbos/');



r('cp '.$from.'/composer.json composer.json');
r('composer install');

r('cp '.$from.'/deploy.php ./.');
r('mkdir tables');
r('cp ~/wf-scripts/site/plugin/tables/parse.php tables/');

r('mkdir features');
r('cp -r '.$from.'features/ features/');
r('git init');
r('git add ./.');

function r($in) {
    echo("\n* ".$in);
    ob_start();
    passthru($in);
    return ob_get_clean();
}