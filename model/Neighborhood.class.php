<?php
/**
 * @version 1.0 Jonah B  4/7/19 7:11 PM
 */
 

/**
* Unit Test:
* @license GPL
* @version $Id: $
*
*/

/**
* Useful inherited ACTable methods:
*  adminPage - GUI for listing and editing records in the table
*  _adminShowCellValue($field,$value);
*  adminPageTopExtend - Add links to admin page navigation
*
* @version 1.0 JB
*/
class CountryTable extends DBOS {
    var $_table = "country";
    var $_idField = "id";
    var $_titleField = "name";
    var $_dateField;
    var $_controller = "/wp-admin/admin.php?page=wf_countries_admin";
    var $_gridEdit = true;
    var $_wpTable = false;
    var $_allowBulkEdit = false;
    var $_allowDelete = false;
    var $_hideViewLink = true;
    
    
    public $id;
    public $name;
    public $country_group_id;
    
    /**
    * @version 1.0 JB
    */
    function __construct() {
        global $wpdb;
        
        $this->loadTable();
        $this->_wpTable = $wpdb->prefix.$this->_table;
        // $this->_table = $wpdb->prefix.$this->_table;
    }
    
    public function fetchAll($sql = false) {
        global $wpdb;
        if(!$sql) {
            $sql = "
                SELECT c.*, cg.item_id 
                FROM {$wpdb->prefix}country c 
                INNER JOIN {$wpdb->prefix}country_group cg on (c.country_group_id = cg.id)
                ORDER BY name 
            ";
        }
        $r = $wpdb->get_results($sql);
        return $r;
    }
    
    public function adminPage($action=false) {
        if($action=='list_countries') {
            $action = 'list';
        }
        $action = 'list';
        parent::adminPage($action);
    }
    
    public function loadData() {
        global $wpdb;
        $sql = file_get_contents(dirname(__FILE__)."/../tables/country_group.data.sql");
        $sql = str_replace('`country_group`','`'.$wpdb->prefix.'country_group`',$sql);
        $this->da($sql);
        $wpdb->query($sql) or die('Could not load country_group data');
        
        $countries = "
Austria
Belgium
Canada
Denmark
Finland
France 
Germany
Iceland
Italy
Japan
Netherlands
Norway
Spain
Sweden
Switzerland
UAE
United Kingdom
United States        
        ";
        $this->loadGroup($countries,1);
        
        $countries = "
Australia
Belarus
Brazil
Brunei
Cape Verde
Costa Rica
Hong Kong
India
Ireland
Israel
Kuwait
Mexico
New Zealand
Nigeria
Oman
Qatar
Saudi Arabia
Singapore
South Korea
Taiwan
Ukraine
        ";
        $this->loadGroup($countries,2);
        
        $countries = "
Afghanistan
Albania
Argentina
Armenia
Azerbaijan
Bangladesh
Belize
Benin
Bhutan
Bolivia
Botswana
Burkina Faso
Cambodia
Chile
China
Colombia
Congo
Cote d'Ivoire
Croatia
Czech Republic
Dominica
Dominican Republic
East Timor
Ecuador
Egypt
El Salvador
Estonia
Ethiopia
Fiji
Gambia
Georgia
Ghana
Greece
Guatemala
Haiti
Honduras
Hungary
Indonesia
Iran
Iraq
Jordan
Kazakhstan
Kenya
Kiribati
Kosovo
Kyrgyzstan
Laos
Lebanon
Lesotho
Liberia
Lithuania
Macao
Macedonia
Madagascar
Malawi
Malaysia
Maldives
Mali
Mauritius
Moldova
Mongolia
Montenegro
Mozambique
Myanmar
Namibia
Nepal
Nicaragua
Pakistan
Palestine
Panama
Papua New Guinea
Paraguay
Peru
Philippines
Poland
Portugal
Puerto Rico
Romania
Russia
Rwanda
Senegal
Serbia
Sierra Leone
Slovenia
Solomon Islands
Somolia
South Africa
Sri Lanka
Swaziland
Tajikistan
Tanzania
Thailand
Tonga
Trinidad and Tobago
Turkey
Tuvalu
Uganda
Uruguay
Uzbekistan
Vanuatu
Vietnam
Zambia
Zimbabwe
        ";
        $this->loadGroup($countries,3);
    }
    
    function loadGroup($countries,$country_group_id) {
        global $wpdb;
        $ls = explode("\n",$countries);
        
        foreach($ls AS $name) {
            $name = trim($name);
            if($name) {
                $c = new Country('new');
                $c->name  = $name;
                $c->country_group_id = $country_group_id;
                $sql = 'REPLACE INTO wff_country SET ';
                
                $table = $wpdb->prefix.$this->_table;
                $data = array('name' => $name, 'country_group_id' => $country_group_id);
                $format = array('%s','%d');
                /*
                $wpdb->insert($table,$data,$format) or die($wpdb->last_query);
                $id = $wpdb->insert_id;
                */
                $this->wpReplaceInto($data);
                // $c->save();
            }
        }
    }
    
    function wpReplaceInto($fs) {
        global $wpdb;
        foreach($fs AS $f=>$v) {
            $names[] = $f;
            $formats[] = '%s';
            $values[] = $v;
        }
        
        $sql = "REPLACE INTO {$this->_wpTable} (".implode(',',$names).") VALUES (".implode(',',$formats).")";
        // var_dump($sql); // debug
        $sql = $wpdb->prepare($sql,$values[0],$values[1]);
        // var_dump($sql); // debug
        $wpdb->query($sql);
    }
}

/**
* Useful inherited DBOS methods:
*   save(), delete(), showForm(), ajaxField('field_name'), formField('fieldname');
* @version 1.0 JB
*/
class Country extends CountryTable {

    /**
    * @version 1.0 JB
    */
    function __construct($id) {
        if($id=='new') $id = false;
        $this->loadRecord($id);
    }

}

