<?php
/**
 *
 * dark: #283547
 * turquoise: #59b5c2
 * @version 1.0 Jonah B  4/7/19 7:11 PM
 */


/**
 *
 */
class PlaceTable  {
    var $_table = "places";
    var $_idField = "id";
    var $_titleField = "name";
    var $_dateField;
    var $_controller = "/wp-admin/admin.php?page=colins_list_admin";
    var $_gridEdit = true;
    var $_wpTable = false;
    var $_allowBulkEdit = false;
    var $_allowDelete = false;
    var $_hideViewLink = true;
    var $_useFieldCache = false;
    public $id;
    /**
     * varchar 300
     */
    public $name;
    /**
     * @date
     */
    public $confirmed;

    /**
     * @related
     */
    public $_pod = false;
    public $neighborhood_id;
    public $neighborhood;
    public $distance = false;
    public $lat_lng;
    public $lat;
    public $lng;
    public $drinks = null;
    /**
     * @type boolean
     */
     public $colins_list;
     /*
    Why Not / Caveat
    Address
    Website
    Facebook
    Instagram
    Twitter
    Changing Table
    High Chairs
    Drinks
    Description
    Tip
        */

    /**
    * @version 1.0 JB
    */
    function __construct() {
        global $wpdb,$table_prefix;

        $this->_wpTable = $table_prefix.$this->_table;
    }

    public function fetchAll($sql = false) {
        global $wpdb;
        // Get the Pods object and run find()
        $params = array(
            'orderby' => '',
            'limit' => 1000,
            'where' => ''
            );

        $places = pods( 'place', $params );
        return $places;

    }

    public function getCache() {
        global $wpdb;
        $sql = "SELECT * FROM {$this->_wpTable}";

        $wpdb->prepare($sql);
        // var_dump($sql); // debug
        $res = $wpdb->get_results($sql);
        foreach($res AS $id=>$data) {
            $out[$id] = json_decode($data->json);
        }
        return $out;
    }

    public function putCache($places) {
        global $wpdb;
        $debug = false;
        // dump('hello putCache');
        foreach($places as $id => $place) {
            if($debug) dump($place);
            $json = json_encode($place);
            $sql = "REPLACE INTO {$this->_wpTable} (id,json) VALUES (%s,%s)";
            // var_dump($sql); // debug
            $sql = $wpdb->prepare($sql,$id,$json);
            // dump($sql);
            $wpdb->query($sql);
            if($debug) {
                if($wpdb->last_error !== '') :
                    $wpdb->print_error();
                endif;
            }
        }
    }

    public function adminPage($action=false) {
        if($action=='list_countries') {
            $action = 'list';
        }
        $action = 'list';
        $this->adminPage($action);
    }

    public function loadData() {
    }

    function wpReplaceInto($fs) {
        global $wpdb;
        foreach($fs AS $f=>$v) {
            $names[] = $f;
            $formats[] = '%s';
            $values[] = $v;
        }

        $sql = "REPLACE INTO {$this->_wpTable} (".implode(',',$names).") VALUES (".implode(',',$formats).")";
        // var_dump($sql); // debug
        $sql = $wpdb->prepare($sql,$values[0],$values[1]);
        // var_dump($sql); // debug
        $wpdb->query($sql);
    }

    function showCell($field,$place) {
        $field = strtolower($field);
        $field = str_replace(' ','_',$field);
        $field = str_replace("'",'',$field);
        // $this->dump($place);
        switch($field) {
            case"Place":
            case'place':
                return $place->data->post_title;
                break;
            case'drinks':
            case'Drinks':
                $this->showDrinks($place->data);
                break;
            default:
                return $place->data->{$field};
        }
    }

    /**
     * rgba(44, 62, 80, 0.9)
     */

    function showIcon($name) {
        switch($name) {
            case"cider":
            case"Cider":
                ?><span title="Serves Cider"
                ><svg style="margin-top:-8px;" class="fas icon icon-cider" width="20pt" height="20pt"
                    viewBox="-55 0 512 512.00001"
                    xmlns="http://www.w3.org/2000/svg">
                    <path d="m0 436.332031h232.175781v75.667969h-232.175781zm0 0"/>
                    <path d="m136.074219 303.347656c-1.835938-.070312-3.953125.132813-6.667969.640625-.546875.105469-1.484375.371094-2.394531.628907-2.175781.621093-4.886719 1.394531-8.015625 1.6875l-.691406.0625h-3.921876l-.691406-.0625c-3.128906-.292969-5.839844-1.066407-8.015625-1.6875-.90625-.257813-1.847656-.523438-2.394531-.628907-2.371094-.441406-4.289062-.65625-5.960938-.65625-.238281 0-.472656.003907-.707031.015625-5.253906.195313-7.816406 3.871094-9.039062 6.914063-4.652344 11.5625-1.769531 25.648437 7.523437 36.761719 2.933594 3.503906 6.089844 6.390624 9.386719 8.570312.496094.324219 1.46875.96875 2.214844 1.394531.578125-.128906 1.082031-.335937 1.382812-.484375l9.085938-7.144531 7.503906 7.175781c.304687.144532.78125.332032 1.316406.453125.746094-.425781 1.722657-1.070312 2.214844-1.398437 3.292969-2.179688 6.453125-5.0625 9.386719-8.566406 9.292968-11.113282 12.175781-25.199219 7.523437-36.761719-1.222656-3.042969-3.785156-6.71875-9.039062-6.914063zm0 0"/><path d="m58.601562 0h114.972657v62.625h-114.972657zm0 0"/><path d="m206.105469 169.429688-15.128907-21.558594c-11.382812-16.226563-17.402343-35.277344-17.402343-55.097656v-.148438h-114.972657v.148438c0 19.820312-6.019531 38.871093-17.402343 55.097656l-15.128907 21.558594c-8.253906 11.757812-14.601562 24.511718-18.980468 37.90625h217.996094c-4.378907-13.394532-10.730469-26.148438-18.980469-37.90625zm0 0"/><path d="m.761719 237.332031c-.492188 4.839844-.761719 9.714844-.761719 14.628907v154.371093h232.175781v-154.367187c0-4.914063-.269531-9.792969-.761719-14.628906h-230.652343zm159.839843 128.9375c-4.796874 5.734375-10.128906 10.558594-15.84375 14.34375-3.441406 2.277344-9.839843 6.507813-17.765624 6.507813-.019532 0-.042969 0-.0625 0-4.03125-.011719-7.625-.75-10.585938-1.695313-2.960938.945313-6.554688 1.683594-10.582031 1.695313-.023438 0-.042969 0-.066407 0-7.925781 0-14.320312-4.230469-17.761718-6.507813-5.71875-3.78125-11.050782-8.609375-15.847656-14.34375-16.398438-19.605469-21.132813-45.355469-12.347657-67.199219 6.132813-15.253906 19.832031-25.101562 35.746094-25.703124 1.828125-.066407 3.679687-.027344 5.601563.109374v-17.476562h30v17.511719c2.109374-.171875 4.128906-.21875 6.117187-.144531 15.914063.601562 29.609375 10.449218 35.746094 25.703124 8.785156 21.839844 4.050781 47.589844-12.347657 67.199219zm0 0"/><path d="m259.113281 316.082031c0 34.023438 24.050781 62.523438 56.039063 69.429688v96.488281h-22v30h74v-30h-22v-96.488281c31.992187-6.90625 56.039062-35.40625 56.039062-69.429688v-61.417969h-142.078125zm0 0"/><path d="m259.113281 163.332031h142.078125v61.332031h-142.078125zm0 0"/>
                </svg></span>&nbsp;
                <?php
                break;
            case"Beer":
            case"beer":
                ?><span  title="Serves Beer"><i class="fas fa-beer fa-lg"></i></span>&nbsp;
                <?php
                break;
            case"Wine":
            case"wine":
                ?><span href="?filter=wine"><i class="fas fa-wine-glass-alt fa-lg" title="Serves Wine"></i></span>&nbsp;
                <?php
                break;
            case"Spirits":
            case"spirits":
            case"cocktails":
            case"Cocktails":
                ?><span href="?filter=cocktails"><i class="fas fa-cocktail fa-lg"
                    title="Serves Cocktails/Hard Alcohol"></i></span>&nbsp;<?php
                break;
        }
    }


    function dump($in=false) {
        /*
        ?><pre><?php
        print_r($in);
        ?></pre><?php
        */
        unset($this->_pod);
        dump($this);

    }
}

/**
* Useful inherited DBOS methods:
*   save(), delete(), showForm(), ajaxField('field_name'), formField('fieldname');
* @version 1.0 JB
*/
class Place extends PlaceTable {

    public $data = false;
    public $name = false;

    /**
    * @version 1.0 JB
    */
    function __construct($data) {
        if(is_string($data)) {
            $this->name = $data;
            unset($data);
        } elseif (is_numeric($data)){
            $this->id = $data;
            unset($data);
            $p = pods('place',$this->id);
            $this->data = (object)$p->data->row;

        } else {
            $this->data = $data;
            $this->name = $data->post_title;
        }
        $this->id = $this->getID();
        $this->neighborhood = $this->getNeighborhood();
        $this->neighborhood_id = $this->getNeighborhoodID();
        $this->colins_list = $this->getColinsList();
        $this->why_not_caveat = $this->getWhyNotCaveat();
        $this->lat_lng = $this->getLatLng();
        $this->data->lat_lng = $this->lat_lng;
        $this->lat = $this->getLat();
        $this->lng = $this->getLng();
    }
    function getLat() {

    }

    function getLng() {

    }
    function getTitle() {
        return $this->data->post_title;
    }

    function getName() {
        return $this->data->post_title;
    }

    function getId() {
        return $this->data->ID;
    }

    function getNeighborhood() {
        $cache_key = 'neighborhood_title_place_'.$this->getId();
        if(!$this->neighborhood) {
            // $this->neighborhood = wp_cache_get($cache_key);
        }
        if(!$this->neighborhood) {
            $p = $this->getPod();
            $hood = $p->field( 'neighborhood' );
            $this->neighborhood = $hood['post_title'];
            // wp_cache_set($cache_key,$this->neighborhood);
        }

        return $this->neighborhood;

    }
/*

$result = wp_cache_get( 'my_result' );
if ( false === $result ) {
	$result = $wpdb->get_results( $query );
	wp_cache_set( 'my_result', $result );
}
// Do something with $result;
*/
    function getPod() {

        if(!$this->_pod) {
            $this->_pod = pods('place',$this->getId());

        }
        return $this->_pod;
    }

    function _cache($key,$val) {
        wp_cache_set($cache_key,$this->neighborhood_id);
    }

    function getLatLng() {
        $cache_key = 'lat_lng_place_'.$this->getId();

        // if(!$this->lat_lng) { $this->lat_lng = wp_cache_get($cache_key); }
        if(!$this->lat_lng) {
            $this->lat_lng = get_geocode_latlng($this->getId());
            // wp_cache_set($cache_key,$this->lat_lng);
        }

        return $this->lat_lng;
    }

    function getNeighborhoodID() {
        $cache_key = 'neighborhood_id_place_'.$this->getId();
        // if(!$this->neighborhood) { $this->neighborhood_id = wp_cache_get($cache_key); }
        if(!$this->neighborhood_id) {

            $p = $this->getPod();
            $hood = $p->field( 'neighborhood' );
            $this->neighborhood_id = $hood['ID'];

            // wp_cache_set($cache_key,$this->neighborhood_id);
        }
        // da($hood);
        return $this->neighborhood_id;
    }


    function showNeighborhood() {
        ?><div><?= $this->getNeighborhood(); ?></div><?php
    }

    function getAddress() {
        return $this->data->address;
    }

    function getDescription() {

        if($this->data->post_content) {
            return $this->data->post_content;
        } else {
            return $this->data->post_excerpt;
        }
    }

    function showTip() {
        $out = $this->data->tip;
        $out = trim($out);

        // print_r($out);
        $out = str_replace("\n\n","\n",$out);
        $out = str_replace("\n","<br />",$out);
        // dump($out);

        if($out) {
            ?><div class="tip"><?= $out; ?></div><?php
        }
    }

    function showDescription() {
        ?><p class="description"><?= $this->getDescription(); ?></p><?php
    }

    /**
     *
     */
    function showWhyNot() {
        $out = $this->data->why_not_caveat;
        $out = trim($out);
        $out = str_replace("\n","<br />",$out);
        // dump($out);


        ?><span class="whyNotCaveat"><?= $out ?></span><?php
    }

    function showWebsite() {
        if($this->data->website) {
            ?><a href="<?= $this->data->website ?>"
                target="_new"><i class="fas fa-globe fa-lg"></i></a><?php
        }
    }

    function showFacebook() {
        if($this->data->facebook) {
            ?><a href="<?= strip_tags($this->data->facebook) ?>" target="_new"
                ><i class="fab fa-facebook fa-lg"></i></a><?php
        }
    }

    function showInstagram() {
        if($this->data->instagram) {
            ?><a href="<?= strip_tags($this->data->instagram) ?>" target="_new"
                ><i class="fab fa-instagram fa-lg"></i></a><?php
        }
    }

    function showTwitter() {
        $twitter = $this->data->twitter;
        $twitter = str_replace('-','',$twitter);
        $twitter = trim($twitter);
        if($twitter) {
            ?><a href="<?= strip_tags($this->data->twitter) ?>"
                target="_new"
                ><i class="fab fa-twitter fa-lg"></i></a><?php
        }
    }

    function showColinsList() {
        $size = '50';
        $style = 'max-width:50px;';


        if(isset($this->data->colins_list) && $this->data->colins_list) {
            if(isset($this->data->why_not_caveat) && $this->data->why_not_caveat) {

                ?><img title="On the list with caveat"
                      width='<?= $size ?>px'
                      height='<?= $size ?>px'
                      style="<?= $style ?>"
                      src="/wp-content/plugins/colinslistplugin/img/checkbox-yellow.png?v=1"
                /><?php
            } else {
              ?>
              <img
                  title="On the list"
                  width='<?= $size ?>px'
                  height='<?= $size ?>px'
                  style="<?= $style ?>"
                  src="/wp-content/plugins/colinslistplugin/img/colins-list-yes-small.png?v=1"
              /><?php
          }
        } else {
            ?><img title="Not on the list" width='<?= $size ?>px' height='<?= $size ?>px' style="<?= $style ?>" src="/wp-content/plugins/colinslistplugin/img/colins-list-no-small.png?v=1" /><?php
        }
    }

    function getPicture() {
        return get_the_post_thumbnail_url( $this->getId(), 'medium' );
    }

    function showPicture() {
        echo get_the_post_thumbnail( $this->getId(), 'medium' );
    }

    function hasDrink($drink) {

    }

    function getDrinks() {
        // $cache_key = 'drinks_place_'.$this->getId();
        // if(!$this->drinks) { $this->drinks = wp_cache_get($cache_key); }

        if($this->drinks==null) {
            $p = $this->getPod();
            $this->drinks = $p->field( 'drinks' );
            // wp_cache_set($cache_key,$this->drinks);
        }
        return $this->drinks;
    }

    function showDrinks() {
        $drinks = $this->getDrinks();
        if(is_array($drinks)) {
            foreach($drinks AS $drink) {
                $this->showIcon($drink);
            }
        }
    }

    /**
     * changing_table
     * high_chairs
     */

    function showEquipment() {
        if($this->data->high_chairs) { ?>
            <span  title="Has Highchairs">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              style="margin-top:-8px;"
              class="icon icon-high-chair"
              width="22pt"
              height="22pt"
              version="1.1"
              viewBox="-169 0 800 800"
              title="Has highchairs" >
              <path fill="white" d="M 400.746094 270.710938 L 400.746094 184.613281 L 439.210938 184.613281 C 451.953125 184.613281 462.285156 174.289062 462.285156 161.53125 C 462.285156 148.789062 451.953125 138.46875 439.210938 138.46875 L 161.207031 138.46875 L 152.476562 67.226562 C 147.785156 28.902344 115.128906 0 76.523438 0 C 54.621094 0 33.730469 9.40625 19.21875 25.804688 C 4.703125 42.207031 -2.09375 64.082031 0.566406 85.828125 L 33.628906 355.855469 C 37.4375 386.972656 59.691406 411.875 88.589844 420.140625 L 24.15625 770.941406 C 22.835938 778.144531 24.765625 785.507812 29.457031 791.136719 C 34.144531 796.773438 41.046875 800 48.371094 800 L 129.859375 800 C 141.75 800 151.929688 791.507812 154.066406 779.835938 L 175.792969 661.53125 L 286.480469 661.53125 L 308.207031 779.824219 C 310.351562 791.515625 320.53125 800 332.421875 800 L 413.90625 800 C 421.230469 800 428.117188 796.773438 432.8125 791.152344 C 437.503906 785.523438 439.4375 778.164062 438.117188 770.9375 L 374.222656 423.078125 L 385.757812 423.078125 C 427.960938 423.078125 462.285156 388.75 462.285156 346.554688 L 462.285156 345.75 C 462.285156 308.6875 435.792969 277.695312 400.746094 270.710938
              Z" />
              <path
                  stroke="black"
                  stroke-width="10"
                  d="
               M 354.59375 184.613281 L 354.59375 269.234375 L 177.210938 269.234375 L 166.851562 184.613281 Z M 388.050781 753.847656 L 350.367188 753.847656 L 328.402344 634.289062 C 326.390625 623.339844 316.847656 615.386719 305.703125 615.386719 L 156.574219 615.386719 C 145.433594 615.386719 135.890625 623.339844 133.871094 634.289062 L 111.914062 753.847656 L 74.222656 753.847656 L 134.976562 423.078125 L 172.667969 423.078125 L 149.414062 549.679688 C 148.179688 556.414062 150 563.355469 154.378906 568.613281 C 158.769531 573.875 165.265625 576.921875 172.109375 576.921875 L 290.167969 576.921875 C 297.011719 576.921875 303.507812 573.875 307.890625 568.613281 C 312.28125 563.355469 314.097656 556.414062 312.855469 549.679688 L 289.609375 423.078125 L 327.300781 423.078125 Z M 242.6875 423.078125 L 262.460938 530.765625 L 199.816406 530.765625 L 219.589844 423.078125 Z M 416.128906 346.554688 C 416.128906 363.300781 402.503906 376.921875 385.757812 376.921875 L 109.585938 376.921875 C 94.261719 376.921875 81.304688 365.453125 79.445312 350.242188 L 46.371094 80.214844 C 45.304688 71.460938 47.933594 63 53.773438 56.398438 C 59.625 49.796875 67.699219 46.152344 76.519531 46.152344 C 91.847656 46.152344 104.800781 57.621094 106.671875 72.832031 L 133.886719 295.113281 C 135.304688 306.683594 145.128906 315.386719 156.792969 315.386719 L 385.757812 315.386719 C 402.503906 315.386719 416.128906 329.007812 416.128906 345.757812 Z
               M 416.128906 346.554688 "
              />

            </svg>
            </span>
            <?php
        }
        if($this->data->changing_table) {
            ?>
            <span title="Has Changing Table">
            <svg

                style="margin-top:-8px;"
                class="icon icon-changing-table"
                height="20pt"
                width="20pt"
                version="1.1"
                viewBox="0 0 364.236 400.227"
                xml:space="preserve" xmlns="http://www.w3.org/2000/svg"
                xmlns:xlink="http://www.w3.org/1999/xlink">
                <path d="M364.229,239.664v39.355H111.98v-39.355H364.229z M340.064,224.359c13.359,0,24.172-10.773,24.172-24.188  S353.424,176,340.064,176c-13.373,0-24.193,10.758-24.193,24.172S326.691,224.359,340.064,224.359z M307.676,195.695  c-0.676-3.367-12.184-42.051-12.184-42.051c-1.68-5.801-7.746-9.137-13.543-7.457c-5.801,1.68-9.137,7.742-7.457,13.543l7.434,25.66  h-29.965l-11.734-40.5h-42.879c-7.852,0-14.211,6.359-14.211,14.211s6.359,14.211,14.211,14.211c0,0,11.98,0,21.523,0  c3.586,12.383,10.348,35.711,10.348,35.711c2.277,7.836,7.613,15.727,19.664,15.727h41.59  C302.57,224.75,311.074,212.648,307.676,195.695z M178.855,96.477c26.652,0,48.227-21.492,48.227-48.25  C227.082,21.461,205.508,0,178.855,0c-26.668,0-48.258,21.461-48.258,48.227C130.598,74.984,152.188,96.477,178.855,96.477z   M192.871,185.203h-41.57v-34.367c0-30.184-10.039-42.574-18.945-51.484c-23.387-23.383-48.777-13.406-53.223-8.953  c-11.773,11.77-38.289,38.281-51.629,51.629C10.047,159.477,0,179,0,217.895s0,149.617,0,149.617  c0,18.07,14.648,32.715,32.711,32.715c18.066,0,32.715-14.645,32.715-32.715V227.816l46.629-46.625v43.375l80.816-0.004  c10.867,0,19.676-8.813,19.676-19.68S203.738,185.203,192.871,185.203z"/>
                </svg>
            </span>
            <?php
        }
    }

    function setDistance($d) {
        $d = str_replace('mi','',$d);
        $d = trim($d);
        $this->distance = $d;
    }

    function showDistance() {
        if($this->data->distance) {
            ?><nobr><?= $this->data->distance ?> mi</nobr><?php
        }
    }

    function showAddress() {
        $this->include('placeAddress.php');
    }

    /**
     *
     */
    public function include($file) {
        require(dirname(__FILE__)."/../../../themes/colinslisttheme/place/".$file);
    }

    function showSingle() {
        $this->showColinsList();
        $this->showPicture();

        $this->showNeighborhood();
        ?><br /><?php
        $this->showDescription();
        $this->showDrinks();
        $this->showAddress();

    }

    function getColinsList() {
        $this->colins_list = $this->data->colins_list;
        return $this->colins_list;
    }

    function getWhyNotCaveat() {
        $this->why_not_caveat = $this->data->why_not_caveat;
        return $this->why_not_caveat;
    }


}
