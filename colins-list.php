<?php
/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              colinslist.drinks
 * @since             1.0.0
 * @package           Colins_List
 *
 * @wordpress-plugin
 * Plugin Name:       Colin's List
 * Plugin URI:        https://bitbucket.org/jonahwhale/colinslistplugin/
 * Description:       Colins List Plugin
 * Version:           1.1.0
 * Author:            Jonah Baker
 * Author URI:        https://bitbucket.org/jonahwhale/colinslistplugin/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       colins-list
 * Domain Path:       /languages
 * Bitbucket Plugin URI: https://bitbucket.org/jonahwhale/colinslistplugin/
 */

require_once('vendor/autoload.php');

use Ivory\GoogleMap\Helper\Builder\ApiHelperBuilder;
use Ivory\GoogleMap\Helper\Builder\MapHelperBuilder;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Service\Base\Location\AddressLocation;
use Ivory\GoogleMap\Service\DistanceMatrix\Request\DistanceMatrixRequest;
use Ivory\GoogleMap\Service\DistanceMatrix\DistanceMatrixService;
use Ivory\GoogleMap\Service\Serializer\SerializerBuilder;
use Http\Adapter\Guzzle6\Client;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Ivory\Serializer\Format;

use Geokit\Position;

use Geodistance\Location;
use function Geodistance\miles;



// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'COLINS_LIST_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-colins-list-activator.php
 */
function activate_colins_list() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-colins-list-activator.php';
		Colins_List_Activator::activate();
        // wp db import wp-content/plugins/colinslistplugin/latest.sql
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-colins-list-deactivator.php
 */
function deactivate_colins_list() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-colins-list-deactivator.php';
		Colins_List_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_colins_list' );
register_deactivation_hook( __FILE__, 'deactivate_colins_list' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-colins-list.php';
require plugin_dir_path( __FILE__ ) . 'features/place.php';
require plugin_dir_path( __FILE__ ) . 'features/neighborhood.php';
require plugin_dir_path( __FILE__ ) . 'features/admin.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_colins_list() {

	$plugin = new Colins_List();
	$plugin->run();

}
run_colins_list();

function dump($in){
		?><pre><?= print_r($in) ?></pre><?php

}

/**
 * https://github.com/0x13a/geodistance-php
 */
function getDistance($a,$b) {
    $debug = false;
    if(!isset($a->lat)) {
        if($debug) { dump('a not set'); }
        return false;
    }
    // dump($b);
    if($b && is_string($b)) {
        $b = str_replace('(','',$b);
        $b = str_replace(')','',$b);
        $ps = explode(',',$b);
        $to = new stdClass();
        $to->lat = $ps[0];
        $to->lng = $ps[1];
        if($debug) dump($to);
    } else {
        if($debug) dump($b);

    }
    if(!is_object($to)) {
        if($debug) {
            echo "<br />No address for:";
            dump($b);
        }
        return false;
    }
    if($debug) { dump($a); }
    $from     = new Location($a->lat, $a->lng);
    $to       = new Location($to->lat, $to->lng);
    $decimal_precision = 1;
    $miles =  miles($from, $to, $decimal_precision);
    return $miles;
    /*
    $math = new Geokit\Math();
    $a = new Geokit\Position(8.50207515, 49.50042565);
    $b = new Geokit\Position(8.50207515, 50.50042565);
    $distance = $math->distanceHaversine($a, $b);
    $miles = $distance->miles();
    dump($miles);
    return $miles;
    */
}

function getPosition($in) {
    $debug = false;
    $in = trim($in);
    // dump($in);
    $url = 'https://maps.googleapis.com/maps/api/geocode/json?address='.urlencode($in).',Washington+USA&key=AIzaSyBOdQFutys6IGw4cLpGVn84D4zw2S902RU';

    // $url = 'https://maps.googleapis.com/maps/api/geocode/json?key=AIzaSyBOdQFutys6IGw4cLpGVn84D4zw2S902RU&components=postal_code:'.urlencode($in);
    // dump($url);
    $r = file_get_contents($url);
    $res = json_decode($r);
    // dump($res);
    if(isset($res->results[0]->geometry->location)) {
        return $res->results[0]->geometry->location;
    }


}

function getDistanceActual($a,$b) {
		$debug = false;
		if($debug) { echo '<br />Finding distance from '.$a.' to '.$b.'<br />'; }
		$raw = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.urlencode($a).'&destinations='.urlencode($b).'&key=AIzaSyBOdQFutys6IGw4cLpGVn84D4zw2S902RU');
		$res = json_decode($raw);
		// print_r($res);
		return $res;

	// $distanceMatrix = new DistanceMatrixService(new Client(), new GuzzleMessageFactory());
	/*
	$distanceMatrix = new DistanceMatrixService(
    new Client(),
    new GuzzleMessageFactory(),
    SerializerBuilder::create($psr6Pool)
);
 */
 	/*
		$response = $distanceMatrix->process(new DistanceMatrixRequest(
			[new AddressLocation($a)],
			[new AddressLocation($b)]
		));
		print_r($response);
		*/
}

function sortDistance($a, $b)
{
		// natsort
    return strnatcmp($a->distance, $b->distance);
}

function getVar($name) {
    if(isset($_REQUEST[$name])) {
        return strip_tags($_REQUEST[$name]);
    } elseif(isset($_SESSION[$name])) {
        return strip_tags($_SESSION[$name]);
    } else {
        return false;
    }
}
