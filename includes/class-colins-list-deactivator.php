<?php

/**
 * Fired during plugin deactivation
 *
 * @link       colinslist.drinks
 * @since      1.0.0
 *
 * @package    Colins_List
 * @subpackage Colins_List/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Colins_List
 * @subpackage Colins_List/includes
 * @author     Jonah Baker <jonahb0001@gmail.com>
 */
class Colins_List_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
