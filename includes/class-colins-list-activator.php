<?php
/**
 * Fired during plugin activation
 *
 * @link       colinslist.drinks
 * @since      1.0.1
 *
 * @package    Colins_List
 * @subpackage Colins_List/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Colins_List
 * @subpackage Colins_List/includes
 * @author     Jonah Baker <jonahb0001@gmail.com>
 */
class Colins_List_Activator
{

    /**
     * Short Description. (use period)
     *
     * Long Description.
     *
     * @since    1.0.0
     */
    public static function activate()
    {
        global $table_prefix, $wpdb;
        $debug = false;
        $tblname = 'places';
        $wp_track_table = $table_prefix . "$tblname ";

        #Check to see if the table exists already, if not, then create it
        // if($wpdb->get_var( "show tables like '$wp_track_table'" ) != $wp_track_table)
        $do = true;
        if ($do) {
            $sql = "CREATE TABLE `". $wp_track_table ."` ( ";
            $sql .= "  `id`  int(11)   NOT NULL, ";
            $sql .= "  `json`  TEXT   NOT NULL, ";
            $sql .= "  PRIMARY KEY `INDEX_ID` (`id`) ";
            $sql .= "); ";
            require_once(ABSPATH . '/wp-admin/includes/upgrade.php');
            dbDelta($sql);
            // dump($sql);
            // die();
        }
    }
}
