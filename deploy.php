<?php
/**
 *
 */
if(isset($argv[1])){
    $to = $argv[1];
} else {
    $to = false;
}

$p = new PutSFTP($to);
// $p->makeDir('acf-json/');

$p->putVersion($to);

echo("\n");

class PutSFTP {
    public $staging = '/colinslist/colin-s-list/wp-content/plugins/colinslistplugin/';
    public $live = '/colinslist/colin-s-list/wp-content/plugins/colinslistplugin/';

    public $filesChanged = [];
    public $sites = [
        'colin-s-list',

    ];
    public $stagingUrl = 'agr.flywheelstaging.com';

    public function __construct($to='staging') {
        if(!$to) $to = 'staging';
        $this->target = $this->{$to};
    }

    public function putVersion($to='staging') {

        $in = r('git status');
        // print_r($in);

        $in .= file_get_contents(dirname(__FILE__)."/version.txt");
        //
        $ls = explode("\n",$in);
        print_r($in);
        foreach($ls as $l){
            $files[] = $this->processLine($l);
        }
        foreach($this->filesChanged as $file) {
            $this->putFile($file);
        }

        echo("\n\nsftp jonahb@sftp.flywheelsites.com");
    }

    public function processLine($l) {
        if(strpos($l,'new file:') || strpos($l,'modified:')) {
            $parts = explode(':',$l);
            $this->filesChanged[] = trim($parts[1]);
        }
    }

    public function makeDir($dir) {
        echo("\nmkdir ".$this->target.$dir);
    }

    public function putFile($file) {

        $path = dirname(__FILE__).'/'.$file;
        $path = str_replace(' ','\ ',$path);
        $remote = $this->target.$file;
        echo("\nput ".$path." ".$remote);
    }
}


function r($in) {
    echo("\n* ".$in);
    ob_start();
    passthru($in);
    return ob_get_clean();
}

function build_scss() {
    r('/Users/jonah/.npm-packages/bin/sass assets/wagun/styles.scss assets/wagun/styles.css');
}
